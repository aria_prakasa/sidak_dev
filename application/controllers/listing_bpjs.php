<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Listing_bpjs extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// error_reporting(-1);
		// $this->output->enable_profiler(TRUE);
		// check login user
		$this->_init_logged_in();
		$this->curpage = $this->router->fetch_class();
		// $this->model_security->getsecurity();
	}

	public function index($a = "") {
		$isi['content'] = 'list/main_bpjs';
		$isi['judul'] = 'Daftar Penduduk Miskin';
		$isi['sub_judul'] = 'List Peserta BPJS';
		$isi['daftar_nav'] = 'active';
		$isi['list_bpjs_nav'] = 'active';

		if ($a == "export") {

			$this->load->model('vdata_induk_miskin');
			$where = $this->OU->get_filter_location_where();
			// $where = ""; // get all for testing only
			$data_list = $this->vdata_induk_miskin->get_list(0, 0, $orderby = "", $where);
			// EXPORT TO EXCEL AND DOWNLOAD
			$header = $body_arr = null;
			$header = array(
				"NO",
				"NO_KK",
				"NAMA_KEP",
				"NIK",
				"NAMA_LGKP",
				"STAT_HBKEL",
				"JENIS_KLMIN",
				"TMPT_LHR",
				"TGL_LHR",
				"STAT_KWN",
				"AGAMA",
				"PDDK_AKH",
				"JENIS_PKRJN",
				"ALAMAT",
				"NO_RT",
				"NO_RW",
				"NAMA_KEL",
				"NAMA_KEC",
				"NAMA_KAB",
				"NAMA_PROP",
			);

			$no = 1;
			$Vtmp = new Vdata_induk_miskin;
			foreach ($data_list as $row) {
				$Vtmp->setup($row);
				extract((array) $row);
				// var_dump($Vtmp->get_dob()); die;
				$body_arr[] = array(
					$no,
					"'" . $NO_KK,
					$NAMA_KEP,
					"'" . $NIK,
					$NAMA_LGKP,
					$STAT_HBKEL,
					$JENIS_KLMIN,
					$TMPT_LHR,
					$TGL_LHR,
					$STAT_KWN,
					$AGAMA,
					$PDDK_AKH,
					$JENIS_PKRJN,
					$ALAMAT,
					$NO_RT,
					$NO_RW,
					$NAMA_KEL,
					$NAMA_KEC,
					$NAMA_KAB,
					$NAMA_PROP,
				);
				$no++;
			}
			unset($data_mohon);
			$this->load->helper('excel');
			$filename = "data-kemiskinan-valid--" . date("Ym") . '.csv';
			array_to_excel($filename, $header, $body_arr, "xls");
			exit;
		}

		if ($this->uri->segment(3) == '1') {
			$isi = array_merge($isi, $this->session->userdata($search_sess));
			// var_dump($isi);die;
			$kd_prop = $this->session->userdata('provinsi_id');
			$kd_kab = $this->session->userdata('kabupaten_id');
			$kd_kec = $this->session->userdata('kecamatan_id');
			$kel = $this->session->userdata('kelurahan_id');
			// var_dump($kel);die;
			if ($kel) {
				$kelurahan_code = $this->session->userdata('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				// var_dump($kd_kel);die;
			}
			// var_dump($isi);die;

			if ($this->session->userdata('inputkk')) {
				$no_kk = $this->session->userdata('inputkk');
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnik')) {
				$nik = $this->session->userdata('inputnik');
				$entri['nik'] = $nik;
				$isi['toggleNik'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnama')) {
				$n = $this->session->userdata('inputnama');
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['toggleNama'] = "checked";
				// $isi['jml_data'] = 2;
				// $isi['n']        = $n;
			}

			if ($entri) {
				$this->load->model('model_plus');
				$isi['biodata_list'] = $this->model_plus->getbioBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
				$isi['jml_data'] = 1;
			}

		}

		if ($this->input->post('kecamatan_id')) {

			$isi = array_merge($isi, $this->input->post());
			$kd_prop = $this->input->post('provinsi_id');
			$kd_kab = $this->input->post('kabupaten_id');
			$kd_kec = $this->input->post('kecamatan_id');

			if ($this->input->post('kelurahan_id')) {
				$kelurahan_code = $this->input->post('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];

			}

			if ($this->cu->USER_LEVEL == 2) {
				$kd_kec = $this->cu->NO_KEC;
			}
			if ($this->cu->USER_LEVEL == 3) {
				$kd_kec = $this->cu->NO_KEC;
				$kd_kel = $this->cu->NO_KEL;
			}

			if ($this->input->post('inputkk', TRUE)) {

				$no_kk = $this->input->post('inputkk', TRUE);
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";

			}
			if ($this->input->post('inputnik', TRUE)) {

				$nik = $this->input->post('inputnik', TRUE);
				$entri['nik'] = $nik;
				$isi['toggleNik'] = "checked";

			}
			if ($this->input->post('inputnama', TRUE)) {

				$n = $this->input->post('inputnama', TRUE);
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['n'] = $n;
				$isi['toggleNama'] = "checked";

			}

			$this->load->model('model_plus');
			$isi['biodata_list'] = $this->model_plus->getbioBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
			$isi['jml_data'] = 1;

			$this->session->unset_userdata('inputnama');
			$this->session->unset_userdata('inputnik');
			$this->session->unset_userdata('inputkk');
			$this->session->unset_userdata('toggle');
			$search_sess = array_merge($this->input->post());
			$this->session->set_userdata($search_sess);

		}

		/*$this->load->model('vbiodata_valid');
        $isi['biodata_list'] = $biodata_list = $this->vbiodata_valid->get_list(0, 0, $orderby = "", $where);*/

		// $this->load->model('model_list');
		// $isi['biodata_list'] = $this->model_list->getdatapddk();

		$this->load->view('home_view', $isi);
	}
}
