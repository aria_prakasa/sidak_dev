<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Search extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
        $this->load->model('model_list', '', true);
    }

    public function index()
    {
        $isi['content']    = 'list/main_search';
        $isi['judul']      = 'Daftar Kemiskinan';
        $isi['sub_judul']  = 'Pencarian Data';
        $isi['daftar_nav'] = 'active';
        $isi['search_nav'] = 'active';
        // $isi['provinsi']    = $this->model_wilayah->ambil_provinsi();
        // $this->load->view('home_view',$isi);
        if ($this->input->post()) {
            $isi = array_merge($isi, $this->input->post());
        }

        if ($this->input->post('provinsi_id') && $this->input->post('kabupaten_id')) {
            $kd_prop = $this->input->post('provinsi_id');
            $kd_kab  = $this->input->post('kabupaten_id');
            $kd_kec  = $this->input->post('kecamatan_id');
            // $kd_kel = $this->input->post('kelurahan_id');
            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code     = $this->input->post('kelurahan_id'); // 03|1001
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
            }
            // var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;
            $isi['data_list'] = $this->model_list->getdatavalid($kd_prop, $kd_kab, $kd_kec, $kd_kel);

            // var_dump($isi['ket_prop'],$isi['ket_kab'],$isi['ket_kec'],$isi['ket_kel']);die;
        }

        $this->load->view('home_view', $isi);
    }

    // dijalankan saat provinsi di klik
    public function pilih_kabupaten($id = 0)
    {
        //var_dump($this->uri->segment(3));die;
        $isi['kabupaten'] = $this->model_wilayah->ambil_kabupaten($id);
        $this->load->view('v_drop_down_kabupaten', $isi);
        //var_dump($isi['kabupaten']);die;
    }

    // dijalankan saat kabupaten di klik
    public function pilih_kecamatan($id = 0)
    {
        $isi['kecamatan'] = $this->model_wilayah->ambil_kecamatan($id);
        $this->load->view('v_drop_down_kecamatan', $isi);
    }

    // dijalankan saat kecamatan di klik
    public function pilih_kelurahan($kec_id = 0)
    {
        // $kelurahan_code = $this->input->post('kelurahan_id');// 03|1001
        // $kelurahan_code_arr = explode("|", $kelurahan_code);
        // $isi['kecamatan_id'] = $kelurahan_code_arr[0];
        // $isi['kelurahan_id'] = $kelurahan_code_arr[1];
        $isi['kelurahan_id'] = $this->input->post('kelurahan_id');
        $isi['kelurahan']    = $this->model_wilayah->ambil_kelurahan($kec_id);
        $this->load->view('v_drop_down_kelurahan', $isi);
    }

}
