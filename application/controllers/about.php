<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// check login user
		$this->_init_logged_in();
		// $this->model_security->getsecurity();
	}

    public function index()
    {
        // $this->model_security->getsecurity();
        $isi['content']   = 'home/about_view';
        $isi['judul']     = 'Halaman Utama';
        $isi['sub_judul'] = 'Tentang Kami';
        $isi['home_nav']  = 'active';
        $isi['about_nav'] = 'active';

        $this->load->view('home_view', $isi);
    }
    
}