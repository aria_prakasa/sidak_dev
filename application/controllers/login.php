<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->curpage = "login";
    }

    public function index()
    {
        if ($this->cu) {
            user_home();
            exit;
        } elseif ($this->ca) {
            admin_home();
            exit;
        }
        $data = array('username' => '', 'password' => '');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run()) {
            extract($_POST);
            // var_dump($_POST);
            // die;
            // check admin
            $check = get_admin($username, $password);
            if (!$check) {
                // var_dump("not admin");
                // die;
                $check = get_user($username, $password);
                if (!$check) {
                    $this->session->set_flashdata('error', 'Kombinasi username dan password salah. Silahkan ulangi kembali.');
                    redirect($this->curpage);
                } else {
                    set_login_session($username, $password, 'user', ($this->input->post('remember') ? true : false));
                    user_home();
                }
            } else {
                // var_dump("is admin");
                // die;
                set_login_session($username, $password, "admin");
                admin_home();
            }
            exit;
        }
        $this->load->view('login_view');
    }

    public function getlogin()
    {
        $u = $this->input->post('username');
        $p = $this->input->post('password');
        $this->load->model('model_login');
        $this->model_login->getlogin($u, $p);

        //$this->load->model('user_model');
        //$this->user_model->getlogin($u,$p);
    }
}
