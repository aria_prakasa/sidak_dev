<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Listing_biodata extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// error_reporting(-1);
		// $this->output->enable_profiler(TRUE);
		// check login user
		$this->_init_logged_in();
		$this->curpage = $this->router->fetch_class();
		// $this->model_security->getsecurity();
	}

	public function index($a = "") {
		$isi['content'] = 'list/main_biodata';
		$isi['judul'] = 'Daftar Penduduk Miskin';
		$isi['sub_judul'] = 'List Biodata Penduduk';
		$isi['daftar_nav'] = 'active';
		$isi['bio_nav'] = 'active';

		if ($a == "export") {

			$isi = array_merge($isi, $this->session->userdata($search_sess));
			// var_dump($isi);die;
			$kd_prop = $this->session->userdata('provinsi_id');
			$kd_kab = $this->session->userdata('kabupaten_id');
			$kd_kec = $this->session->userdata('kecamatan_id');
			$kel = $this->session->userdata('kelurahan_id');
			// var_dump($kel);die;
			if ($kel) {
				$kelurahan_code = $this->session->userdata('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				// var_dump($kd_kel);die;
			}
			// var_dump($isi);die;

			if ($this->session->userdata('inputkk')) {
				$no_kk = $this->session->userdata('inputkk');
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnik')) {
				$nik = $this->session->userdata('inputnik');
				$entri['nik'] = $nik;
				$isi['toggleNik'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnama')) {
				$n = $this->session->userdata('inputnama');
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['toggleNama'] = "checked";
				// $isi['jml_data'] = 2;
				// $isi['n']        = $n;
			}

			if ($entri) {
				$this->load->model('model_list');
				$data_list = $this->model_list->getbioValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
			}

			$this->load->model('vdata_induk_miskin');
			// $where = $this->OU->get_filter_location_where();
			// // $where = ""; // get all for testing only
			// $data_list = $this->vdata_induk_miskin->get_list(0, 0, $orderby = "", $where);
			// EXPORT TO EXCEL AND DOWNLOAD
			$header = $body_arr = null;
			$header = array(
				"NO",
				"NO_KK",
				"NAMA_KEP",
				"NIK",
				"NAMA_LGKP",
				"STAT_HBKEL",
				"JENIS_KLMIN",
				"TMPT_LHR",
				"TGL_LHR",
				"STAT_KWN",
				"AGAMA",
				"PDDK_AKH",
				"JENIS_PKRJN",
				"ALAMAT",
				"NO_RT",
				"NO_RW",
				"NAMA_KEL",
				"NAMA_KEC",
				"NAMA_KAB",
				"NAMA_PROP",
			);

			$no = 1;
			$Vtmp = new Vdata_induk_miskin;
			foreach ($data_list as $row) {
				$Vtmp->setup($row);
				extract((array) $row);
				// var_dump($Vtmp->get_dob()); die;
				$body_arr[] = array(
					$no,
					"'" . $NO_KK,
					$NAMA_KEP,
					"'" . $NIK,
					$NAMA_LGKP,
					$STAT_HBKEL,
					$JENIS_KLMIN,
					$TMPT_LHR,
					$TGL_LHR,
					$STAT_KWN,
					$AGAMA,
					$PDDK_AKH,
					$JENIS_PKRJN,
					$ALAMAT,
					$NO_RT,
					$NO_RW,
					$NAMA_KEL,
					$NAMA_KEC,
					$NAMA_KAB,
					$NAMA_PROP,
				);
				$no++;
			}
			unset($data_list);
			$this->load->helper('excel');
			$filename = "data-kemiskinan-valid--" . date("Ym") . '.csv';
			array_to_excel($filename, $header, $body_arr, "xls");
			exit;
		}

		if ($this->uri->segment(3) == '1') {
			$isi = array_merge($isi, $this->session->userdata($search_sess));
			// var_dump($isi);die;
			$kd_prop = $this->session->userdata('provinsi_id');
			$kd_kab = $this->session->userdata('kabupaten_id');
			$kd_kec = $this->session->userdata('kecamatan_id');
			$kel = $this->session->userdata('kelurahan_id');
			// var_dump($kel);die;
			if ($kel) {
				$kelurahan_code = $this->session->userdata('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				// var_dump($kd_kel);die;
			}
			// var_dump($isi);die;

			if ($this->session->userdata('inputkk')) {
				$no_kk = $this->session->userdata('inputkk');
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnik')) {
				$nik = $this->session->userdata('inputnik');
				$entri['nik'] = $nik;
				$isi['toggleNik'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnama')) {
				$n = $this->session->userdata('inputnama');
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['toggleNama'] = "checked";
				// $isi['jml_data'] = 2;
				// $isi['n']        = $n;
			}

			if ($entri) {
				$this->load->model('model_list');
				$isi['biodata_list'] = $this->model_list->getbioValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
				$isi['jml_data'] = 1;
			}

		}

		if ($this->input->post('kecamatan_id')) {

			$isi = array_merge($isi, $this->input->post());
			$kd_prop = $this->input->post('provinsi_id');
			$kd_kab = $this->input->post('kabupaten_id');
			$kd_kec = $this->input->post('kecamatan_id');

			if ($this->input->post('kelurahan_id')) {
				$kelurahan_code = $this->input->post('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];

			}

			if ($this->cu->USER_LEVEL == 2) {
				$kd_kec = $this->cu->NO_KEC;
			}
			if ($this->cu->USER_LEVEL == 3) {
				$kd_kec = $this->cu->NO_KEC;
				$kd_kel = $this->cu->NO_KEL;
			}

			if ($this->input->post('inputkk', TRUE)) {

				$no_kk = $this->input->post('inputkk', TRUE);
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";

			}
			if ($this->input->post('inputnik', TRUE)) {

				$nik = $this->input->post('inputnik', TRUE);
				$entri['nik'] = $nik;
				$isi['toggleNik'] = "checked";

			}
			if ($this->input->post('inputnama', TRUE)) {

				$n = $this->input->post('inputnama', TRUE);
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['n'] = $n;
				$isi['toggleNama'] = "checked";

			}

			$this->load->model('model_list');
			$isi['biodata_list'] = $this->model_list->getbioValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
			$isi['jml_data'] = 1;

			$this->session->unset_userdata('inputnama');
			$this->session->unset_userdata('inputnik');
			$this->session->unset_userdata('inputkk');
			$this->session->unset_userdata('toggle');
			$search_sess = array_merge($this->input->post());
			$this->session->set_userdata($search_sess);

		}

		/*$this->load->model('vbiodata_valid');
        $isi['biodata_list'] = $biodata_list = $this->vbiodata_valid->get_list(0, 0, $orderby = "", $where);*/

		// $this->load->model('model_list');
		// $isi['biodata_list'] = $this->model_list->getdatapddk();

		$this->load->view('home_view', $isi);
	}

	public function lihat() {
		$isi['content'] = 'list/biodata_view';
		$isi['judul'] = 'Daftar Kemiskinan';
		$isi['sub_judul'] = 'List Data';
		$isi['daftar_nav'] = 'active';
		$isi['bio_nav'] = 'active';
		$isi['controller'] = 'listing_biodata';

		if ($this->session->flashdata('nik')) {
			$nik = $this->session->flashdata('nik');
		} else {
			$nik = $this->uri->segment(3);
		}

		//var_dump($no_kk);die;
		$this->load->model('model_list');
		$biodata = $this->model_list->getbiodata($nik);

		if (!$biodata) {
			$this->session->set_flashdata('info', "ERROR");
			redirect('listing_biodata', "refresh");
			exit;
		} else {
			//die(var_dump($data_kk));
			//die(var_dump($anggota_kk)); //dd($data_kk);
			foreach ($biodata as $key => $value) {
				$isi[$key] = $value;
			}
		}

		$bio_miskin = $this->model_list->check_dataMiskin($nik);

		foreach ($bio_miskin as $key => $value) {
			$isi[$key] = $value;
		}

		$this->load->model('model_keterangan');
		// $isi['opsi_gaji']   = $this->model_keterangan->getgaji();
		$isi['opsi_kategori'] = $this->model_keterangan->getkategori();

		$this->load->view('home_view', $isi);
	}

	public function simpan() {
		if ($this->input->post()) {
			$no_kk = $this->input->post('no_kk');
			$this->session->set_userdata($search_sess);
			if ($this->input->post('submit')) {
				// var_dump($this->input->post()); die;
				$nik = $this->input->post('submit');

				$data['nik'] = $this->input->post('submit');
				$data['no_bpjs'] = $this->input->post('bpjs');
				$data['kategori'] = $this->input->post('kategori');
				$data['tgl_berlaku'] = strtoupper(date($this->input->post('period')));
				// $data['tgl_insert']     = strtoupper(date('d-m-Y'));
				$data['nip_pet_entri'] = $this->cu->NIP;
				$data['nama_pet_entri'] = $this->cu->NAMA_LENGKAP;
				// var_dump($data);die;
				//--cek if data exist
				$this->load->model('model_plus');
				$check = $this->model_plus->check_bpjs($nik);
				if ($check) {
					// $this->load->model('model_plus');
					$update = $this->model_plus->update_bpjs($nik, $data);
				} else {
					// $this->load->model('model_plus');
					$insert = $this->model_plus->insert_bpjs($data);
				}
			}

			if ($this->input->post('hapus')) {
				$nik = $this->input->post('hapus');

				$this->load->model('model_plus');
				$delete = $this->model_plus->delete_bpjs($nik);
			}

			$this->session->set_flashdata('nik', $nik);
			redirect('listing_biodata/lihat');
			exit;
		}

	}

}
