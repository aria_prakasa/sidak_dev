<?php
class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_kecamatan');

    }
    public function index()
    {
        $data['kecamatan'] = $this->model_kecamatan->getkecamatanlist();
        //var_dump($data['kecamatan']);die;
        $this->load->view('select_wilayah', $data);
    }

    public function select_kelurahan($no_kec)
    {
        $this->load->model('model_kelurahan');
        header('Content-Type: application/x-json; charset=utf-8');
        echo (json_encode($this->model_kelurahan->getkelurahanlist($no_kec)));
    }

}
