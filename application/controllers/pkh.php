<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pkh extends MY_Controller {
    public function __construct() {
        parent::__construct();
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        // $this->model_security->getsecurity();
        $isi['content']     = 'Plus/pkh/main_pkh';
        $isi['judul']       = 'Entri Kemiskinan Plus';
        $isi['sub_judul']   = 'Rumah Tidak Layak Huni';
        $isi['plus_nav']    = 'active';
        $isi['pkh_nav']     = 'active';

        if ($this->uri->segment(3) == '1') {
            $isi = array_merge($isi, $this->session->userdata($search_sess));
            // var_dump($isi);die;
            $kd_prop    = $this->session->userdata('provinsi_id');
            $kd_kab     = $this->session->userdata('kabupaten_id');
            $kd_kec     = $this->session->userdata('kecamatan_id');
            $kel        = $this->session->userdata('kelurahan_id');
            // var_dump($kel);die;   
            if ($kel) {
                $kelurahan_code     = $this->session->userdata('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
             // var_dump($kd_kel);die;   
            }
            // var_dump($isi);die;

            if ($this->session->userdata('inputkk')) {
                $no_kk = $this->session->userdata('inputkk');
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                // $isi['jml_data'] = 1;
            }

            if ($this->session->userdata('inputnama')) {
                $n    = $this->session->userdata('inputnama');
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['toggleNama'] = "checked";
                // $isi['jml_data'] = 2;
                // $isi['n']        = $n;
            }

            if($entri)
            {
                $this->load->model('model_plus');
                $isi['data_list'] = $this->model_plus->getdatakkPlus($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
                // var_dump($isi['data_list']);die;
                $isi['jml_data'] = 1;
            }    
        }   


        if ($this->input->post('inputkk') || $this->input->post('inputnama')) {
            $isi        = array_merge($isi, $this->input->post());
            // var_dump($isi);die;
            $kd_prop    = $this->input->post('provinsi_id');
            $kd_kab     = $this->input->post('kabupaten_id');

            if ($this->input->post('kecamatan_id')) {
                $kd_kec     = $this->input->post('kecamatan_id');
            }

            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code     = $this->input->post('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
                
            }

            if($this->cu->USER_LEVEL == 2)
                {
                    $kd_kec = $this->cu->NO_KEC;
                }
                if($this->cu->USER_LEVEL == 3)
                {
                    $kd_kec = $this->cu->NO_KEC;
                    $kd_kel = $this->cu->NO_KEL;
                }

            if ($this->input->post('inputkk',TRUE)) {

                $no_kk = $this->input->post('inputkk',TRUE);
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                
            }
            
            if ($this->input->post('inputnama',TRUE)) {

                $n    = $this->input->post('inputnama',TRUE);
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['n']        = $n;
                $isi['toggleNama'] = "checked";

            }
            // var_dump($nama,$no_kk);die;
            $this->load->model('model_plus');
            $isi['data_list'] = $this->model_plus->getdatakkPlus($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
            $isi['jml_data'] = 1;

            $this->session->unset_userdata('inputnama');
            // $this->session->unset_userdata('inputnik');
            $this->session->unset_userdata('inputkk');
            $this->session->unset_userdata('toggle');
            $search_sess = array_merge($this->input->post());
            $this->session->set_userdata($search_sess);
        }
        
        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'Plus/pkh/detail_pkh';
        $isi['judul']       = 'Entri Kemiskinan Plus';
        $isi['sub_judul']   = 'Rumah Tidak Layak Huni';
        $isi['plus_nav']    = 'active';
        $isi['pkh_nav']     = 'active';

        if ($this->uri->segment(3)) {
            $no_kk = $this->uri->segment(3);
        } else {
            $no_kk = $this->session->flashdata('no_kk');
        }
        
        $this->load->model('model_list');
        $data_list    = $this->model_list->getdata($no_kk);
        // var_dump($data_list);die;
        $anggota_list = $this->model_list->getanggota($no_kk);
        // var_dump($data_list);die;
        if (!$data_list) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('raskin', "refresh");
            exit;
        } else {
           foreach ($data_list as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_list'] = $anggota_list;
        }

        // var_dump($data_list);die;
        $this->load->model('model_plus');
        $data_plus = $this->model_plus->getdataPlus($no_kk);
        if (!$data_plus) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('raskin', "refresh");
            exit;
        } else {
            foreach ($data_plus as $key => $value) {
                $isi[$key] = $value;
            }
        }

        // var_dump($isi);die;
        $this->load->model('model_keterangan');
        $isi['opsi_hunian']   = $this->model_keterangan->gethunian();
        $this->load->view('home_view', $isi);
    }

    public function simpan()
    {
        if($this->input->post())
        {
            if($this->input->post('submit'))
            {
                $no_kk          = $this->input->post('submit');

                $data['no_kk']          = $this->input->post('submit');
                $data['bantuan']        = strtoupper($this->input->post('bantuan'));
                $data['tipe_hunian']    = $this->input->post('tipe_hunian');
                // $data['tgl_insert']     = strtoupper(date('d-m-Y'));
                $data['nip_pet_entri']  = $this->cu->NIP;
                $data['nama_pet_entri'] = $this->cu->NAMA_LENGKAP;
                
                //--cek if data exist
                $this->load->model('model_plus');
                $check = $this->model_plus->check_pkh($no_kk);
                if ($check) {
                    $this->load->model('model_plus');
                    $update = $this->model_plus->update_pkh($no_kk,$data);
                    // var_dump($update);die;
                } else {
                    $this->load->model('model_plus');
                    $insert = $this->model_plus->insert_pkh($data);
                }
            }

            if($this->input->post('hapus'))
            {
                $no_kk          = $this->input->post('hapus');
                // var_dump($no_kk);die;
                $this->load->model('model_plus');
                $delete = $this->model_plus->delete_pkh($no_kk);        
            }

            $this->session->set_flashdata('no_kk', $no_kk);
            redirect('pkh/lihat');
            exit;
        }

    }
}