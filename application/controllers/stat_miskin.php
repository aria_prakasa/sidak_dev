<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stat_miskin extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
        $this->load->model('model_agregat', '', true);
    }

    public function index()
    {
        $isi['content']         = 'statistik/main_miskin';
        $isi['judul']           = 'Statistik';
        $isi['sub_judul']       = 'Statistik Data Miskin';
        $isi['stat_nav']        = 'active';
        $isi['stat_miskin_nav'] = 'active';
        // $isi['agr_nav_tabs']    = 'active';

        // $isi['provinsi']    = $this->Model_wilayah->ambil_provinsi();
        // $isi['kabupaten']    = $this->Model_wilayah->ambil_kabupaten();
        // $isi['kecamatan']    = $this->Model_wilayah->ambil_kecamatan();
        if ($this->input->post()) {
            $isi = array_merge($isi, $this->input->post());
        }
        if ($this->input->post('provinsi_id') && $this->input->post('kabupaten_id')) {
            $kd_prop = $this->input->post('provinsi_id');
            $kd_kab  = $this->input->post('kabupaten_id');
            if ($this->input->post('kecamatan_id')) {
                $kd_kec              = $this->input->post('kecamatan_id');
                $isi['filter_level'] = 1;
            }
            // $kd_kel = $this->input->post('kelurahan_id');
            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code      = $this->input->post('kelurahan_id'); // 03|1001
                $kelurahan_code_arr  = explode("-", $kelurahan_code);
                $kd_kec              = $kelurahan_code_arr[0];
                $kd_kel              = $kelurahan_code_arr[1];
                $isi['filter_level'] = 2;
                // var_dump($kd_kel);die;
            }
            // var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;
            $isi['agregat'] = $this->model_agregat->getagrMiskin($kd_prop, $kd_kab, $kd_kec, $kd_kel);
            // $keterangan_wil = $this->Model_wilayah->getWilayah($kd_prop,$kd_kab,$kd_kec,$kd_kel);
            // var_dump($keterangan_wil);die;
            // var_dump($isi['ket_prop'],$isi['ket_kab'],$isi['ket_kec'],$isi['ket_kel']);die;
        }

        $this->load->view('home_view', $isi);
    }

    /*// dijalankan saat provinsi di klik
public function pilih_kabupaten($id=0){
//var_dump($this->uri->segment(3));die;
$isi['kabupaten']=$this->Model_wilayah->ambil_kabupaten($id);
$this->load->view('v_drop_down_kabupaten',$isi);
//var_dump($isi['kabupaten']);die;
}

// dijalankan saat kabupaten di klik
public function pilih_kecamatan($id=0){
$isi['kecamatan']=$this->Model_wilayah->ambil_kecamatan($id);
$this->load->view('v_drop_down_kecamatan',$isi);
}

// dijalankan saat kecamatan di klik
public function pilih_kelurahan($id=0){
$isi['kelurahan']=$this->Model_wilayah->ambil_kelurahan($id);
$this->load->view('v_drop_down_kelurahan',$isi);
}*/
}
