<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agr_penduduk extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
        $this->load->model('model_agregat', '', true);
        $this->load->model('model_header', '', true);
    }

    public function index()
    {
        $isi['content']      = 'agregat/main_pddk';
        $isi['judul']        = 'Statistik';
        $isi['sub_judul']    = 'Agregat Data Penduduk';
        $isi['agr_nav']      = 'active';
        $isi['agr_pend_nav'] = 'active';
        // $isi['agr_nav_tabs']    = 'active';

        if ($this->input->post()) {
            $isi = array_merge($isi, $this->input->post());
        }
        // if($this->input->post('provinsi_id') && $this->input->post('kabupaten_id') && $this->input->post('kd_agr') && $this->input->post('bulan'))
        if ($this->input->post('kd_agr') && $this->input->post('bulan')) {
            $kd_prop = $this->input->post('provinsi_id');
            $kd_kab  = $this->input->post('kabupaten_id');
            $kd_kec  = $this->input->post('kecamatan_id');
            // $kd_kel = $this->input->post('kelurahan_id');
            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code     = $this->input->post('kelurahan_id'); // 03|1001
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
                $isi['pane_nav']    = 'hide';
                $isi['kel_active']  = 'active';
            } else {
                $isi['kec_active'] = 'active';
            }
            $kd_agr = $this->input->post('kd_agr');
            $bulan  = $this->input->post('bulan');
            // var_dump($kd_agr,$bulan);die;
            // var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;

            if ($kd_agr == 1) {
                $isi['agregat'] = $this->model_agregat->getagrPddk($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            if ($kd_agr == 2) {
                $isi['agregatAgama'] = $this->model_agregat->getagrAgama($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                // $isi['headers']      = $this->model_header->getheaderAgama();
                // var_dump($isi['agregatAgama']);die;
            }
            if ($kd_agr == 3) {
                $isi['agregatGoldrh'] = $this->model_agregat->getagrGoldrh($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                //$isi['headers'] = $this->model_header->getheaderGoldrh();
            }
            if ($kd_agr == 4) {
                $isi['agregatPddkan'] = $this->model_agregat->getagrPddkan($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                // $isi['headers'] = $this->model_header->getheaderPddkan();
                //var_dump($isi['agregatPddkan']);die;
            }
            if ($kd_agr == 5) {
                $isi['agregatPkrjn'] = $this->model_agregat->getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                // $isi['headers'] = $this->model_header->getheaderPddkan();
                //var_dump($isi['agregatPddkan']);die;
            }
            if ($kd_agr == 6) {
                $isi['agregatUmur'] = $this->model_agregat->getagrUmur($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                $isi['kec_active']  = 'active';
                unset($isi['pane_nav']);
                unset($isi['kel_active']);
            }
            if ($kd_agr == 7) {
                $isi['agregatUmur'] = $this->model_agregat->getagrUmurBppd($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
                $isi['kec_active']  = 'active';
                unset($isi['pane_nav']);
                unset($isi['kel_active']);
            }
            if ($kd_agr == 8) {
                $isi['agregat'] = $this->model_agregat->getagrWajibktp($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            if ($kd_agr == 9) {
                $isi['agregat'] = $this->model_agregat->getagrKTP($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            if ($kd_agr == 10) {
                $isi['agregat'] = $this->model_agregat->getagrKTP17($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            if ($kd_agr == 11) {
                $isi['agregat'] = $this->model_agregat->getagrKwn17($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            if ($kd_agr == 12) {
                $isi['agregat'] = $this->model_agregat->getagrKK($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan);
            }
            // var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;

            // var_dump($isi['ket_prop'],$isi['ket_kab'],$isi['ket_kec'],$isi['ket_kel']);die;
        }

        $this->load->view('home_view', $isi);
    }

}
