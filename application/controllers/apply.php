<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apply extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->curpage = $this->router->fetch_class();
        // $this->model_security->getsecurity();
    }

    public function index($a = "")
    {
        $isi['content']     = 'data/main_apply';
        $isi['judul']       = 'Pemasukan Data';
        $isi['sub_judul']   = 'List';
        $isi['data_nav']    = 'active';
        $isi['ltmohon_nav'] = 'active';
        //$this->load->view('home_view',$isi);
        // $where = $this->OU->get_filter_location_where();
        // var_dump($where);die;
        // $where = ""; // get all for testing only

        if ($a == "export") {

            $this->load->model('vdata_induk_mohon');
            $where = $this->OU->get_filter_location_where();
            // $where = ""; // get all for testing only
            $data_mohon = $this->vdata_induk_mohon->get_list(0, 0, $orderby = "", $where);
            // EXPORT TO EXCEL AND DOWNLOAD
            $header = $body_arr = null;
            $header = array(
                "NO",
                "NO_KK",
                "NAMA_KEP",
                "NIK",
                "NAMA_LGKP",
                "STAT_HBKEL",
                "JENIS_KLMIN",
                "TMPT_LHR",
                "TGL_LHR",
                "STAT_KWN",
                "AGAMA",
                "PDDK_AKH",
                "JENIS_PKRJN",
                "ALAMAT",
                "NO_RT",
                "NO_RW",
                "NAMA_KEL",
                "NAMA_KEC",
                "NAMA_KAB",
                "NAMA_PROP",
            );

            $no   = 1;
            $Vtmp = new Vdata_induk_mohon;
            foreach ($data_mohon as $row) {
                $Vtmp->setup($row);
                extract((array) $row);
                // var_dump($Vtmp->get_dob()); die;
                $body_arr[] = array(
                    $no,
                    "'" . $NO_KK,
                    $NAMA_KEP,
                    "'" . $NIK,
                    $NAMA_LGKP,
                    $STAT_HBKEL,
                    $JENIS_KLMIN,
                    $TMPT_LHR,
                    $TGL_LHR,
                    $STAT_KWN,
                    $AGAMA,
                    $PDDK_AKH,
                    $JENIS_PKRJN,
                    $ALAMAT,
                    $NO_RT,
                    $NO_RW,
                    $NAMA_KEL,
                    $NAMA_KEC,
                    $NAMA_KAB,
                    $NAMA_PROP,
                );
                $no++;
            }
            unset($data_mohon);
            $this->load->helper('excel');
            $filename = "data-kemiskinan-permohonan--" . date("Ym") . '.csv';
            array_to_excel($filename, $header, $body_arr, "xls");
            exit;
        }

        // $this->load->model('model_verify');
        // $isi['data_mohon'] = $data_mohon = $this->model_verify->getdatamohon();
        $this->load->model('vdata_mohon');
        $where = $this->OU->get_filter_location_where();
        $isi['data_mohon'] = $data_mohon = $this->vdata_mohon->get_list(0, 0, $orderby = "", $where);
        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'data/view';
        $isi['judul']       = 'Pemasukan Data';
        $isi['sub_judul']   = 'List';
        $isi['data_nav']    = 'active';
        $isi['ltmohon_nav'] = 'active';

        if(!$this->uri->segment(3))
        {
            redirect('apply', "refresh");
            exit;
        }    

        $no_kk = $this->uri->segment(3);
        $this->load->model('model_entry');
        $data_mohon    = $this->model_entry->getdatamohon($no_kk);
        $anggota_mohon = $this->model_entry->getanggotamohon($no_kk);
        if (!$data_mohon) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('apply', "refresh");
            exit;
        } else {
            //die(var_dump($data_kk));
            //die(var_dump($anggota_kk)); //dd($data_kk);
            foreach ($data_mohon as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_mohon'] = $anggota_mohon;
        }

        $this->load->view('home_view', $isi);
    }

    public function hapus()
    {
        // $no_kk = $this->uri->segment(3);
        $no_kk = $this->input->post('no_kk');
        // $check = $this->input->post('check');
        // var_dump($no_kk, $check);die;

        if($this->input->post('check'))
        {
            // var_dump($no_kk, $check);die;
            $nik = array_keys($this->input->post('check'));

            $this->load->model('model_entry');
            $biodata = $this->model_entry->getanggotamohon($no_kk);
            // var_dump($biodata);die;
            if ($biodata) {
                foreach ($biodata as $row) {
                    if(!in_array($row->nik, $nik)) 
                    {
                        continue;
                    }
                    $deleteNik = $row->nik;
                    // var_dump($deleteNik);die;
                    /*$datapddk[] = array(
                        'nik'            => $deleteNik
                        
                    );*/
                    $this->load->model('model_pullout');
                    if($this->model_pullout->biodeleteMohon($deleteNik))
                    {
                        $this->model_entry->get_biodelete($deleteNik);               
                    } 
                    else 
                    {
                        die("Koneksi VPN gagal!");
                    }
                }
            }
            // var_dump($datapddk);die;
        }
        //var_dump($no_kk);die;
        $check = $this->model_entry->getanggotamohon($no_kk);
        if($check)
        {
            $this->session->set_flashdata('no_kk', $no_kk);
            redirect('apply/lihat/' . $no_kk);
            exit;
        }
        redirect('apply/index');
        exit;
    }

    public function lihat_biodata()
    {
        $isi['content']    = 'data/biodata_view';
        $isi['judul']      = 'Pemasukan Data';
        $isi['sub_judul']  = 'Entri';
        $isi['data_nav']   = 'active';
        $isi['entri_nav']  = 'active';
        $isi['controller'] = 'apply';

        if ($this->uri->segment(4)) {
            $nik = $this->uri->segment(4);
        } else {
            $nik = $this->session->flashdata('nik');
        }

        $this->load->model('model_entry');
        //$data_kk = $this->model_entry->getdatakk($no_kk);
        $biodata    = $this->model_entry->getbiodata($nik);
        $bio_miskin = $this->model_entry->getbiodata($nik);
        //die(var_dump($data_kk, $anggota_kk));
        if (!$biodata) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($biodata as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $bio_miskin = $this->model_entry->getcatatan_miskin($nik);

        foreach ($bio_miskin as $key => $value) {
            $isi[$key] = $value;
        }

        $this->load->view('home_view', $isi);
    }

    public function simpan_biodata()
    {

        $no_kk              = $this->uri->segment(3);
        $nik                = $this->uri->segment(4);
        $datamskn['nik']    = $nik;
        $datamskn['field1'] = strtoupper($this->input->post('field1'));
        $datamskn['field2'] = strtoupper($this->input->post('field2'));
        $datamskn['field3'] = strtoupper($this->input->post('field3'));
        $datamskn['field4'] = strtoupper($this->input->post('field4'));

        //--cek if data exist
        $this->load->model('model_entry');
        $check = $this->model_entry->check_dataMiskin($nik);
        if ($check) {
            $this->load->model('model_entry');
            $update = $this->model_entry->update_dataMiskin($datamskn);
        } else {
            $this->load->model('model_entry');
            $insert = $this->model_entry->insert_dataMiskin($datamskn);
        }

        // var_dump($datamskn['field1'],$datamskn['field2'],$datamskn['field3'],$datamskn['field4'],$no_kk,$datamskn['nik']);die;

        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('apply/lihat');
        exit;
        //}
    }
}
