<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bpjs extends MY_Controller {
    public function __construct() {
        parent::__construct();
        // check login user
        $this->_init_logged_in();
        // $this->output->enable_profiler(TRUE);
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        // $this->model_security->getsecurity();
        $isi['content']   = 'Plus/bpjs/main_bpjs';
        $isi['judul']     = 'Entri Kemiskinan Plus';
        $isi['sub_judul'] = 'Kepesertaan BPJS';
        $isi['plus_nav']  = 'active';
        $isi['bpjs_nav'] = 'active';

        if ($this->uri->segment(3) == '1') {
            $isi = array_merge($isi, $this->session->userdata($search_sess));
            // $search_sess_dump = array_merge($this->session->userdata($search_sess));
            // $this->session->set_userdata($search_sess_dump);
            // var_dump($isi);die; $search_sess_dump = array_merge($this->session->userdata($search_sess));
            $this->session->set_userdata($search_sess_dump);
            $kd_prop    = $this->session->userdata('provinsi_id');
            $kd_kab     = $this->session->userdata('kabupaten_id');
            $kd_kec     = $this->session->userdata('kecamatan_id');
            $kel        = $this->session->userdata('kelurahan_id');
            // var_dump($kel);die;   
            if ($kel) {
                $kelurahan_code     = $this->session->userdata('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
             // var_dump($kd_kel);die;   
            }
            // var_dump($isi);die;

            if ($this->session->userdata('inputkk')) {
                $no_kk = $this->session->userdata('inputkk');
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                // $isi['jml_data'] = 1;
            }

            if ($this->session->userdata('inputnik')) {
                $nik = $this->session->userdata('inputnik');
                $entri['nik']   = $nik;
                $isi['toggleNik'] = "checked";
                // $isi['jml_data'] = 1;
            }

            if ($this->session->userdata('inputnama')) {
                $n    = $this->session->userdata('inputnama');
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['toggleNama'] = "checked";
                // $isi['jml_data'] = 2;
                // $isi['n']        = $n;
            }

            if($entri)
            {
                $this->load->model('model_plus');
                $isi['biodata_list'] = $this->model_plus->getbioPlus($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
                // var_dump($isi['data_list']);die;
                $isi['jml_data'] = 1;
            }

            // $this->session->unset_userdata('inputnama');
            // $this->session->unset_userdata('inputnik');
            // $this->session->unset_userdata('inputkk');
            // $this->session->unset_userdata('toggle');
            
        }   


        if ($this->input->post('inputkk') || $this->input->post('inputnik') || $this->input->post('inputnama')) {
            $isi        = array_merge($isi, $this->input->post());
            // var_dump($isi);die;
            $kd_prop    = $this->input->post('provinsi_id');
            $kd_kab     = $this->input->post('kabupaten_id');

            if ($this->input->post('kecamatan_id')) {
                $kd_kec     = $this->input->post('kecamatan_id');
            }

            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code     = $this->input->post('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
                
            }

            if($this->cu->USER_LEVEL == 2)
                {
                    $kd_kec = $this->cu->NO_KEC;
                }
                if($this->cu->USER_LEVEL == 3)
                {
                    $kd_kec = $this->cu->NO_KEC;
                    $kd_kel = $this->cu->NO_KEL;
                }

            if ($this->input->post('inputkk',TRUE)) {

                $no_kk = $this->input->post('inputkk',TRUE);
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                
            }
            if ($this->input->post('inputnik',TRUE)) {

                $nik = $this->input->post('inputnik',TRUE);
                $entri['nik']   = $nik;
                $isi['toggleNik'] = "checked";
                
            }
            if ($this->input->post('inputnama',TRUE)) {

                $n    = $this->input->post('inputnama',TRUE);
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['n']        = $n;
                $isi['toggleNama'] = "checked";

            }

            // var_dump($entri);die;
            $this->load->model('model_plus');
            $isi['biodata_list'] = $this->model_plus->getbioPlus($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
            // $isi['biodata_list'] = $this->Vbiodata_plus->get_list(0, 0, $orderby = "", $where);
            $isi['jml_data'] = 1;

            $this->session->unset_userdata('inputnama');
            $this->session->unset_userdata('inputnik');
            $this->session->unset_userdata('inputkk');
            $this->session->unset_userdata('toggle');
            $search_sess = array_merge($this->input->post());
            $this->session->set_userdata($search_sess);
            // var_dump($isi['biodata_list']);die;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'Plus/bpjs/detail_bpjs';
        $isi['judul']       = 'Entri Kemiskinan Plus';
        $isi['sub_judul']   = 'Kepesertaan BPJS';
        $isi['plus_nav']    = 'active';
        $isi['bpjs_nav']    = 'active';

        if ($this->uri->segment(3)) {
            $no_kk = $this->uri->segment(3);
        } else {
            $no_kk = $this->session->flashdata('no_kk');
        }

        $this->load->model('model_plus');
        $isi['data_list'] = $this->model_plus->getdataKK($no_kk);
        
         // var_dump($isi['data_list']);die;

        $this->load->model('model_list');
        $data_kk = $this->model_list->getdata($no_kk);
        
        //die(var_dump($data_kk, $anggota_kk));
        if (!$data_kk) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('bpjs', "refresh");
            exit;
        } else {
            foreach ($data_kk as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $this->load->model('model_keterangan');
        $isi['opsi_kategori']   = $this->model_keterangan->getkategori();
        
        $this->load->view('home_view', $isi);
    }

    public function simpan()
    {
        $this->session->set_userdata($search_sess);
        if($this->input->post())
        {
            $no_kk          = $this->input->post('no_kk');
            $this->session->set_userdata($search_sess);
            if($this->input->post('submit'))
            {
                // var_dump($this->input->post()); die;
                $nik            = $this->input->post('submit');

                $data['nik']            = $this->input->post('submit');
                $data['no_bpjs']        = $this->input->post('bpjs');
                $data['kategori']       = $this->input->post('kategori');
                $data['tgl_berlaku']    = strtoupper(date($this->input->post('period')));
                // $data['tgl_insert']     = strtoupper(date('d-m-Y'));
                $data['nip_pet_entri']  = $this->cu->NIP;
                $data['nama_pet_entri'] = $this->cu->NAMA_LENGKAP;
                 // var_dump($data);die;
                //--cek if data exist
                $this->load->model('model_plus');
                $check = $this->model_plus->check_bpjs($nik);
                if ($check) {
                    // $this->load->model('model_plus');
                    $update = $this->model_plus->update_bpjs($nik,$data);
                } else {
                    // $this->load->model('model_plus');
                    $insert = $this->model_plus->insert_bpjs($data);
                }
            }
            
            if($this->input->post('hapus'))
            {
                $nik          = $this->input->post('hapus');

                $this->load->model('model_plus');
                $delete = $this->model_plus->delete_bpjs($nik);        
            }

            $this->session->set_flashdata('no_kk', $no_kk);
            redirect('bpjs/lihat');
            exit;
        }

    }

    /*public function hapus()
    {
        if($this->input->post())
        {
            $nik          = $this->input->post('submit');
            $no_kk          = $this->input->post('no_kk');

            $this->load->model('model_plus');
            $delete = $this->model_plus->delete_bpjs($data);
            
            $this->session->set_flashdata('no_kk', $no_kk);
            redirect('bpjs/lihat');
            exit;
        }

    }*/
}