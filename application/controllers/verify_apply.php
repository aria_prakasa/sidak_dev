<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verify_apply extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->curpage = $this->router->fetch_class();        
        // $this->model_security->getsecurity();
    }

    public function index($a = "")
    {
        $isi['content']   = 'verify/main_verify';
        $isi['judul']     = 'Verifikasi';
        $isi['sub_judul'] = 'Permohonan Data';
        //$isi['data_nav']    = 'active';
        $isi['verify_nav'] = 'active';
        $isi['apply_nav']  = 'active';

        if ($a == "export") {

            $this->load->model('vdata_induk_mohon');
            $where = $this->OU->get_filter_location_where();
            // $where = ""; // get all for testing only
            $data_mohon = $this->vdata_induk_mohon->get_list(0, 0, $orderby = "", $where);
            // EXPORT TO EXCEL AND DOWNLOAD
            $header = $body_arr = null;
            $header = array(
                "NO",
                "NO_KK",
                "NAMA_KEP",
                "NIK",
                "NAMA_LGKP",
                "STAT_HBKEL",
                "JENIS_KLMIN",
                "TMPT_LHR",
                "TGL_LHR",
                "STAT_KWN",
                "AGAMA",
                "PDDK_AKH",
                "JENIS_PKRJN",
                "ALAMAT",
                "NO_RT",
                "NO_RW",
                "NAMA_KEL",
                "NAMA_KEC",
                "NAMA_KAB",
                "NAMA_PROP",
            );

            $no   = 1;
            $Vtmp = new Vdata_induk_mohon;
            foreach ($data_mohon as $row) {
                extract((array) $row);
                $Vtmp->setup($row);
                $body_arr[] = array(
                    $no,
                    "'" . $NO_KK,
                    $NAMA_KEP,
                    "'" . $NIK,
                    $NAMA_LGKP,
                    $STAT_HBKEL,
                    $JENIS_KLMIN,
                    $TMPT_LHR,
                    $Vtmp->get_dob(),
                    $STAT_KWN,
                    $AGAMA,
                    $PDDK_AKH,
                    $JENIS_PKRJN,
                    $ALAMAT,
                    $NO_RT,
                    $NO_RW,
                    $NAMA_KEL,
                    $NAMA_KEC,
                    $NAMA_KAB,
                    $NAMA_PROP,
                );
                $no++;
            }
            unset($data_mohon);
            $this->load->helper('excel');
            $filename = "data-permohonan-kemiskinan--" . date("Ym") . '.csv';
            array_to_excel($filename, $header, $body_arr, "xls");
            exit;
        }

        // $this->load->model('model_verify');
        // $isi['data_mohon'] = $data_mohon = $this->model_verify->getdatamohon();
        $this->load->model('vdata_mohon');
        $isi['data_mohon'] = $data_mohon = $this->vdata_mohon->get_list(0, 0, $orderby = "", $where);
        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']   = 'verify/verify_view';
        $isi['judul']     = 'Data Kemiskinan';
        $isi['sub_judul'] = 'Verifikasi Permohonan Data';
        //$isi['data_nav']    = 'active';
        $isi['verify_nav'] = 'active';
        $isi['apply_nav']  = 'active';

        $no_kk = $this->uri->segment(3);
        $this->load->model('model_verify');
        $data_mohon    = $this->model_verify->getdatatinggal($no_kk);
        $anggota_mohon = $this->model_verify->getdataanggota($no_kk);
        if (!$data_mohon) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('verify_apply', "refresh");
            exit;
        } else {
            //die(var_dump($data_kk));
            //die(var_dump($anggota_kk)); //dd($data_kk);
            foreach ($data_mohon as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_mohon'] = $anggota_mohon;
        }

        $this->load->view('home_view', $isi);
    }

    public function simpan()
    {
        $no_kk = $this->input->post('no_kk');
        
        if($this->input->post('check'))
        {
            $nik = array_keys($this->input->post('check'));

            // var_dump($no_kk,$nik);die;
            
            $this->load->model('model_verify');
            $biodata_mohon = $this->model_verify->getbiodata_mohon($no_kk);
            
            if ($biodata_mohon) {
                $datapddk = null;
                foreach ($biodata_mohon as $row) {
                    if(!in_array($row->nik, $nik)) {
                        continue;
                    }
                    $datapddk[] = array(
                        'nik'            => $row->nik,
                        'no_kk'          => $row->no_kk,
                        'nama_lgkp'      => $row->nama_lgkp,
                        'jenis_pkrjn'    => $this->input->post('jenis_pkrjn')[$row->nik],
                        'nip_pet_entri'  => $this->cu->NIP,
                        'nama_pet_entri' => $this->cu->NAMA_LENGKAP
                    );
                }
                // var_dump($datapddk);die;
            }

            $this->load->model('model_pullout');
            if($insert = $this->model_pullout->insert_valid($datapddk))
            {
                $this->load->model('model_verify');
                $insert = $this->model_verify->insert_datapddk($datapddk);
            } else {
                die("Koneksi VPN gagal!");
            }
        }

        $check = $this->model_verify->getdatatinggal($no_kk);
        if($check)
        {
            $this->session->set_flashdata('no_kk', $no_kk);
            redirect('verify_apply/lihat/' . $no_kk);
            exit;
        }
        redirect('verify_apply/index');
        exit;
    }

    public function hapus()
    {
        $no_kk = $this->uri->segment(3);
        $this->load->model('model_entry');
        $biodata = $this->model_entry->getbiodata_mohon($no_kk);
        if ($biodata) {
            $this->load->model('model_pullout');
            if($this->model_pullout->deleteMohon($no_kk))
            {
                $this->model_entry->getdelete($no_kk);                
            } else {
                die("Koneksi VPN gagal!");
            }
        }
        //var_dump($no_kk);die;
        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('verify_apply');
        exit;
    }

    public function lihat_biodata()
    {
        $isi['content']    = 'data/biodata_view';
        $isi['judul']      = 'Pemasukan Data';
        $isi['sub_judul']  = 'Entri';
        $isi['data_nav']   = 'active';
        $isi['entri_nav']  = 'active';
        $isi['controller'] = 'verify_apply';

        if ($this->uri->segment(4)) {
            $nik = $this->uri->segment(4);
        } else {
            $nik = $this->session->flashdata('nik');
        }

        $this->load->model('model_entry');
        //$data_kk = $this->model_entry->getdatakk($no_kk);
        $biodata    = $this->model_entry->getbiodata($nik);
        $bio_miskin = $this->model_entry->getbiodata($nik);
        //die(var_dump($data_kk, $anggota_kk));
        if (!$biodata) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($biodata as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $bio_miskin = $this->model_entry->check_dataMiskin($nik);

        foreach ($bio_miskin as $key => $value) {
            $isi[$key] = $value;
        }

        $this->load->view('home_view', $isi);
    }

    public function simpan_biodata()
    {
        $no_kk              = $this->uri->segment(3);
        $nik                = $this->uri->segment(4);
        $datamskn['nik']    = $nik;
        $datamskn['field1'] = strtoupper($this->input->post('field1'));
        $datamskn['field2'] = strtoupper($this->input->post('field2'));
        $datamskn['field3'] = strtoupper($this->input->post('field3'));
        $datamskn['field4'] = strtoupper($this->input->post('field4'));

        //--cek if data exist
        $this->load->model('model_entry');
        $check = $this->model_entry->check_dataMiskin($nik);
        if ($check) {
            $this->load->model('model_entry');
            $update = $this->model_entry->update_dataMiskin($datamskn);
        } else {
            $this->load->model('model_entry');
            $insert = $this->model_entry->insert_dataMiskin($datamskn);
        }

        // var_dump($datamskn['field1'],$datamskn['field2'],$datamskn['field3'],$datamskn['field4'],$no_kk,$datamskn['nik']);die;

        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('verify_apply/lihat');
        exit;
        //}
    }
}
