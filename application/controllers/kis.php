<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kis extends MY_Controller {
    public function __construct() {
        parent::__construct();
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        // $this->model_security->getsecurity();
        $isi['content']   = 'Plus/kis';
        $isi['judul']     = 'Entri Kemiskinan Plus';
        $isi['sub_judul'] = 'Kartu Indonesia Sehat';
        $isi['plus_nav']  = 'active';
        $isi['kis_nav'] = 'active';

        $this->load->view('home_view', $isi);
    }
    
}