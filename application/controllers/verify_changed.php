<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verify_changed extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        $isi['content']   = 'verify/main_change';
        $isi['judul']     = 'Verifikasi';
        $isi['sub_judul'] = 'Perubahan Data';
        //$isi['data_nav']    = 'active';
        $isi['verify_nav'] = 'active';
        $isi['change_nav'] = 'active';


        $this->load->model('vbiodata_change');
        $where = 'CHANGE_STATUS IS NOT NULL';
        $isi['biodata_change'] = $biodata_change = $this->vbiodata_change->get_list(0, 0, $orderby = "", $where);

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']   = 'verify/change_view';
        $isi['judul']     = 'Verifikasi';
        $isi['sub_judul'] = 'Perubahan Data';
        //$isi['data_nav']    = 'active';
        $isi['verify_nav'] = 'active';
        $isi['change_nav'] = 'active';

        $no_kk = $this->uri->segment(3);
        $this->load->model('model_list');
        $data_list    = $this->model_list->getdata($no_kk);
        $anggota_list = $this->model_list->getanggota($no_kk);
        // var_dump($data_list);die;
        if (!$data_list) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('Verify_changed', "refresh");
            exit;
        } else {
            //die(var_dump($data_kk));
            //die(var_dump($anggota_kk)); //dd($data_kk);
            foreach ($data_list as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_list'] = $anggota_list;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat_baru()
    {
        $isi['content']   = 'verify/new_kk_view';
        $isi['judul']     = 'Verifikasi';
        $isi['sub_judul'] = 'Perubahan Data';
        //$isi['data_nav']    = 'active';
        $isi['verify_nav'] = 'active';
        $isi['change_nav'] = 'active';

        $no_kk = $this->uri->segment(3);
        $this->load->model('model_entry');
        $check_kk = $this->model_entry->getdatamohon($no_kk);
        $data_kk    = $this->model_entry->getdatakk($no_kk);
        
        if (!$check_kk) {
            $anggota_kk = $this->model_entry->getanggotakk($no_kk);
        } else {
            $anggota_kk = $this->model_entry->getanggotamohon($no_kk);
        }

        if (!$data_kk) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            //die(var_dump($data_kk));
            //die(var_dump($anggota_kk)); //dd($data_kk);
            foreach ($data_kk as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_kk'] = $anggota_kk;
            //$isi['pekerjaan'] = $pekerjaan;
        }

        $this->load->view('home_view', $isi);
    }


    public function hapus()
    {
        $nik = $this->uri->segment(3);
        $this->load->model('model_list');
        $this->load->model('model_entry');
        $biodata = $this->model_list->getbiodata($nik);
         // var_dump($biodata);die;
        if ($biodata) {
            $this->load->model('model_pullout');
            if($this->model_pullout->biodeleteMohon($nik))
            {
                $this->model_pullout->biodeleteValid($nik);
                $this->model_list->get_biodelete($nik);
                $this->model_entry->get_biodelete($nik);
            } 
            // $this->model_list->set_delete($no_kk);
        } else {
            die("Koneksi VPN gagal!");
        }

        // $this->session->set_flashdata('no_kk', $no_kk);
        redirect('Verify_changed');
        exit;
    }
}
