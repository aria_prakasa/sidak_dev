<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agr_miskin extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// check login user
		$this->_init_logged_in();
		// $this->model_security->getsecurity();
		$this->load->model('model_agregat_mskn', '', true);
	}

	public function index() {
		// $this->model_security->getsecurity();
		$isi['content'] = 'agregat/main_miskin';
		$isi['judul'] = 'Statistik';
		$isi['sub_judul'] = 'Agregat Data Miskin';
		$isi['agr_nav'] = 'active';
		$isi['agr_miskin_nav'] = 'active';
		// $isi['agr_nav_tabs']    = 'active';

		// $isi['provinsi']    = $this->Model_wilayah->ambil_provinsi();
		// $isi['kabupaten']    = $this->Model_wilayah->ambil_kabupaten();
		// $isi['kecamatan']    = $this->Model_wilayah->ambil_kecamatan();
		if ($this->input->post()) {
			$isi = array_merge($isi, $this->input->post());
		}
		if ($this->input->post('provinsi_id') && $this->input->post('kabupaten_id')) {
			$kd_prop = $this->input->post('provinsi_id');
			$kd_kab = $this->input->post('kabupaten_id');
			$kd_kec = $this->input->post('kecamatan_id');

			if ($this->input->post('kelurahan_id')) {
				$kelurahan_code = $this->input->post('kelurahan_id'); // 03|1001
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				$isi['filter_level'] = 2;
				$isi['pane_nav'] = 'hide';
				$isi['kel_active'] = 'active';
				// var_dump($kd_kel);die;
			} else {
				$isi['kec_active'] = 'active';
			}
			// var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;
			$kd_agr = $this->input->post('kd_agr');

			if ($kd_agr == 1) {
				$isi['agregat'] = $this->model_agregat_mskn->getagrMiskin($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				//var_dump($isi['agregat']);die;
			}
			if ($kd_agr == 2) {
				$isi['agregatAgama'] = $this->model_agregat_mskn->getagrAgama_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers']      = $this->model_header->getheaderAgama();
				// var_dump($isi['agregatAgama']);die;
			}
			if ($kd_agr == 3) {
				$isi['agregatGoldrh'] = $this->model_agregat_mskn->getagrGoldrh_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				//$isi['headers'] = $this->model_header->getheaderGoldrh();
			}
			if ($kd_agr == 4) {
				$isi['agregatPddkan'] = $this->model_agregat_mskn->getagrPddkan_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers'] = $this->model_header->getheaderPddkan();
				//var_dump($isi['agregatPddkan']);die;
			}
			if ($kd_agr == 5) {
				$isi['agregatSHDK'] = $this->model_agregat_mskn->getagrSHDK_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers'] = $this->model_header->getheaderPddkan();
				//var_dump($isi['agregatPddkan']);die;
			}
			if ($kd_agr == 6) {
				$isi['agregatUmur'] = $this->model_agregat_mskn->getagrUmur_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				$isi['kec_active'] = 'active';
				unset($isi['pane_nav']);
				unset($isi['kel_active']);
			}
			if ($kd_agr == 7) {
				$isi['agregatUmur'] = $this->model_agregat_mskn->getagrUmurBppd_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				$isi['kec_active'] = 'active';
				unset($isi['pane_nav']);
				unset($isi['kel_active']);
			}
			if ($kd_agr == 8) {
				$isi['agregat'] = $this->model_agregat_mskn->getagrWajibktp_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 9) {
				$isi['agregatPkrjn'] = $this->model_agregat_mskn->getagrPkrjn_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 10) {
				$isi['agregatPkrjn'] = $this->model_agregat_mskn->getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 11) {
				$isi['agregat'] = $this->model_agregat_mskn->getagrBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			// var_dump($isi);die;
		}

		$this->load->view('home_view', $isi);
	}

	/*// dijalankan saat provinsi di klik
		public function pilih_kabupaten($id=0){
		//var_dump($this->uri->segment(3));die;
		$isi['kabupaten']=$this->Model_wilayah->ambil_kabupaten($id);
		$this->load->view('v_drop_down_kabupaten',$isi);
		//var_dump($isi['kabupaten']);die;
		}

		// dijalankan saat kabupaten di klik
		public function pilih_kecamatan($id=0){
		$isi['kecamatan']=$this->Model_wilayah->ambil_kecamatan($id);
		$this->load->view('v_drop_down_kecamatan',$isi);
		}

		// dijalankan saat kecamatan di klik
		public function pilih_kelurahan($id=0){
		$isi['kelurahan']=$this->Model_wilayah->ambil_kelurahan($id);
		$this->load->view('v_drop_down_kelurahan',$isi);
	*/
}
