<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        /*$isi['content']     = 'home/dashboard';
        $isi['judul']        = 'Halaman Utama';
        $isi['sub_judul']    = '';
        $this->load->view('welcome',$isi);*/

        $isi['content']   = 'home/dashboard';
        $isi['judul']     = 'Halaman Utama';
        $isi['sub_judul'] = 'Dashboard';
        $isi['home_nav']  = 'active';
        $isi['dash_nav']  = 'active';

        $this->load->model('model_home');
        $data_kel      = $this->model_home->getheaderPddk();
        $bio_wni       = $this->model_home->getdetailPddk();
        $data_mohon    = $this->model_home->getmohonMiskin();
        $bio_pddk_mskn = $this->model_home->getdetailMiskin();

        foreach ($data_kel as $key => $value) {
            $isi[$key] = $value;
        }

        foreach ($bio_wni as $key => $value) {
            $isi[$key] = $value;
        }

        foreach ($data_mohon as $key => $value) {
            $isi[$key] = $value;
        }

        foreach ($bio_pddk_mskn as $key => $value) {
            $isi[$key] = $value;
        }
        // var_dump($isi['bio_wni']);die;

        $this->load->view('home_view', $isi);

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}
