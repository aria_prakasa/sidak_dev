<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Listing extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// error_reporting(-1);
		// $this->output->enable_profiler(TRUE);
		// check login user
		$this->_init_logged_in();
		$this->curpage = $this->router->fetch_class();
		// $this->model_security->getsecurity();
	}

	public function index($a = "") {
		$isi['content'] = 'list/main_view';
		$isi['judul'] = 'Daftar Penduduk Miskin';
		$isi['sub_judul'] = 'List Data Keluarga';
		$isi['daftar_nav'] = 'active';
		$isi['list_nav'] = 'active';

		// iki link tapi gak fitler sama sekali
		if ($a == "export") {

			$this->load->model('vdata_induk_miskin');
			$where = $this->OU->get_filter_location_where();
			if ($this->input->get()) {
				$where = get_filter_location_where($this->input->get());
			}
			// $where = ""; // get all for testing only
			$data_list = $this->vdata_induk_miskin->get_list(0, 0, $orderby = "", $where);
			// EXPORT TO EXCEL AND DOWNLOAD
			$header = $body_arr = null;
			$header = array(
				"NO",
				"NO_KK",
				"NAMA_KEP",
				"NIK",
				"NAMA_LGKP",
				"STAT_HBKEL",
				"JENIS_KLMIN",
				"TMPT_LHR",
				"TGL_LHR",
				"STAT_KWN",
				"AGAMA",
				"PDDK_AKH",
				"JENIS_PKRJN",
				"ALAMAT",
				"NO_RT",
				"NO_RW",
				"NAMA_KEL",
				"NAMA_KEC",
				"NAMA_KAB",
				"NAMA_PROP",
			);

			$no = 1;
			$Vtmp = new Vdata_induk_miskin;
			foreach ($data_list as $row) {
				$Vtmp->setup($row);
				extract((array) $row);
				// var_dump($Vtmp->get_dob()); die;
				$body_arr[] = array(
					$no,
					"'" . $NO_KK,
					$NAMA_KEP,
					"'" . $NIK,
					$NAMA_LGKP,
					$STAT_HBKEL,
					$JENIS_KLMIN,
					$TMPT_LHR,
					$TGL_LHR,
					$STAT_KWN,
					$AGAMA,
					$PDDK_AKH,
					$JENIS_PKRJN,
					$ALAMAT,
					$NO_RT,
					$NO_RW,
					$NAMA_KEL,
					$NAMA_KEC,
					$NAMA_KAB,
					$NAMA_PROP,
				);
				$no++;
			}
			unset($data_mohon);
			$this->load->helper('excel');
			$filename = "data-kemiskinan-valid--" . date("Ym") . '.csv';
			array_to_excel($filename, $header, $body_arr, "xls");
			exit;
		}

		if ($this->uri->segment(3) == '1') {
			$isi = array_merge($isi, $this->session->userdata($search_sess));
			// var_dump($isi);die;
			$kd_prop = $this->session->userdata('provinsi_id');
			$kd_kab = $this->session->userdata('kabupaten_id');
			$kd_kec = $this->session->userdata('kecamatan_id');
			$kel = $this->session->userdata('kelurahan_id');
			// var_dump($kel);die;
			if ($kel) {
				$kelurahan_code = $this->session->userdata('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				// var_dump($kd_kel);die;
			}
			// var_dump($isi);die;

			if ($this->session->userdata('inputkk')) {
				$no_kk = $this->session->userdata('inputkk');
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";
				// $isi['jml_data'] = 1;
			}

			if ($this->session->userdata('inputnama')) {
				$n = $this->session->userdata('inputnama');
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['toggleNama'] = "checked";
				// $isi['jml_data'] = 2;
				// $isi['n']        = $n;
			}

			if ($entri) {
				$this->load->model('model_list');
				$isi['data_list'] = $this->model_list->getdataValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
				// var_dump($isi['data_list']);die;
				$isi['jml_data'] = 1;
			}

		}

		if ($this->input->post()) {

			$kd_prop = $this->input->post('provinsi_id');
			$kd_kab = $this->input->post('kabupaten_id');
			$kd_kec = $this->input->post('kecamatan_id');

			if ($this->input->post('kelurahan_id')) {
				$kelurahan_code = $this->input->post('kelurahan_id');
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];

			}

			if ($this->cu->USER_LEVEL == 2) {
				$kd_kec = $this->cu->NO_KEC;
			}
			if ($this->cu->USER_LEVEL == 3) {
				$kd_kec = $this->cu->NO_KEC;
				$kd_kel = $this->cu->NO_KEL;
			}

			$this->load->model('model_list');

			$isi = array_merge($isi, $this->input->post());

			if ($this->input->post('inputkk', TRUE)) {

				$no_kk = $this->input->post('inputkk', TRUE);
				$entri['no_kk'] = $no_kk;
				$isi['toggleKk'] = "checked";

			}

			if ($this->input->post('inputnama', TRUE)) {

				$n = $this->input->post('inputnama', TRUE);
				$nama = strtoupper($n);
				$entri['nama'] = $nama;
				$isi['n'] = $n;
				$isi['toggleNama'] = "checked";

			}

			// TAMPIL ONLY
			if ($this->input->post('tampil')) {
				// var_dump($entri);die;
				$isi['data_list'] = $this->model_list->getdataValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
				// var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel,$nik,$no_kk,$nama);die;
				$isi['jml_data'] = 1;

			}

			// EXPORT
			if ($this->input->post('export')) {

				// var_dump($this->input->post());die;

				// $where = $this->OU->get_filter_location_where();
				// if ($this->input->get()) {
				// 	$where = get_filter_location_where($this->input->get());
				// }
				// $where = ""; // get all for testing only
				$data_list = $this->model_list->getdataValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $entri);
				// $data_list = $this->vdata_induk_miskin->get_list(0, 0, $orderby = "", $where);

				// var_dump($data_list);die;
				// EXPORT TO EXCEL AND DOWNLOAD
				$header = $body_arr = null;
				$header = array(
					"NO",
					"NO_KK",
					"NAMA_KEP",
					"NIK",
					"NAMA_LGKP",
					"STAT_HBKEL",
					"JENIS_KLMIN",
					"TMPT_LHR",
					"TGL_LHR",
					"STAT_KWN",
					"AGAMA",
					"PDDK_AKH",
					"JENIS_PKRJN",
					"ALAMAT",
					"NO_RT",
					"NO_RW",
					"NAMA_KEL",
					"NAMA_KEC",
					"NAMA_KAB",
					"NAMA_PROP",
				);

				$no = 1;
				// $Vtmp = new Vdata_induk_miskin;
				foreach ($data_list as $row) {
					// $Vtmp->setup($row);
					extract((array) $row);
					// var_dump($Vtmp->get_dob()); die;
					$body_arr[] = array(
						$no,
						"'" . $NO_KK,
						$NAMA_KEP,
						"'" . $NIK,
						$NAMA_LGKP,
						$STAT_HBKEL,
						$JENIS_KLMIN,
						$TMPT_LHR,
						$TGL_LHR,
						$STAT_KWN,
						$AGAMA,
						$PDDK_AKH,
						$JENIS_PKRJN,
						$ALAMAT,
						$NO_RT,
						$NO_RW,
						$NAMA_KEL,
						$NAMA_KEC,
						$NAMA_KAB,
						$NAMA_PROP,
					);
					$no++;
				}
				// var_dump($header, $body_arr);die;
				$this->load->helper('excel');
				$filename = "data-kemiskinan-valid--" . date("Ym") . '.csv';
				array_to_excel($filename, $header, $body_arr, "xls");
				exit;
			}

			$this->session->unset_userdata('inputnama');
			// $this->session->unset_userdata('inputnik');
			$this->session->unset_userdata('inputkk');
			$this->session->unset_userdata('toggle');
			$search_sess = array_merge($this->input->post());
			$this->session->set_userdata($search_sess);
		}
		/*$this->load->model('model_list');
			        $isi['data_list'] = $data_list = $this->model_list->getdatavalid();
			        $this->load->model('vdata_valid');
			        $isi['data_list'] = $data_list = $this->vdata_valid->get_list(0, 0, $orderby = "", $where);
		*/
		$this->load->view('home_view', $isi);
	}

	public function lihat() {
		$isi['content'] = 'list/detail_view';
		$isi['judul'] = 'Daftar Kemiskinan';
		$isi['sub_judul'] = 'List Data';
		$isi['daftar_nav'] = 'active';
		$isi['list_nav'] = 'active';

		if ($this->uri->segment(3)) {
			$no_kk = $this->uri->segment(3);
		} else {
			$no_kk = $this->session->flashdata('no_kk');
		}
		//var_dump($no_kk);die;
		$this->load->model('model_list');
		$data_list = $this->model_list->getdata($no_kk);
		// var_dump($data_list);die;
		$anggota_list = $this->model_list->getanggota($no_kk);
		// var_dump($data_list);die;
		if (!$data_list) {
			$this->session->set_flashdata('info', "ERROR");
			redirect('listing', "refresh");
			exit;
		} else {
			//die(var_dump($data_kk));
			//die(var_dump($anggota_kk)); //dd($data_kk);
			foreach ($data_list as $key => $value) {
				$isi[$key] = $value;
			}
			$isi['anggota_list'] = $anggota_list;
		}

		$this->load->view('home_view', $isi);
	}

	public function proses() {
		if ($this->input->post()) {
			// var_dump($this->input->post()); die;
			$no_kk = $this->input->post('no_kk');
			// var_dump($this->input->post()); die;
			if ($this->input->post('submit')) {
				$nik = $this->input->post('submit');
				$jenis_pkrjn = $this->input->post('jenis_pkrjn');
				// $tgl_entri      = strtoupper(date('d-m-Y'));
				$nip_pet_entri = $this->cu->NIP;
				$nama_pet_entri = $this->cu->NAMA_LENGKAP;

				// var_dump($nik,$no_kk,$jenis_pkrjn,$nip_pet_entri,$nama_pet_entri);die;

				// $datavalid['nik']               = $nik;
				$datavalid['jenis_pkrjn'] = strtoupper($this->input->post('jenis_pkrjn')[$nik]);
				$datavalid['tgl_update'] = strtoupper(date('d-m-Y'));
				$datavalid['nip_pet_update'] = $this->cu->NIP;
				$datavalid['nama_pet_update'] = $this->cu->NAMA_LENGKAP;

				// var_dump($datavalid, $nik); die;

				$this->load->model('model_pullout');
				if ($this->model_pullout->update_dataValid($nik, $datavalid)) {
					$this->load->model('model_list');
					$update = $this->model_list->update_dataValid($nik, $datavalid);
				} else {
					die("Koneksi VPN gagal!");
				}
			}

			if ($this->input->post('hapus')) {
				$nik = $this->input->post('hapus');
				$this->load->model('model_list');
				$this->load->model('model_entry');
				//var_dump($this->input->post()); die;
				$biodata = $this->model_list->getbiodata($nik);
				// var_dump($biodata);die;
				if ($biodata) {
					$this->load->model('model_pullout');
					if ($this->model_pullout->biodeleteMohon($nik)) {
						$this->model_pullout->biodeleteValid($nik);
						$this->model_list->get_biodelete($nik);
						$this->model_entry->get_biodelete($nik);
					}
				} else {
					die("Koneksi VPN gagal!");
				}
			}
		}

		$this->session->set_flashdata('no_kk', $no_kk);
		redirect('listing/lihat');
		exit;
		//}
	}

	public function hapus() {
		if ($this->uri->segment(3)) {
			$no_kk = $this->uri->segment(3);
			$this->load->model('model_list');
			$this->load->model('model_entry');
			$biodata = $this->model_list->getdata($no_kk);
			// var_dump($biodata);die;
			if ($biodata) {
				$this->load->model('model_pullout');
				if ($this->model_pullout->deleteMohon($no_kk)) {
					$this->model_pullout->deleteValid($no_kk);
					$this->model_list->getdelete($no_kk);
					$this->model_entry->getdelete($no_kk);
				}
				// $this->model_list->set_delete($no_kk);
			} else {
				die("Koneksi VPN gagal!");
			}

			$this->session->set_flashdata('no_kk', $no_kk);
			redirect('listing');
			exit;
		}
	}

	public function lihat_biodata() {
		$isi['content'] = 'data/biodata_view';
		$isi['judul'] = 'Pemasukan Data';
		$isi['sub_judul'] = 'Entri';
		$isi['data_nav'] = 'active';
		$isi['entri_nav'] = 'active';
		$isi['controller'] = 'listing';

		$nik = $this->uri->segment(3);
		//var_dump($no_kk);die;
		$this->load->model('model_list');
		$biodata = $this->model_list->getbiodata($nik);

		if (!$biodata) {
			$this->session->set_flashdata('info', "ERROR");
			redirect('listing_biodata', "refresh");
			exit;
		} else {
			//die(var_dump($data_kk));
			//die(var_dump($anggota_kk)); //dd($data_kk);
			foreach ($biodata as $key => $value) {
				$isi[$key] = $value;
			}
		}

		$bio_miskin = $this->model_list->getcatatan_miskin($nik);

		foreach ($bio_miskin as $key => $value) {
			$isi[$key] = $value;
		}

		$this->load->view('home_view', $isi);
	}

	public function simpan_biodata() {
		$no_kk = $this->uri->segment(3);
		$nik = $this->uri->segment(4);
		$datamskn['nik'] = $nik;
		$datamskn['field1'] = strtoupper($this->input->post('field1'));
		$datamskn['field2'] = strtoupper($this->input->post('field2'));
		$datamskn['field3'] = strtoupper($this->input->post('field3'));
		$datamskn['field4'] = strtoupper($this->input->post('field4'));

		//--cek if data exist
		$this->load->model('model_entry');
		$check = $this->model_entry->check_dataMiskin($nik);
		if ($check) {
			$this->load->model('model_entry');
			$update = $this->model_entry->update_dataMiskin($datamskn);
		} else {
			$this->load->model('model_entry');
			$insert = $this->model_entry->insert_dataMiskin($datamskn);
		}

		// var_dump($datamskn['field1'],$datamskn['field2'],$datamskn['field3'],$datamskn['field4'],$no_kk,$datamskn['nik']);die;

		$this->session->set_flashdata('no_kk', $no_kk);
		redirect('apply/lihat');
		exit;
		//}
	}
}
