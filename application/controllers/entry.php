<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entry extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        $isi['content']   = 'data/main_entry';
        $isi['judul']     = 'Pemasukan Data';
        $isi['sub_judul'] = 'Entri';
        $isi['data_nav']  = 'active';
        $isi['entri_nav'] = 'active';

        
        if ($this->uri->segment(3) == '1') {
            $isi = array_merge($isi, $this->session->userdata($search_sess));
           
             // var_dump($isi);die;
            $kd_prop    = $this->session->userdata('provinsi_id');
            $kd_kab     = $this->session->userdata('kabupaten_id');
            $kd_kec     = $this->session->userdata('kecamatan_id');
            $kel        = $this->session->userdata('kelurahan_id');
            // var_dump($kel);die;   
            if ($kel) {
                $kelurahan_code     = $this->session->userdata('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
             // var_dump($kd_kel);die;   
            }
            // var_dump($isi);die;

            if ($this->session->userdata('inputkk')) {
                $no_kk = $this->session->userdata('inputkk');
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                // $isi['jml_data'] = 1;
            }

            if ($this->session->userdata('inputnik')) {
                $nik = $this->session->userdata('inputnik');
                $entri['nik']   = $nik;
                $isi['toggleNik'] = "checked";
                // $isi['jml_data'] = 1;
            }

            if ($this->session->userdata('inputnama')) {
                $n    = $this->session->userdata('inputnama');
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['toggleNama'] = "checked";
                // $isi['jml_data'] = 2;
                // $isi['n']        = $n;
            }

            // var_dump($entri);die;
            if($entri)
            {
                $this->load->model('model_entry');
            // $data_kk = $this->model_entry->getdatakk($kk,$no_kec,$no_kel);
                $isi['data_kk'] = $this->model_entry->getEntri($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
                $isi['jml_data'] = 1;
            }
            
            // var_dump($isi['date(format)a_kk']);die;
            // $this->session->unset_userdata('inputnama');
            // $this->session->unset_userdata('inputnik');
            // $this->session->unset_userdata('inputkk');
            // $this->session->unset_userdata('toggle');
        }   

        if ($this->input->post('inputkk') || $this->input->post('inputnik') || $this->input->post('inputnama')) {
            // var_dump($this->input->post());die;
            $isi        = array_merge($isi, $this->input->post());
            $kd_prop    = $this->input->post('provinsi_id');
            $kd_kab     = $this->input->post('kabupaten_id');

            if ($this->input->post('kecamatan_id')) {
                $kd_kec     = $this->input->post('kecamatan_id');
            }

            if ($this->input->post('kelurahan_id')) {
                $kelurahan_code     = $this->input->post('kelurahan_id'); 
                $kelurahan_code_arr = explode("-", $kelurahan_code);
                $kd_kec             = $kelurahan_code_arr[0];
                $kd_kel             = $kelurahan_code_arr[1];
                
            }

            if($this->cu->USER_LEVEL == 2)
                {
                    $kd_kec = $this->cu->NO_KEC;
                }
                if($this->cu->USER_LEVEL == 3)
                {
                    $kd_kec = $this->cu->NO_KEC;
                    $kd_kel = $this->cu->NO_KEL;
                }

            if ($this->input->post('inputkk',TRUE)) {

                $no_kk = $this->input->post('inputkk',TRUE);
                $entri['no_kk']   = $no_kk;
                $isi['toggleKk'] = "checked";
                
            }
            if ($this->input->post('inputnik',TRUE)) {

                $nik = $this->input->post('inputnik',TRUE);
                $entri['nik']   = $nik;
                $isi['toggleNik'] = "checked";
                
            }
            if ($this->input->post('inputnama',TRUE)) {

                $n    = $this->input->post('inputnama',TRUE);
                $nama = strtoupper($n);
                $entri['nama']   = $nama;
                $isi['n']        = $n;
                $isi['toggleNama'] = "checked";
                
            }
            // var_dump($entri);die;
            $this->load->model('model_entry');
            // $data_kk = $this->model_entry->getdatakk($kk,$no_kec,$no_kel);
            $isi['data_kk'] = $this->model_entry->getEntri($kd_prop,$kd_kab,$kd_kec,$kd_kel,$entri);
            $isi['jml_data'] = 1;
            
            $this->session->unset_userdata('inputnama');
            $this->session->unset_userdata('inputnik');
            $this->session->unset_userdata('inputkk');
            $this->session->unset_userdata('toggle');
            $search_sess = array_merge($this->input->post());
            $this->session->set_userdata($search_sess);
            // var_dump($isi['biodata_list']);die;
            
        }
        /*else
        {
            $this->session->unset_userdata('nama');
            $this->session->unset_userdata('nik');
            $this->session->unset_userdata('kk');           
        }*/
        // var_dump($data_kk);die;
        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']   = 'data/entry';
        $isi['judul']     = 'Pemasukan Data';
        $isi['sub_judul'] = 'Entri';
        $isi['data_nav']  = 'active';
        $isi['entri_nav'] = 'active';

        if ($this->uri->segment(3)) {
            $no_kk = $this->uri->segment(3);
        } else {
            $no_kk = $this->session->flashdata('no_kk');
        }

        if($this->cu->USER_LEVEL == 2)
        {
            $no_kec = $this->cu->NO_KEC;
        }
        if($this->cu->USER_LEVEL == 3)
        {
            $no_kec = $this->cu->NO_KEC;
            $no_kel = $this->cu->NO_KEL;
        }

        $this->load->model('model_entry');
        $check_kk = $this->model_entry->getdatamohon($no_kk);
        $data_kk    = $this->model_entry->getdatakk($no_kk,$no_kec,$no_kel);
        
        // if (!$check_kk) {
            $anggota_kk = $this->model_entry->getanggotakk($no_kk);
        /*} else {
            $anggota_kk = $this->model_entry->getanggotamohon($no_kk);
        }*/

        if (!$data_kk) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            //die(var_dump($data_kk));
            //die(var_dump($anggota_kk)); //dd($data_kk);
            foreach ($data_kk as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_kk'] = $anggota_kk;
            //$isi['pekerjaan'] = $pekerjaan;
        }

        $this->load->view('home_view', $isi);
    }

    public function simpan()
    {
        // var_dump($this->input->post());die;
        $this->session->set_userdata($search_sess);
        $no_kk = $this->input->post('no_kk');

        if($this->input->post('check'))
        {
            // var_dump(array_keys($this->input->post('check'))); die;
            $nik = array_keys($this->input->post('check'));
            
            $this->load->model('model_entry');
            $biodata = $this->model_entry->getbiodata_wni($no_kk);
            
            if ($biodata) {
                $datapddk = null;
                foreach ($biodata as $row) {
                    if(!in_array($row->nik, $nik)) {
                        continue;
                    }
                    $datapddk[] = array(
                        'nik'            => $row->nik,
                        'no_kk'          => $row->no_kk,
                        'nama_lgkp'      => $row->nama_lgkp,
                        'jenis_pkrjn'    => $this->input->post('jenis_pkrjn')[$row->nik],
                        'nip_pet_entri'  => $this->cu->NIP,
                        'nama_pet_entri' => $this->cu->NAMA_LENGKAP
                    );
                }
                // var_dump($datapddk);die;
            }
            $this->load->model('model_pullout');
            if($insert = $this->model_pullout->insert_mohon($datapddk))
            {
                $this->load->model('model_entry');
                $insert = $this->model_entry->insert_datapddk($datapddk);
            } else {
                die("Koneksi VPN gagal!");
            }

        }
        
        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('entry/lihat/' . $no_kk);
        exit;
        //}

    }

    public function edit()
    {
        $isi['content']   = 'data/edit';
        $isi['judul']     = 'Data Kemiskinan';
        $isi['sub_judul'] = 'Entri Data';
        $isi['data_nav']  = 'active';
        $isi['entri_nav'] = 'active';

        $nik = $this->uri->segment(3);
        $this->load->model('model_entry');
        $mohon = $this->model_entry->getpermohonan($nik);

        foreach ($mohon as $key => $value) {
            $isi[$key] = $value;
        }

        $this->load->view('home_view', $isi);
    }

    /*public function hapus()
    {

        $no_kk = $this->uri->segment(3);
        // var_dump($no_kk);die;
        $this->load->model('model_entry');
        $biodata = $this->model_entry->getbiodata_mohon($no_kk);
        // var_dump($biodata);die;
        if ($biodata) {
            $this->load->model('model_pullout');
            if($this->model_pullout->biodeleteMohon($nik))
            {
                $this->model_entry->get_biodelete($nik);
            } else {
                die("Koneksi VPN gagal!");
            }
        }

        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('entry/lihat/' . $no_kk . '/' . $val);
        // redirect('entry/lihat');
        exit;
    }*/

    public function lihat_biodata()
    {
        $isi['content']    = 'data/biodata_view';
        $isi['judul']      = 'Pemasukan Data';
        $isi['sub_judul']  = 'Entri';
        $isi['data_nav']   = 'active';
        $isi['entri_nav']  = 'active';
        $isi['controller'] = 'entry';

        if ($this->uri->segment(4)) {
            $nik = $this->uri->segment(4);
        } else {
            $nik = $this->session->flashdata('nik');
        }

        $this->load->model('model_entry');
        //$data_kk = $this->model_entry->getdatakk($no_kk);
        $biodata    = $this->model_entry->getbiodata($nik);
        $bio_miskin = $this->model_entry->getbiodata($nik);
        //die(var_dump($data_kk, $anggota_kk));
        if (!$biodata) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($biodata as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $bio_miskin = $this->model_entry->check_dataMiskin($nik);

        foreach ($bio_miskin as $key => $value) {
            $isi[$key] = $value;
        }

        $this->load->view('home_view', $isi);
    }

    public function simpan_biodata()
    {
        $no_kk              = $this->uri->segment(3);
        $nik                = $this->uri->segment(4);
        $datamskn['nik']    = $nik;
        $datamskn['field1'] = strtoupper($this->input->post('field1'));
        $datamskn['field2'] = strtoupper($this->input->post('field2'));
        $datamskn['field3'] = strtoupper($this->input->post('field3'));
        $datamskn['field4'] = strtoupper($this->input->post('field4'));

        //--cek if data exist
        $this->load->model('model_entry');
        $check = $this->model_entry->check_dataMiskin($nik);
        if ($check) {
            $this->load->model('model_entry');
            $update = $this->model_entry->update_dataMiskin($datamskn);
        } else {
            $this->load->model('model_entry');
            $insert = $this->model_entry->insert_dataMiskin($datamskn);
        }

        // var_dump($datamskn['field1'],$datamskn['field2'],$datamskn['field3'],$datamskn['field4'],$no_kk,$datamskn['nik']);die;

        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('entry/lihat');
        exit;
        //}
    }
}
