<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

function emptyres($res) {
	if (!is_object($res) || $res->num_rows() == 0) {
		return true;
	} else {
		return false;
	}

}
function get_db_total_rows($db = false) {
	if ($db == false) {
		$CI = &get_instance();
		$res = $CI->db->query("SELECT FOUND_ROWS() AS total;");
	} else {
		$res = $db->query("SELECT FOUND_ROWS() AS total;");
	}
	if (emptyres($res)) {
		return 0;
	}

	$tmp = $res->row();
	return $tmp->total;

}

function array_merge_special($arr1, $arr2) {
	if (empty($arr1)) {
		return $arr2;
	}

	if (empty($arr2)) {
		return $arr1;
	}

	$arr = array();
	foreach ($arr1 as $key => $value) {
		$arr[$key] = $value;
	}
	foreach ($arr2 as $key => $value) {
		$arr[$key] = $value;
	}
	return $arr;

}

// http://stackoverflow.com/questions/2981458/converting-keys-of-an-array-object-tree-to-lowercase
// use references to avoid that a copy is used
if (!function_exists('keysToLower')) {
	function &keysToLower(&$obj) {
		$type = (int) is_object($obj) - (int) is_array($obj);
		if ($type === 0) {
			return $obj;
		}

		foreach ($obj as $key => &$val) {
			$element = keysToLower($val);
			switch ($type) {
			case 1:
				if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
					unset($obj->{$key});
					$key = $keyLowercase;
				}
				$obj->{$key} = $element;
				break;
			case -1:
				if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
					unset($obj[$key]);
					$key = $keyLowercase;
				}
				$obj[$key] = $element;
				break;
			}
		}
		return $obj;
	}
}

if (!function_exists('keysToLower2')) {
	function keysToLower2($obj) {
		$type = (int) is_object($obj) - (int) is_array($obj);
		if ($type === 0) {
			return $obj;
		}

		reset($obj);
		while (($key = key($obj)) !== null) {
			$element = keysToLower(current($obj));
			switch ($type) {
			case 1:
				if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
					unset($obj->{$key});
					$key = $keyLowercase;
				}
				$obj->{$key} = $element;
				break;
			case -1:
				if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
					unset($obj[$key]);
					$key = $keyLowercase;
				}
				$obj[$key] = $element;
				break;
			}
			next($obj);
		}
		return $obj;
	}
}

function trimmer($str, $maxchar = 15) {
	$str = strip_tags($str);
	if (strlen($str) <= $maxchar) {
		return $str;
	} else {
		return substr($str, 0, $maxchar - 3) . " ..";
	}

}

function csv_from_array($array, $delim = ",", $newline = "\n", $enclosure = '"') {
	if (!is_array($array) && count($array) < 1) {
		return false;
	}

	$out = '';

	foreach ($array as $row) {
		foreach ($row as $item) {
			$out .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $item) . $enclosure . $delim;
		}
		$out = rtrim($out);
		$out .= $newline;
	}

	return $out;
}

function rrmdir($dir, $exclude_dirs = array('originals')) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir . "/" . $object) == "dir" && !in_array($object, $exclude_dirs)) {
					rrmdir($dir . "/" . $object);
				} else {
					unlink($dir . "/" . $object);
				}

			}
		}
		reset($objects);
		rmdir($dir);
	}
}

function get_setting($key) {
	$CI = &get_instance();
	$q = "SELECT * FROM settings WHERE `key` = ?";
	$res = $CI->db->query($q, array($key));
	if (emptyres($res)) {
		return false;
	} else {
		$r = $res->row();
		return $r->content;
	}
}

function get_params($params = null, $exclude_arr = array()) {
	$get_arr = $get_array_arr = $ret = null;
	if (sizeof($params)) {
		foreach ($params as $key => $val):

			if (empty($key) || empty($val) || in_array($key, $exclude_arr)) {
				continue;
			}

			if (!is_array($val)) {
				$get_arr[$key] = $val;
			} else {
				//$key = htmlspecialchars($key, ENT_QUOTES);
				foreach ($val as $k => $v) {
					$get_array_arr[] = $key . "[{$k}]=" . $v;
				}
			}
		endforeach;
		//var_dump($get_array_arr);
		if (count($get_arr) > 0) {
			$ret = http_build_query($get_arr);
		}

		if (count($get_array_arr) > 0) {
			$ret = $ret . "&amp;" . implode("&amp;", $get_array_arr);
		}
		return $ret;
	}
	return false;
}

function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) {return ('n/a');} else {
		return (round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);}
}

function parse_breadcrumb($arr) {
	$CI = &get_instance();
	if (count($arr) <= 0) {
		$arr = array(
			$CI->uri->slash_segment(1) . $CI->uri->segment(2) => humanize($CI->uri->segment(2)),
			"" => humanize($CI->uri->segment(3)),
		);
	}
	$tmp_arr = null;
	foreach ($arr as $k => $v) {
		if (empty($k)) {
			$tmp_arr[] = $v;
		} else {
			$tmp_arr[] = anchor($k, $v);
		}

	}
	$ret = '<ol class="breadcrumb"><li>' . implode("</li><li>", $tmp_arr) . '</li></ol>';
	return $ret;
}

function no_image() {
	return base_url("_assets/css/images/noimage.png");
}

function get_error_msg($type = "") {
	switch ($type) {
	case 'NO_ACCESS':
		$ret = "YOU HAVE NO ACCESS!";
		break;

	case 'INVALID_URL':
		$ret = "INVALID URL ADDRESS!";
		break;

	default:
		$ret = "UNKNOWN ERROR!";
		break;
	}

	return $ret;
}
