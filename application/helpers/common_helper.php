<?php
function &keysToLower(&$obj) {
	$type = (int) is_object($obj) - (int) is_array($obj);
	if ($type === 0) {
		return $obj;
	}

	foreach ($obj as $key => &$val) {
		$element = keysToLower($val);
		switch ($type) {
		case 1:
			if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
				unset($obj->{$key});
				$key = $keyLowercase;
			}
			$obj->{$key} = $element;
			break;
		case -1:
			if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
				unset($obj[$key]);
				$key = $keyLowercase;
			}
			$obj[$key] = $element;
			break;
		}
	}
	return $obj;
}

// Konversi waktu ke : Senin, 4 Januari 2014
function format_hari_tanggal($waktu) {
	// Senin, Selasa dst.
	$hari_array = array(
		'Minggu',
		'Senin',
		'Selasa',
		'Rabu',
		'Kamis',
		'Jumat',
		'Sabtu',
	);
	$hr = date('w', strtotime($waktu));
	$hari = $hari_array[$hr];

	// Tanggal: 1-31 dst, tanpa leading zero.
	$tanggal = date('j', strtotime($waktu));

	// Bulan: Januari, Maret dst.
	$bulan_array = array(
		1 => 'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember',
	);
	$bl = date('n', strtotime($waktu));
	$bulan = $bulan_array[$bl];

	// Tahun, 4 digit.
	$tahun = date('Y', strtotime($waktu));

	// Hasil akhir: Senin, 1 Oktober 2014
	return "$hari, $tanggal $bulan $tahun";
}

// Format tangal ke 1 Januari 1990
function format_tanggal($waktu) {
	// Tanggal, 1-31 dst, tanpa leading zero.
	$tanggal = date('j', strtotime($waktu));

	// Bulan, Januari, Maret dst
	$bulan_array = array(
		1 => 'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember',
	);
	$bl = date('n', strtotime($waktu));
	$bulan = $bulan_array[$bl];

	// Tahun
	$tahun = date('Y', strtotime($waktu));

	// Senin, 12 Oktober 2014
	return "$tanggal $bulan $tahun";
}

// Format tangal ke yyyy-mm-dd
function date_to_en($tanggal) {
	$tgl = date('Y-m-d', strtotime($tanggal));
	if ($tgl == '1970-01-01') {
		return '';
	} else {
		return $tgl;
	}
}

// Format tangal ke dd-mm-yyyy
function date_to_id($tanggal) {
	$tgl = date('d-m-Y', strtotime($tanggal));
	if ($tgl == '01-01-1970') {
		return '';
	} else {
		return $tgl;
	}
}

// Status user
function user_desc($user_level) {
	if ($user_level == '0') {
		return 'Administrator';
	} elseif ($user_level == '1') {
		return 'Operator Kota';
	} elseif ($user_level == '2') {
		return 'Operator Kecamatan';
	} elseif ($user_level == '3') {
		return 'Operator Kelurahan';
	} elseif ($user_level == '4') {
		return 'Supervisor';
	} else {
		return ucwords($user_level);
	}
}

// Buat setiap awal kata huruf besar
function format_title_case($string) {
	return ucwords($string);
}

function format_agama($agama) {
	if ($agama == '7') {
		return 'KEPERCAYAAN LAINNYA';
	} elseif ($agama == '1') {
		return 'ISLAM';
	} elseif ($agama == '2') {
		return 'KRISTEN';
	} elseif ($agama == '3') {
		return 'KATHOLIK';
	} elseif ($agama == '4') {
		return 'HINDU';
	} elseif ($agama == '5') {
		return 'BUDHA';
	} elseif ($agama == '6') {
		return 'KONGHUCU';
	} else {
		return ucwords($agama);
	}
}

function format_goldarah($gol_drh) {
	if ($gol_drh == '1') {
		return 'A';
	} elseif ($gol_drh == '2') {
		return 'B';
	} elseif ($gol_drh == '3') {
		return 'AB';
	} elseif ($gol_drh == '4') {
		return 'O';
	} elseif ($gol_drh == '5') {
		return 'A+';
	} elseif ($gol_drh == '6') {
		return 'A-';
	} elseif ($gol_drh == '7') {
		return 'B+';
	} elseif ($gol_drh == '8') {
		return 'B-';
	} elseif ($gol_drh == '9') {
		return 'AB+';
	} elseif ($gol_drh == '10') {
		return 'AB-';
	} elseif ($gol_drh == '11') {
		return 'O+';
	} elseif ($gol_drh == '12') {
		return 'O-';
	} elseif ($gol_drh == '13') {
		return 'TIDAK TAHU';
	} else {
		return ucwords($gol_drh);
	}
}

function format_pendidikan($pddk_akh) {
	if ($pddk_akh == '1') {
		return 'TIDAK/BELUM SEKOLAH';
	} elseif ($pddk_akh == '2') {
		return 'BELUM TAMAT SD/SEDERAJAT';
	} elseif ($pddk_akh == '3') {
		return 'TAMAT SD/SEDERAJAT';
	} elseif ($pddk_akh == '4') {
		return 'SLTP/SEDERAJAT';
	} elseif ($pddk_akh == '5') {
		return 'SLTA/SEDERAJAT';
	} elseif ($pddk_akh == '6') {
		return 'DIPLOMA I/II';
	} elseif ($pddk_akh == '7') {
		return 'AKADEMI/DIPLOMA III/S. MUDA';
	} elseif ($pddk_akh == '8') {
		return 'DIPLOMA IV/STRATA I';
	} elseif ($pddk_akh == '9') {
		return 'STRATA II';
	} elseif ($pddk_akh == '10') {
		return 'STRATA III';
	} else {
		return ucwords($pddk_akh);
	}
}

function format_angka($angka) {
	// return number_format($angka, 2, ',', '.' );
	return number_format($number);
}

function get_filter_location_where($params) {
	// filter current user location
	$where_arr = [];
	$where_arr[] = "NO_PROP = " . NO_PROP;
	$where_arr[] = "NO_KAB = " . NO_KAB;
	if (is_array($params) && count($params) > 0) {
		foreach ($params as $key => $value) {
			$key = strtolower($key);
			if (stristr($key, "kec")) {
				$where_arr[] = "NO_KEC = " . $value;
			}
			if (stristr($key, "kel")) {
				$kel_arr = explode('-', $value);
				$no_kec = $kel_arr[0];
				$value = end($kel_arr);
				$where_arr[] = "NO_KEC = " . $no_kec;
				$where_arr[] = "NO_KEL = " . $value;
			}
		}
	}
	$where = implode(" AND ", $where_arr);
	return $where;
}