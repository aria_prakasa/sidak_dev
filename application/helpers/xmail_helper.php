<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function contact_email() { return "contact@".DOMAIN_NAME; }
function admin_email() { return "admin@".DOMAIN_NAME; }
function noreply_mail($to,$subject,$content)
{
	$CI =& get_instance();
	$CI->load->library('email');
	$CI->email->from('noreply@'.DOMAIN_NAME, 'noreply@'.DOMAIN_NAME);
	$CI->email->to($to);
	$CI->email->subject($subject);
	$CI->email->message($content); 
	$CI->email->mailtype ='html';
	$CI->email->send();
}

function send_mail($from_name, $from_mail, $to_mail, $subject, $content)
{
	$CI =& get_instance();
	$CI->load->library('email');
	$CI->email->from($from_mail, $from_name);
	$CI->email->to($to_mail);
	$CI->email->subject($subject);
	$CI->email->message($content); 
	$CI->email->mailtype ='html';
	$CI->email->send();
}
