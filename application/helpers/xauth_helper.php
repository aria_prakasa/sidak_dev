<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/* LOGIN SECTION */

function admin_home()
{
    redirect("admin/home");
}
function admin_login()
{
    redirect("login");
}
function admin_logout()
{
    redirect("admin/home/logout");
}
function remove_cache()
{
    $CI = &get_instance();
    $CI->load->helper('directory');
    $map = directory_map('./application/cache/', 1);
    foreach ($map as $file) {
        $ext = explode(".", $file);
        if (count($ext) == 1) {
            unlink(FCPATH . "application/cache/" . $file);
        }

    }
}
function user_login()
{
    // remove_cache();
    redirect("login");
}
function user_home()
{
    redirect("");
}
function user_logout()
{
    redirect("home/logout");
}

function get_current_admin()
{
    $CI = &get_instance();
    $CI->load->helper('cookie');
    $cookie = get_cookie('remember_logged_in');
    if ($cookie == 1) {
        // delete_cookie('remember_logged_in');
        // delete_cookie('user_logged_in');
        $user_logged_in_hash = get_cookie('user_logged_in');
        $user_logged_in      = encrypt_decrypt('decrypt', $user_logged_in_hash);
        $user_logged_in_arr  = explode("|", $$user_logged_in);
        $aname               = $user_logged_in_arr[0];
        $apass               = $user_logged_in_arr[1];
        return get_admin($aname, $apass);
    }
    if ($CI->session->userdata('aname') == false || $CI->session->userdata('apass') == false) {
        return false;
    }

    return get_admin($CI->session->userdata('aname'), $CI->session->userdata('apass'));
}
function get_admin($aname, $apass)
{
    $CI  = &get_instance();
    $res = $CI->db->get_where('ADMIN', array('USERNAME' => $aname, 'PASSWORD' => $apass), 1);
    if (emptyres($res)) {
        return false;
    } else {
        return $res->row();
    }

}

function get_logged_in_user()
{
    $CI = &get_instance();
    $CI->load->helper('cookie');
    $cookie = get_cookie('remember_logged_in');
    if ($cookie == 1) {
        // delete_cookie('remember_logged_in');
        // delete_cookie('user_logged_in');
        $user_logged_in_hash = get_cookie('user_logged_in');
        $user_logged_in      = encrypt_decrypt('decrypt', $user_logged_in_hash);
        $user_logged_in_arr  = explode("|", $$user_logged_in);
        $uname               = $user_logged_in_arr[0];
        $upass               = $user_logged_in_arr[1];
        return get_user($uname, $upass);
    }
    if (!$CI->session->userdata('uname') || !$CI->session->userdata('upass')) {
        return false;
    }

    return get_user($CI->session->userdata('uname'), $CI->session->userdata('upass'));
}
function get_user($umail, $upass)
{
    $CI = &get_instance();
    $CI->load->model('ouser');
    $O   = new Ouser;
    $res = $O->get_list(0, 1, "", "USERNAME = '" . $umail . "' AND PASSWORD = '" . $upass . "' AND IS_BLOKIR = 0");
    if (!$res) {
        return false;
    }

    $row = $res[0];
    return $row;
}

function set_login_session($umail, $upass, $type = "user", $cookie = false)
{
    $CI = &get_instance();
    $CI->load->helper('cookie');
    if ($type == "admin") {
        $CI->session->set_userdata(array("aname" => $umail, "apass" => $upass));
    } else {
        $CI->session->set_userdata(array("uname" => $umail, "upass" => $upass));
    }

    if ($cookie) {
        set_cookie('user_logged_in', encrypt_decrypt('encrypt', $umail . "|" . $upass));
        set_cookie('remember_logged_in', 1);
    }
}
function unset_login_session($type = "user")
{
    $CI = &get_instance();
    $CI->load->helper('cookie');
    $cookie = get_cookie('remember_logged_in');
    if ($cookie == 1) {
        delete_cookie('remember_logged_in');
        delete_cookie('user_logged_in');
    }
    if ($type == "admin") {
        $CI->session->unset_userdata(array("aname" => "", "apass" => ""));
    } else {
        $CI->session->unset_userdata(array("uname" => "", "upass" => ""));
    }

}

function get_login_secret_key()
{
    return "M46!c15m!n3";
}

function get_login_public_key()
{
    return "SIDAK-PENDISPENDUK-CAPIL-PASURUAN-2015";
}

function encrypt_decrypt($action, $string)
{
    $skey   = get_login_secret_key();
    $pkey   = get_login_public_key();
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key     = $skey;
    $secret_iv      = $pkey;

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}
