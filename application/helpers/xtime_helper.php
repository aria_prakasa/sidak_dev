<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* TIME HELPER */

/*
~~ http://stackoverflow.com/questions/3776682/php-calculate-age
PHP >= 5.3.0

# object oriented
$from = new DateTime('1970-02-01');
$to   = new DateTime('today');
echo $from->diff($to)->y;

# procedural
echo date_diff(date_create('1970-02-01'), date_create('today'))->y;

MySQL >= 5.0.0
SELECT TIMESTAMPDIFF(YEAR, '1970-02-01', CURDATE()) AS age
*/

function datediff($dob)
{
	if (version_compare(phpversion(), '5.3.0', '>='))
	{
		$from = new DateTime($dob);
		$to   = new DateTime('today');
		$age = $from->diff($to)->y;
	}
	elseif(version_compare(phpversion(), '5.0.0', '>='))
	{
    $age = date_diff(date_create($dob), date_create('today'))->y;
	}
	else {
		$age = datediff2($dob);
	}
	$ret['years'] = $age;
	return $ret;
}

function datediff2($dob)
{
	$dob = (is_string($dob) ? strtotime($dob) : $dob);
	$dob_y = date("Y", $dob);
	$dob_m = date("m", $dob);
	$dob_d = date("d", $dob);
  //get age from date or birthdate
  $age = (date("md", date("U", mktime(0, 0, 0, $dob_m, $dob_d, $dob_y))) > date("md")
    ? ((date("Y") - $dob_y) - 1)
    : (date("Y") - $dob_y));
  $ret = $age;
  return $ret;
}

function datediff_old1($d1, $d2){  
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
	$diff_secs = abs($d1 - $d2);  
	$base_year = min(date("Y", $d1), date("Y", $d2));  
	$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
	return array( 
				"years" => date("Y", $diff) - $base_year, 
				"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  
				"months" => date("n", $diff) - 1,  
				"days_total" => floor($diff_secs / (3600 * 24)),  
				"days" => date("j", $diff) - 1,  
				"hours_total" => floor($diff_secs / 3600),  
				"hours" => date("G", $diff),  
				"minutes_total" => floor($diff_secs / 60),  
				"minutes" => (int) date("i", $diff),  
				"seconds_total" => $diff_secs,  
				"seconds" => (int) date("s", $diff)  
				);  
}

function get_month_arr($type = "long", $lang="id")
{
	$lang = strtolower($lang);
	
	$month_arr = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	if($lang === "en") $month_arr = array("January","February","March","April","May","June","July","August","September","October","November","December");
	
	if($type === "short")
	{
		$month_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Ags","Sep","Okt","Nov","Des");
		if($lang === "en") $month_arr = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	}
	return $month_arr;
}

function parse_date($dt, $format="j F Y", $lang="id", $month_type="long")
{
	$lang = strtolower($lang);
	if($dt === "0000-00-00" || intval($dt) === 0) return "";
	if(empty($month_type)) $month_type = "long";
	if(empty($format))
	{
		$format = "F d, Y";
		if($lang === "id") $format = "j F Y";

		if($month_type === "long")
		{
			$format = "M d, Y";
			if($lang === "id") $format = "j M Y";
		}
	}

	$time = (is_string($dt) ? strtotime($dt) : $dt);  
	$ret = date($format,$time);

	$month_arr = get_month_arr($month_type, 'en');
	$month_replace_arr = get_month_arr($month_type, 'id');
	if($lang != "en") $ret = str_replace($month_arr, $month_replace_arr, $ret);
	return $ret;
}
function parse_date_time($dt, $format="j F Y G:i", $lang="id", $month_type="long")
{
	$lang = strtolower($lang);
	if($dt === "0000-00-00 00:00:00" || intval($dt) === 0) return "";
	if(empty($month_type)) $month_type = "long";
	if(empty($format))
	{
		$format = "F d, Y g:ia";
		if($lang === "id") $format = "j F Y G:i";

		if($month_type === "long")
		{
			$format = "M d, Y g:ia";
			if($lang === "id") $format = "j M Y G:i";
		}
	}
	$time = (is_string($dt) ? strtotime($dt) : $dt);  
	$ret = date($format,$time);

	$month_arr = get_month_arr($month_type, 'id');
	$month_replace_arr = get_month_arr($month_type, 'en');
	if($lang != "en") $ret = str_replace($month_arr, $month_lang_arr, $ret);
	return $ret;
}
function getrelativetime($ts)
{
	$curyear = date("Y");
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d",strtotime("-1 day"));
	$lastweek = date("Y-m-d",strtotime("-1 week"));
	$todayts = date("Y-m-d",strtotime($ts));
	$yearts = date("Y",strtotime($ts));
	$CI =& get_instance();
	$CI->load->helper('date');
	if($today == $todayts)
	{
		return str_replace(array(" hours"," hour"," minutes"," minute",","	),array("h","h","m","m",""),strtolower(timespan(strtotime($ts),time())." ago"));
	}
	if($todayts == $yesterday)
	{
		return "Yesterday at ".date("g:ia",strtotime($ts));
	}
	if($todayts > $lastweek)
	{
		return date("D \a\\t g:ia",strtotime($ts));
	}
	if($yearts == $curyear)
	{
		return date("M d \a\\t g:ia",strtotime($ts));
	}
	return date("M d, Y \a\\t g:ia",strtotime($ts));
	   
}

/*
~~ http://php.net/manual/en/datetime.createfromformat.php
(PHP 5 >= 5.3.0)
date(y) ~ 1970-2069

* Object oriented style
$date = DateTime::createFromFormat('j-M-Y', '15-Feb-2009');
echo $date->format('Y-m-d');

* Procedural style
$date = date_create_from_format('j-M-Y', '15-Feb-2009');
echo date_format($date, 'Y-m-d');

*/
function parse_date_ora($oradt=false,$format="Y-m-d")
{
	if(!$oradt) return;
	$date = DateTime::createFromFormat('d-m-Y', $oradt);
	if(!$date) return;
	$converteddate = $date->format($format);
	return $converteddate;
}

function get_date_lang($dt,$lang="EN")
{
	$dt_arr = explode("-",$dt);
	$dt_arr_reverse = array_reverse($dt_arr);
	if((substr($dt,4,1) == "-") && (substr($dt,4,1) == "-")) //yyyy-mm-dd
	{
		if($lang == "ID") $ret = implode("-",$dt_arr_reverse);
		if($lang == "EN") $ret = implode("-",$dt_arr);
	}
	if((substr($dt,2,1) == "-") && (substr($dt,5,1) == "-")) //dd-mm-yyyy
	{
		if($lang == "ID") $ret = implode("-",$dt_arr);
		if($lang == "EN") $ret = implode("-",$dt_arr_reverse);
	}
	return $ret;
}

function get_table_suffix($filter_bulan,$filter_tahun)
{
	$parse_filter_bulan = str_pad(intval($filter_bulan),2,"0",STR_PAD_LEFT);
  
  $cur_month = date('m', strtotime("-1 month"));
  $cur_year = date('Y');

  $table_suffix = "";

  if(
    	( !empty($filter_bulan) && !empty($filter_tahun) ) ||
      ( $filter_bulan != $cur_month && $filter_tahun != $cur_year )
    )
  {
    if(
      	( $filter_bulan < $cur_month && $filter_tahun <= $cur_year ) ||
      	( $filter_tahun < $cur_year )
      )
    {
      $table_suffix = "_".$parse_filter_bulan."".$filter_tahun;
    }
  }

  return $table_suffix;
}