<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_wilayah extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        // $this->db_pullout = $this->load->database('default', true);
    }

    public function ambil_kecamatan()
    {
        $this->db->select('NO_KEC, NAMA_KEC');
        $query = $this->db->get('SETUP_KEC');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result['-']                  = '- Pilih Kecamatan -';
                $result[$row['id_kecamatan']] = ucwords(strtolower($row['NAMA_KEC']));
            }
            return $result;
        }
    }

    public function ambil_kelurahan($kode_kec)
    {
        $this->db->where('NO_KEC', $kode_prop);
        $this->db->select('NO_KEC, NAMA_KEC');
        $query = $this->db->get('SETUP_KEC');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[$row['id_kelurahan']] = ucwords(strtolower($row['NAMA_KEL']));
            }
        } else {
            $result['-'] = '- Belum Ada Kelurahan -';
        }
        return $result;
    }

    public function ambil_pekerjaan()
    {
        // $this->db->where('NO_KEC', $kode_prop);
        // $this->db->select('NO_KEC, NAMA_KEC');
        $query = $this->db->get('MASTER_PEKERJAAN');
        
        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }
}
