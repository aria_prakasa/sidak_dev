<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_kategori extends Omaster
{
    public $table_name = "MASTER_KATEGORI";

    public function drop_down_select($name, $selval, $optional = "", $default = "", $readonly = "")
    {
        $list = $this->get_list(0, 0, "");

        $Otmp = new self();
        foreach ($list as $r) {
            $Otmp->setup($r);
            $arr[$Otmp->ID] = $Otmp->get_name();
        }
        unset($Otmp);
        return dropdown($name, $arr, $selval, $optional, $default, $readonly);
    }

    public function get_list()
    {
        $sql = $this->db->get('MASTER_KATEGORI');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Provinsi -';
                $result[$row['NO']] = ucwords(strtolower($row['DESCRIP']));
            }
            return $result;
        }
    }
}
