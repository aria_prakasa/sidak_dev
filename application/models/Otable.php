<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Otable extends CI_Model
{

    public $table_name = "";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null, $type = "ID")
    {
        if (!empty($ID)) {
            $arr = array("ID" => $ID);
            if ($type != "ID") {
                $arr = array("URL_TITLE" => $ID);
            }

            $res = $this->db->get_where($this->table_name, $arr);
            // $res = $this->db->where($arr)
            //                 ->get($this->table_name);
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->NO;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->NO;
        }
        return false;
    }

    public function refresh()
    {
        $res = $this->db->get_where($this->table_name, array("ID" => $this->ID));
        if (!emptyres($res)) {
            $this->row = $res->row();
            $this->ID  = $this->row->ID;
        }
    }

    /*function add($arr)
    {
    $this->db->insert($this->table_name,$arr);
    return $this->db->insert_id();
    }

    function edit($arr)
    {
    return $this->db->update($this->table_name,$arr,array("ID" => $this->ID));
    }

    function delete()
    {
    return $this->db->delete($this->table_name,array("ID" => $this->ID));
    }*/

    public function get_list($offset = 0, $limit = 0, $orderby = "", $where = "")
    {
        if (!empty($where) && trim($where) != "") {
            $this->db->where($where, null, false);
        }

        if (!empty($orderby) && trim($orderby) != "") {
            $this->db->order_by($orderby);
        }

        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }

        // $this->db->select('*', FALSE);
        $res = $this->db->get($this->table_name);
        if (emptyres($res)) {
            return false;
        }

        return $res->result();
    }

    public function get_list_total($where = "")
    {
        if (trim($where) != "") {
            $this->db->where($where, null, false);
        }

        $res = $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    public function search($keyword = "", $offset = 0, $limit = 0, $orderby = "", $where = "")
    {
        if (trim($where) != "") {
            $this->db->where($where, null, false);
        }

        if (trim($keyword) != "") {
            $this->db->like("ID", intval($keyword), 'none');
            $like_array = array('NAME' => $keyword);
            $this->db->or_like($like_array);
        }
        if (trim($orderby) != "") {
            $this->db->order_by($orderby);
        }

        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }

        //$this->db->select('*', FALSE);
        $res = $this->db->get($this->table_name);
        if (emptyres($res)) {
            return false;
        }

        return $res->result();
    }

    public function search_total($keyword = "", $where = "")
    {
        if (trim($where) != "") {
            $this->db->where($where, null, false);
        }

        if (trim($keyword) != "") {
            $this->db->like("ID", intval($keyword), 'none');
            $like_array = array('NAME' => $keyword);
            $this->db->or_like($like_array);
        }
        $this->db->select('count(*) as total', false);
        $res = $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    public function drop_down_select($name, $selval, $optional = "", $default = "", $readonly = "")
    {
        $list = $this->get_list(0, 0, "");

        $Otmp = new self();
        foreach ($list as $r) {
            $Otmp->setup($r);
            $arr[$Otmp->ID] = $Otmp->get_name();
        }
        unset($Otmp);
        return dropdown($name, $arr, $selval, $optional, $default, $readonly);
    }

    /*function update_photo($image_file)
    {
    $files = auto_resize_photo($image_file);
    $arr = array('PHOTO' => $image_file);
    $this->edit($arr);
    }

    function get_photo($type = "thumbnail")
    {
    $filename = $this->row->photo;
    $upload_folder = $this->config->item('upload_dir');
    return base_url($upload_dir."{$type}/{$filename}");
    }*/

    public function update_url_title()
    {

        $new_url_title = url_title($this->row->NAME, "-", true);
        $res           = $this->get_list(0, 1, "", "URL_TITLE='{$new_url_title}' AND ID <> " . intval($this->ID));
        if ($res) {
            $new_url_title .= "-" . $this->ID;
        }
        $arr = array('url_title' => $new_url_title);
        $this->edit($arr);
    }

    /*function get_link()
    {
    return $this->row->URL_TITLE;
    }*/

    public function get_name()
    {
        return $this->row->NAME;
    }

}
