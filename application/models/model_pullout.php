<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_pullout extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db_pullout = $this->load->database('pullout', true);
	}

	// public function insert_biomohon($params = array())
	// {
	//     if (count($params) <= 0) {
	//         return false;
	//     }

	//     $data = null;
	//     foreach ($params as $key => $value) {
	//         $key        = strtoupper($key);
	//         $data[$key] = $value;
	//     }
	//     $res = $this->db->insert('MHN_BIODATA_PDDK_MISKIN', $data);
	//     // die(var_dump($this->db->last_query()));

	//     return $res;
	// }

	public function insert_mohon($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datas = null;
		foreach ($params as $values) {
			$data = null;
			foreach ($values as $key => $value) {
				$key = strtoupper($key);
				$data[$key] = $value;
			}
			$datas[] = $data;
		}
		// var_dump($datas);die;
		$this->db_pullout->where('FLAG_STATUS', '0');
		$res = $this->db_pullout->insert_batch('MHN_BIODATA_PDDK_MISKIN', $datas);
		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function insert_valid($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datas = null;
		foreach ($params as $values) {
			$data = null;
			foreach ($values as $key => $value) {
				$key = strtoupper($key);
				$data[$key] = $value;
			}
			$datas[] = $data;
		}
		//var_dump($datas);die;
		$res = $this->db_pullout->insert_batch('BIODATA_PDDK_MISKIN', $datas);
		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function update_dataValid($nik = NULL, $params = array()) {

		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$datavalid = array_change_key_case($params, CASE_UPPER);

		$this->db_pullout->where('NIK', $nik);
		$res = $this->db_pullout->update('BIODATA_PDDK_MISKIN', $datavalid);
		// die(var_dump($this->db_pullout->last_query()));

		return $res;
	}

	public function deleteMohon($no_kk) {
		$this->db_pullout->where(array('NO_KK' => $no_kk));
		// $this->db->where('NIK', $nik);
		return $this->db_pullout->delete('MHN_BIODATA_PDDK_MISKIN');
	}

	public function deleteValid($no_kk) {
		$this->db_pullout->where(array('NO_KK' => $no_kk));
		// $this->db->where('NIK', $nik);
		$query = $this->db_pullout->delete('BIODATA_PDDK_MISKIN');
		// var_dump($this->db->last_query());die;

		return $query;
	}

	public function biodeleteMohon($nik) {
		$this->db_pullout->where(array('NIK' => $nik));
		$query = $this->db_pullout->delete('MHN_BIODATA_PDDK_MISKIN');
		// var_dump($this->db->last_query());die;

		return $query;
	}

	public function biodeleteValid($nik) {
		$this->db_pullout->where(array('NIK' => $nik));
		$query = $this->db_pullout->delete('BIODATA_PDDK_MISKIN');
		// var_dump($this->db->last_query());die;

		return $query;
	}
}
