<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_entry extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getEntri($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		// var_dump($database,$data['NAMA']);die;
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NIK']) {
			$this->db->where(array('NIK' => $data['NIK']));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA_LGKP', $data['NAMA'], 'both');
		}
		$query = $this->db->get('GETBIODATA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getdatakk($kk, $no_kec, $no_kel) {

		if ($this->cu->USER_LEVEL == 2) {
			$this->db->where(array('NO_KEC' => $no_kec));
		}
		if ($this->cu->USER_LEVEL == 3) {
			$this->db->where(array('NO_KEC' => $no_kec));
			$this->db->where(array('NO_KEL' => $no_kel));
		}
		$this->db->where(array('NO_KK' => $kk));
		$query = $this->db->get('GETDATAKK');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;

	}

	public function getdatanama($nama, $no_kec, $no_kel) {

		if ($this->cu->USER_LEVEL == 2) {
			$this->db->where(array('NO_KEC' => $no_kec));
		}
		if ($this->cu->USER_LEVEL == 3) {
			$this->db->where(array('NO_KEC' => $no_kec));
			$this->db->where(array('NO_KEL' => $no_kel));
		}
		$this->db->like('NAMA', $nama);
		$query = $this->db->get('GETDATAKK');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getanggotakk($kk) {

		$this->db->where(array('NO_KK' => $kk));
		$query = $this->db->get('GETBIODATA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getbiodata($nik) {
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('GETBIODATA');

		//var_dump($this->db->last_query());die;
		//var_dump($query);die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getbiodata_mohon($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('GETDATAMOHON');

		//var_dump($this->db->last_query());die;
		//var_dump($query);die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getbiodata_wni($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->where('FLAG_STATUS', '0');
		$query = $this->db->get('BIODATA_WNI');

		//var_dump($this->db->last_query());die;
		//var_dump($query);die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getdata_keluarga($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('DATA_KELUARGA');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getpermohonan($nik) {
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('MHN_BIODATA_PDDK_MISKIN');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	/*public function getfilter($no_kk)
		    {
		        $this->db->where(array('NO_KK' => $no_kk));
		        $query = $this->db->get('MHN_DATA_TINGGAL');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() < 1) {
		            return keysToLower($query->row());
		        }

		        return false;
	*/

	public function getdelete($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		// $this->db->where('NIK', $nik);
		return $this->db->delete('MHN_BIODATA_PDDK_MISKIN');
	}

	public function get_biodelete($nik) {
		$this->db->where(array('NIK' => $nik));
		return $this->db->delete('MHN_BIODATA_PDDK_MISKIN');
	}

	public function getdatamohon($kk) {
		$this->db->where(array('NO_KK' => $kk));
		$query = $this->db->get('GETDATAMOHON');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getanggotamohon($kk) {
		$this->db->where(array('NO_KK' => $kk));
		$query = $this->db->get('GETBIODATAMOHON');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	/*public function getcatatan_miskin($nik)
		    {
		        $this->db->where(array('NIK' => $nik));
		        $query = $this->db->get('DATA_KEMISKINAN');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->row());
		        }

		        return false;
	*/

	public function check_dataMiskin($nik) {
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('DATA_KEMISKINAN');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insert_biodatapddk($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$res = $this->db->insert('MHN_BIODATA_PDDK_MISKIN', $data);
		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function insert_datapddk($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datas = null;
		foreach ($params as $values) {
			$data = null;
			foreach ($values as $key => $value) {
				$key = strtoupper($key);
				$data[$key] = $value;
			}
			$datas[] = $data;
		}
		// var_dump($datas);die;
		$this->db->where('FLAG_STATUS', '0');
		$res = $this->db->insert_batch('MHN_BIODATA_PDDK_MISKIN', $datas);
		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function insert_dataMiskin($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datamskn = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$datamskn[$key] = $value;
		}
		$res = $this->db->insert('DATA_KEMISKINAN', $datamskn);
		//die(var_dump($this->db->last_query()));

		return $res;
	}

	public function update_dataMiskin($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datamskn = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$datamskn[$key] = $value;
		}
		$this->db->where('NIK', $datamskn['NIK']);
		$res = $this->db->update('DATA_KEMISKINAN', $datamskn);
		// die(var_dump($this->db->last_query()));

		return $res;
	}
}
//$res = $this->db->get_where($this->table_name,$arr);
//$query = $this->db->getwhere('users', array('username' => $username, 'password' => md5($passwordx
