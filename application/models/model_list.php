<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_list extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
	}

	public function getdata($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('GETDATAVALID');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getanggota($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('GETBIODATAVALID');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getdataValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($no_kk) {
			$this->db->where(array('NO_KK' => $no_kk));
		}
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA_KEP', $data['NAMA'], 'both');
		}
		// $this->db->where('SIDAK_STATUS', '0');
		$query = $this->db->get('GETDATAVALID');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return keysToLower($query->result());
			return $query->result();
		}

		return false;
	}

	public function getbioValid($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($no_kk) {
			$this->db->where(array('NO_KK' => $no_kk));
		}
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NIK']) {
			$this->db->where(array('NIK' => $data['NIK']));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA_LGKP', $data['NAMA'], 'both');
		}
		// $this->db->where('SIDAK_STATUS', '0');
		$query = $this->db->get('GETBIODATAVALID');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getbiodata($nik) {
		if ($nik) {
			$this->db->where('NIK', $nik);
		}
		//$this->db->where('SIDAK_STATUS', '0');
		$query = $this->db->get('GETBIODATAVALID');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getdelete($no_kk) {
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->delete('BIODATA_PDDK_MISKIN');
		// var_dump($this->db->last_query());die;

		return $query;
	}

	public function get_biodelete($nik) {
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->delete('BIODATA_PDDK_MISKIN');
		// var_dump($this->db->last_query());die;

		return $query;
	}

	public function set_delete($no_kk) {
		$this->db->set('SIDAK_STATUS', '1');
		$this->db->where(array('NO_KK' => $no_kk));
		return $this->db->update('BIODATA_PDDK_MISKIN');

		// $query = $this->db->update('MHN_BIODATA_PDDK');

		/*$this->db->set('SIDAK_STATUS', '2');
			        $this->db->where(array('NO_KK' => $no_kk));
		*/
	}

	/*public function getcatatan_miskin($nik)
		    {
		        $this->db->where(array('NIK' => $nik));
		        $query = $this->db->get('DATA_KEMISKINAN');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->row());
		        }

		        return false;
	*/

	public function check_dataMiskin($nik) {
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('DATA_KEMISKINAN');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insert_dataMiskin($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$datamskn = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$datamskn[$key] = $value;
		}
		$res = $this->db->insert('DATA_KEMISKINAN', $datamskn);
		//die(var_dump($this->db->last_query()));

		return $res;
	}

	public function update_dataValid($nik = NULL, $params = array()) {
		/*if (count($params) <= 0) {
			            return false;
			        }

			        $datas = null;
			        foreach ($params as $values) {
			            $data = null;
			            foreach ($values as $key => $value) {
			                $key        = strtoupper($key);
			                $data[$key] = $value;
			            }
			            $datas[] = $data;
		*/

		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		// $datavalid = null;
		// foreach ($params as $key => $value) {
		//     $key            = strtoupper($key);
		//     $datavalid[$key] = $value;
		// }
		$datavalid = array_change_key_case($params, CASE_UPPER);

		$this->db->where('NIK', $nik);
		$res = $this->db->update('BIODATA_PDDK_MISKIN', $datavalid);
		// die(var_dump($this->db->last_query()));

		return $res;
	}
}
