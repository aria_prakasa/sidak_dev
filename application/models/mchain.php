<?php
class MChain extends Model
{
    public function __construct()
    {
        parent::Model();
    }

    public function getKecamatanList()
    {
        $result = array();
        $this->db->select('NO_KEC, NAMA_KEC');
        $array_keys_values = $this->db->get('SETUP_KEC');
        // $this->db->select('*');
        // $this->db->from('Kecamatan');
        // $this->db->order_by('Kecamatan','ASC');
        // $array_keys_values = $this->db->get();
        foreach ($array_keys_values->result() as $row) {
            $result[0]                  = '-Pilih Kecamatan-';
            $result[$row->kecamatan_id] = $row->kecamatan;
        }

        return $result;
    }

    public function getKelurahanList()
    {
        $kecamatan_id = $this->input->post('kecamatan_id');
        $result       = array();
        $this->db->select('NO_KEC, NO_KEL, NAMA_KEL');
        $this->db->where('NO_KEC', $kecamatan_id);
        $array_keys_values = $this->db->get('SETUP_KEL');
        // $this->db->select('*');
        //       $this->db->from('Kelurahan_kabupaten');
        //       $this->db->where('Kecamatan_id',$Kecamatan_id);
        //       $this->db->order_by('Kelurahan_kabupaten','ASC');
        //       $array_keys_values = $this->db->get();
        foreach ($array_keys_values->result() as $row) {
            $result[0]                  = '-Pilih Kelurahan-';
            $result[$row->kelurahan_id] = $row->kelurahan_kabupaten;
        }

        return $result;
    }

}
