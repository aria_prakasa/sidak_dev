<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_gaji extends Omaster
{
    public $table_name = "MASTER_GAJI";

    public function drop_down_select($name, $selval, $optional = "", $default = "", $readonly = "")
    {
        $list = $this->get_list(0, 0, "");

        $Otmp = new self();
        foreach ($list as $r) {
            $Otmp->setup($r);
            $arr[$Otmp->ID] = $Otmp->get_name();
        }
        unset($Otmp);
        return dropdown($name, $arr, $selval, $optional, $default, $readonly);
    }
}
