<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_header extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        //$this->db_pullout = $this->load->database('pullout', TRUE);
    }

    public function getheaderAgama()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_AGAMA');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getheaderGoldrh()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_GOLONGAN_DARAH');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getheaderPddkan()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_PENDIDIKAN');

        var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }
}
