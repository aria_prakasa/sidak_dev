<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Vdata_valid extends Otable
{

    public $table_name = "GETDATAVALID";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null, $type = "ID")
    {
        if (!empty($ID)) {
            $arr = array("NO_KK" => $ID);
            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->NO_KK;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->NO_KK;
        }
        return false;
    }

    public function get_list($offset = 0, $limit = 0, $orderby = "", $where = "")
    {
        if (!empty($where) && trim($where) != "") {
            $this->db->where($where, null, false);
        }

        if (!empty($orderby) && trim($orderby) != "") {
            $this->db->order_by($orderby);
        }

        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }

        // $this->db->select('*', FALSE);
        $res = $this->db->get($this->table_name);
        // var_dump($this->db->last_query());die;
        if (emptyres($res)) {
            return false;
        }

        return $res->result();
    }

}
