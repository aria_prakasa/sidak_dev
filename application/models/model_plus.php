<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_plus extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		$this->db_pullout = $this->load->database('pullout', true);
	}

	public function getbioPlus($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NIK']) {
			$this->db->where(array('NIK' => $data['NIK']));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA_LGKP', $data['NAMA'], 'both');
		}
		$query = $this->db->get('GETBIODATA_PLUS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getdatakkPlus($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA', $data['NAMA'], 'both');
		}
		$query = $this->db->get('GETDATAKK_PLUS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getbioBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		$this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
		if ($kd_kec) {
			$this->db->where(array('NO_KEC' => $kd_kec));
		}
		if ($kd_kel) {
			$this->db->where(array('NO_KEL' => $kd_kel));
		}
		if ($data['NIK']) {
			$this->db->where(array('NIK' => $data['NIK']));
		}
		if ($data['NO_KK']) {
			$this->db->where(array('NO_KK' => $data['NO_KK']));
		}
		if ($data['NAMA']) {
			$this->db->like('NAMA_LGKP', $data['NAMA'], 'both');
		}
		$this->db->where('STATUS_BPJS', 2);
		$query = $this->db->get('GETBIODATA_PLUS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getdataPlus($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$query = $this->db->get('GETDATAKK_PLUS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getdataKK($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$this->db->order_by('STAT_HBKEL_ID');
		$this->db->order_by('TGL_LHR');
		$query = $this->db->get('GETBIODATA_PLUS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function check_bpjs($nik) {
		$this->db_pullout->where('NIK', $nik);
		$query = $this->db_pullout->get('MISKIN_BPJS');

		// var_dump($this->db_pullout->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insert_bpjs($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		// var_dump($data);die;
		$res = $this->db_pullout->insert('MISKIN_BPJS', $data);
		// die(var_dump($this->db_pullout->last_query()));

		return $res;
	}

	public function update_bpjs($nik = NULL, $params = array()) {
		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$data = array_change_key_case($params, CASE_UPPER);

		$this->db_pullout->where('NIK', $nik);
		$res = $this->db_pullout->update('MISKIN_BPJS', $data);

		return $res;
	}

	public function delete_bpjs($nik) {
		$this->db_pullout->where('NIK', $nik);
		$res = $this->db_pullout->delete('MISKIN_BPJS');
		// var_dump($this->db_pullout->last_query());die;

		return $res;
	}

	public function check_raskin($no_kk) {
		$this->db_pullout->where('NO_KK', $no_kk);
		$query = $this->db_pullout->get('MISKIN_RASKIN');

		// var_dump($this->db_pullout->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insert_raskin($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		// $datas = array_change_key_case($data,CASE_UPPER);
		// var_dump($datas);die;
		$res = $this->db_pullout->insert('MISKIN_RASKIN', $data);
		// die(var_dump($this->db_pullout->last_query()));

		return $res;
	}

	public function update_raskin($no_kk = NULL, $params = array()) {
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$data = array_change_key_case($params, CASE_UPPER);

		$this->db_pullout->where('NO_KK', $no_kk);
		$res = $this->db_pullout->update('MISKIN_RASKIN', $data);
		// die(var_dump($this->db_pullout->last_query()));
		return $res;
	}

	public function delete_raskin($no_kk) {
		$this->db_pullout->where('NO_KK', $no_kk);
		$res = $this->db_pullout->delete('MISKIN_RASKIN');
		return $res;
	}

	public function check_pkh($no_kk) {
		$this->db_pullout->where('NO_KK', $no_kk);
		$query = $this->db_pullout->get('MISKIN_PKH');

		// var_dump($this->db_pullout->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insert_pkh($params = array()) {
		if (count($params) <= 0) {
			return false;
		}

		$data = null;
		foreach ($params as $key => $value) {
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		// $datas = array_change_key_case($data,CASE_UPPER);
		// var_dump($datas);die;
		$res = $this->db_pullout->insert('MISKIN_PKH', $data);
		// die(var_dump($this->db_pullout->last_query()));

		return $res;
	}

	public function update_pkh($no_kk = NULL, $params = array()) {
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$data = array_change_key_case($params, CASE_UPPER);

		$this->db_pullout->where('NO_KK', $no_kk);
		$res = $this->db_pullout->update('MISKIN_PKH', $data);
		// die(var_dump($this->db_pullout->last_query()));
		return $res;
	}

	public function delete_pkh($no_kk) {
		$this->db_pullout->where('NO_KK', $no_kk);
		$res = $this->db_pullout->delete('MISKIN_PKH');

		return $res;
	}
}
