<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_kab extends Otable {

  public $table_name = "SETUP_KAB";
  public $row = NULL;
  public $ID = NULL;

	function __construct($PROP_ID=NULL, $ID=NULL, $type="ID")
  {
    if(!empty($PROP_ID) && !empty($ID))
    {
      $arr = array("NO_PROP" => $PROP_ID, "NO_KAB" => $ID);
  		if($type != "ID") $arr = array("URL_TITLE" => $ID);
      $res = $this->db->get_where($this->table_name,$arr);
      if(!emptyres($res))
      {
        $this->row = $res->row();
        $this->ID = $this->row->NO_KAB; 
      }
    }
  }

  function setup($row=NULL)
  {
    if(is_object($row) && intval($row->NO_KAB) > 0)
    {
      $this->row = $row;
      $this->ID = $row->NO_KAB;
    }
    return FALSE;
  }

  function get_name()
  {
    return $this->row->NAMA_KAB;
  }

  function get_total_penduduk($table_suffix="", $where="")
  {
    $arr = array("NO_PROP" => intval($this->row->NO_PROP),
            "NO_KAB" => intval($this->ID),
            "FLAG_STATUS" => 0
            );
    $table_name = 'BIODATA_WNI'.$table_suffix;
    $this->db->where($arr);
    if(trim($where) != "") $this->db->where($where,NULL,FALSE);
    $res = $this->db->from($table_name);
    $total = $this->db->count_all_results();
    return $total;
  }

  function get_penduduk_totals($table_suffix="", $add_params=NULL)
  {
    $params = array("filter_by" => "jenis_kelamin");
    if( ! empty($add_params) && count($add_params) > 0)
    {
      $params = array_merge($add_params,$params);
    }
    $table_data = $this->get_list_filter($params,$table_suffix);
    // var_dump($table_data); die;
    $tmp_total = $tmp_male = $tmp_female = 0;
    foreach ($table_data as $key => $value)
    {
      $tmp_total = $tmp_total + $value[1]['total'];
      $tmp_male = $tmp_male + $value[1]['male'];
      $tmp_female = $tmp_female + $value[1]['female'];
    }
    // extract(array_values($table_data)[0]);

    $ret = array(
            "total_penduduk" => $tmp_total,
            "total_male" => $tmp_male,
            "total_female" => $tmp_female
            );
    // var_dump($ret);
    return json_encode($ret);
  }

  function get_kk_total()
  {
    $arr = array("NO_PROP" => intval($this->row->NO_PROP),
            "NO_KAB" => intval($this->ID)
            );
    $this->db->where($arr);
    $this->db->from('DATA_KELUARGA');
    $ret = $this->db->count_all_results();

    return $ret;
  }

  function get_kec($offset=0,$limit=0,$orderby="",$where="")
  {
    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID)
      );
    if(!empty($where)) $where_arr[] = $where;
    $this->load->model('setup_kec');
    return $this->setup_kec->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
  }

  function get_kec_total($where)
  {
    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID)
      );
    if(!empty($where)) $where_arr[] = $where;
    $this->load->model('setup_kec');
    return $this->setup_kec->get_list_total(implode(" AND ", $where_arr));
  }

  function is_my_kec($kec_id=NULL)
  {
    if(empty($kel_id)) return FALSE;

    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID),
      "NO_KEC = ".intval($kec_id),
      "NO_KEL = ".intval($this->row->NO_KEL)
      );
    $this->load->model('setup_kec');
    return $this->setup_kec->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
  }

  function get_kel($offset=0,$limit=0,$orderby="",$where="")
  {
    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID)
      );
    if(!empty($where)) $where_arr[] = $where;
    $this->load->model('setup_kel');
    return $this->setup_kel->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
  }

  function get_kel_total($where="")
  {
    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID)
      );
    if(!empty($where)) $where_arr[] = $where;
    $this->load->model('setup_kel');
    return $this->setup_kel->get_list_total(implode(" AND ", $where_arr));
  }

  function is_my_kel($kel_id=NULL)
  {
    if(empty($kel_id)) return FALSE;

    $where_arr = array(
      "NO_PROP = ".intval($this->row->NO_PROP),
      "NO_KAB = ".intval($this->ID),
      "NO_KEC = ".intval($this->row->NO_KEC),
      "NO_KEL = ".intval($kel_id)
      );
    $this->load->model('setup_kel');
    return $this->setup_kel->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
  }

  function get_list_filter($params=NULL,$table_suffix="")
  {
    $params['no_prop'] = $this->row->NO_PROP;
    $params['no_kab'] = $this->row->NO_KAB;
    $params['filter_kab'] = $this->row->NO_PROP."-".$this->ID;
    // $params['filter_by'] = "jenis_kelamin";
    // var_dump($params);
    $this->load->model('oagregat');
    $oa = new Oagregat;
    return $oa->get_data($params,$table_suffix);
    // die(var_dump($ret));
  }

  function drop_down_select_by_prop($no_prop,$name,$selval,$optional = "",$default="",$readonly="")
  {
    if(empty($no_prop)) return;
    
    $list = $this->get_list(0, 0, "NO_PROP ASC, NO_KAB ASC", "NO_PROP=".intval($no_prop)." AND NO_KAB = ".intval(NO_KAB));
    if( ! $list) return FALSE;
    
    foreach($list as $r)
    {
      // $arr[$r->NO_PROP."-".$r->NO_KAB] = "[".$r->NO_PROP."-".$r->NO_KAB."] ".$r->NAMA_KAB;
      $arr[$r->NO_PROP."-".$r->NO_KAB] = $r->NAMA_KAB;
    }
    return dropdown($name,$arr,$selval,$optional,$default,$readonly);
  }

  function get_link()
  {
    extract(get_object_vars($this->row));
    return "/kabupaten/{$NO_PROP}-{$NO_KAB}";
  }
  
  /*function get_list_filter_($params=NULL,$table_suffix="")
  {
    $recalc = FALSE;
    // get filter month and year
    if(intval($params['filter_bulan']) > 0 && intval($params['filter_tahun']) > 0)
    {
      $filter_bulan = $params['filter_bulan'];
      $filter_tahun = $params['filter_tahun'];
      $filter_bulan = str_pad(intval($filter_bulan),2,"0",STR_PAD_LEFT);
    }
    else
    {
      // set default current month and year
      $cur_month = date('m');
      $cur_year = date('Y');
      // set/replace default params
      $filter_bulan = $cur_month;
      $filter_tahun = $cur_year;
    }
    // get wilayah KEC n KEL
    if($params['filter_wilayah'] !== "")
    {
      // die('filter wilayah kosong');
      $wilayah_arr = explode("-", $params['filter_wilayah']);
      if(count($wilayah_arr) > 3)
      {
        $NO_KEC = $wilayah_arr[2];
        $NO_KEL = end($wilayah_arr);
      }
      else
      {
        $NO_KEC = end($wilayah_arr);
      }
    }

    // check to agregats table first instead of recalculate    
    // init where array
    $arr = array("no_prop" => intval($this->row->NO_PROP),
            "no_kab" => intval($this->row->NO_KAB)
            );
    if($params['filter_wilayah'] != "")
    {
      $arr["no_kec"] = intval($NO_KEC);
      if(intval($NO_KEL) > 0) $arr["no_kel"] = intval($NO_KEL);
    }
    $arr['month'] = $filter_bulan;
    $arr['year'] = $filter_tahun;
    // var_dump($arr);
    // check agregats
    $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    $this->dbd->where($arr);
    $check = $this->dbd->get('agregats');
    // var_dump($this->dbd->last_query());
    $count_all = get_db_total_rows($this->dbd);
    
    if(emptyres($check)) $recalc = TRUE;
    else
    {
      // var_dump($count_all);
      if(intval($count_all) > 1)
      {
        $results = $check->result();
        $total = $total_kk = $male = $female = 0;
        $key = $params['filter_by']."_data";
        foreach ($results as $row)
        {
          if($params['filter_by'] == 'jenis_kelamin')
          {
            $jk_arr = (array) json_decode($row->$key);
            $total = $total + $row->total_penduduk;
            $total_kk = $total_kk + $row->total_kk;
            $male = $male + $jk_arr['male'];
            $female = $female + $jk_arr['female'];
          }
        }
        if($params['filter_by'] == 'jenis_kelamin')
        {
          $jk_arr = array("male" => $male, "female" => $female);
          $total_arr = array('total' => $total, 'total_kk' => $total_kk);
          $ret['Jenis Kelamin'] = array_merge($jk_arr, $total_arr);
        }
        return $ret;
      }
      else
      {
        $row = $check->row();
        if(isset($params['filter_by']))
        {
          $key = $params['filter_by']."_data";
          if($params['filter_by'] == 'umur' && $params['umur_range'] != 5) $recalc = TRUE;
          else
          {
            if($params['filter_by'] == 'jenis_kelamin')
            {
              if($row->$key == "") $recalc = TRUE;
              else
              {
                $jk_arr = (array) json_decode($row->$key);
                $total_arr = array('total' => $row->total_penduduk, 'total_kk' => $row->total_kk);
                $ret['Jenis Kelamin'] = array_merge($jk_arr, $total_arr);
              }
            }
            else
            {
              if($row->$key == "") $recalc = TRUE;
              else {
                $ret = (array) json_decode($row->$key);
              }
            }
            // var_dump($ret,$recalc); die;
            return $ret;
          }
        }
      }
    }

    if($recalc === TRUE)
    {
      // die('RECALC');
      $OMa = new Master_agama;
      $agamas = $OMa->get_arr();
      $OMpd = new Master_pendidikan;
      $pendidikans = $OMpd->get_arr();
      $OMpk = new Master_pekerjaan;
      $pekerjaans = $OMpk->get_arr();

      if($params['filter_by'] != "akte_mati")
      {
        // BUKAN AKTE MATI!
        $arr = array("NO_PROP" => intval($this->row->NO_PROP),
                "NO_KAB" => intval($this->row->NO_KAB)
                );
        if($params['filter_wilayah'] != "")
        {
          $arr["NO_KEC"] = intval($NO_KEC);
          $arr["NO_KEL"] = intval($NO_KEL);
        }

        $wni_arr = array_merge($arr, array("FLAG_STATUS" => 0));

        $this->db->select("NIK, JENIS_KLMIN, JENIS_PKRJN, AGAMA, PDDK_AKH, AKTA_LHR,
          TO_CHAR(TGL_LHR, 'YYYY-MM-DD') AS TGL_LHR_C
          ", FALSE);
        $this->db->where($wni_arr);
        $this->db->from('BIODATA_WNI'.$table_suffix);
      }
      else
      {
        $arr = array("MATI_NO_PROV" => intval($this->row->NO_PROP),
                "MATI_NO_KAB" => intval($this->row->NO_KAB)
                );
        if($params['filter_wilayah'])
        {
          $arr["MATI_NO_KEC"] = intval($NO_KEC);
          $arr["MATI_NO_KEL"] = intval($NO_KEL);
        }

        $wni_arr = array_merge($arr, array("FLAG_STATUS" => 0));

        $this->db->select("MATI_JNS_KELAMIN AS JENIS_KLMIN", FALSE);

        $this->db->where($wni_arr);
        if($params['filter_bulan'] && $params['filter_tahun'])
        {
          $filter_bulan = str_pad($params['filter_bulan'], 2, "0", STR_PAD_LEFT);
          $filter_tahun = $params['filter_tahun'];
          $this->db->where("ADM_TGL_ENTRY <= TO_DATE('".$filter_bulan."/01/".$filter_tahun." 00:00:00', 'MM/DD/YYYY HH24:MI:SS')");
        }
        $this->db->from('CAPIL_MATI');
      }

      $q_penduduk = $this->db->get();

      
      // var_dump($q_penduduk->result());
      // //var_dump($this->db->count_all_results());
      // // var_dump($this->db->last_query());
      // die;
      

      $total_laki = $total_wanita = $total_non_wajib_ktp = $total_wajib_ktp = 0;
      $total_non_akta_lahir = $total_akta_lahir = 0;
      $total_non_akta_mati = $total_akta_mati = 0;
      $results = $q_penduduk->result();
      foreach($results as $r)
      {
        switch ($params['filter_by']) {
          case 'agama':
            // agama
            foreach($agamas as $key => $val)
            {
              if($r->AGAMA == $key)
              {
                if($r->JENIS_KLMIN == 1) $agama[$key]['male']++;
                else $agama[$key]['female']++;
                $agama[$key]['total']++;
              }
            }
          break;
          case 'pendidikan':
            // pendidikan
            foreach($pendidikans as $key => $val)
            {
              if($r->PDDK_AKH == $key)
              {
                if($r->JENIS_KLMIN == 1) $pendidikan[$key]['male']++;
                else $pendidikan[$key]['female']++;
                $pendidikan[$key]['total']++;
              }
            }
          break;
          case 'pekerjaan':
            // pekerjaan
            foreach($pekerjaans as $key => $val)
            {
              if($r->JENIS_PKRJN == $key)
              {
                if($r->JENIS_KLMIN == 1) $pekerjaan[$key]['male']++;
                else $pekerjaan[$key]['female']++;
                $pekerjaan[$key]['total']++;
              }
            }
          break;
          case 'umur':
            // umur
            $umur_arr = datediff($r->TGL_LHR_C);
            // var_dump($r->TGL_LHR, $r->TGL_LHR_C,$umur_arr);
            $umur = intval($umur_arr['years']);
            $umur_limit = 65;
            $umur_range = 5;
            if($params['umur_range'])
            {
              $umur_range = $params['umur_range'];
            }
            
            if($umur <= $umur_limit)
            {
              // if($r->JENIS_KLMIN == 1) $total_umur['male'][$umur]++;
              // else $total_umur['female'][$umur]++;
              // $total_umur['total'][$umur]++;
              $i = 0;
              $umur_ranges = NULL;
              while ($i <= $umur_limit) {
                $umur_ranges[] = $i;
                $i = $i+$umur_range;
                // handle last umur
                if($i > $umur_limit && $i != $umur_limit)
                {
                  $umur_ranges[] = $umur_limit;
                }
              }
              // var_dump($umur_ranges);
              // die();
              $i = 0;
              foreach ($umur_ranges as $umur_range_item) {
                $umur_range_bef = ( $i == 0 ? intval($umur_ranges[0]) : intval($umur_ranges[($i-1)]) );
                if($umur >= $umur_range_bef && $umur <= $umur_range_item)
                {
                  if($r->JENIS_KLMIN == 1) $total_umur_range['male'][$umur_range_item]++;
                  else $total_umur_range['female'][$umur_range_item]++;
                  $total_umur_range['total'][$umur_range_item]++;
                  break;
                }
                $i++;
              }
            }
            else
            {
              if($r->JENIS_KLMIN == 1) $total_umur['male'][-1]++;
              else $total_umur['female'][-1]++;
              $total_umur['total'][-1]++;
            }
          break;
          case 'ktp':
            $umur_arr = datediff($r->TGL_LHR_C);
            $umur = intval($umur_arr['years']);
            // wajib KTP
            $umur_wajib_ktp = 17;
            if($umur < $umur_wajib_ktp)
            {
              if($r->JENIS_KLMIN == 1) $non_wajib_ktp['male']++;
              else $non_wajib_ktp['female']++;
              $non_wajib_ktp['total']++;
            }
            else
            {
              if($r->JENIS_KLMIN == 1) $wajib_ktp['male']++;
              else $wajib_ktp['female']++;
              $wajib_ktp['total']++;
            }
          break;
          case 'akte_lahir':
            // Akta Lahir
            $lahir = 2;
            if(intval($r->AKTA_LHR) == $lahir)
            {
              if($r->JENIS_KLMIN == 1) $akta_lahir['male']++;
              else $akta_lahir['female']++;
              $akta_lahir['total']++;
            }
            else
            {
              if($r->JENIS_KLMIN == 1) $non_akta_lahir['male']++;
              else $non_akta_lahir['female']++;
              $non_akta_lahir['total']++;
            }
          break;
          case 'akte_mati':
            // Akta Mati
            $mati = 0;
            // if(intval($r->AKTA_LHR) == $mati)
            // {
              if($r->JENIS_KLMIN == 1) $akta_mati['male']++;
              else $akta_mati['female']++;
              $akta_mati['total']++;
            // }
            // else
            // {
            //   if($r->JENIS_KLMIN == 1) $non_akta_mati['male']++;
            //   else $non_akta_mati['female']++;
            //   $non_akta_mati['total']++;
            // }
          break;
          default:
            // JK
            if($r->JENIS_KLMIN == 1) $total_laki++;
            else $total_wanita++;
          break;
        }
      }


      $gender_arr = array('male' => intval($total_laki), 'female' => intval($total_wanita));

      $table_data = null;
      switch ($params['filter_by']) {
        case 'agama':
          foreach($agamas as $key => $value)
          {
            // skip if value = 0
            //if(empty($agama[$key]['total'])) continue;
            $gender_arr = array('male' => intval($agama[$key]['male']), 'female' => intval($agama[$key]['female']));
            $table_data[$value] = array_merge($gender_arr, array('total' => intval($agama[$key]['total'])));
          }
          break;

        case 'pendidikan':
          foreach($pendidikans as $key => $value)
          {
            // skip if value = 0
            //if(empty($pendidikan[$key]['total'])) continue;
            $gender_arr = array('male' => intval($pendidikan[$key]['male']), 'female' => intval($pendidikan[$key]['female']));
            $table_data[$value] = $gender_arr + array('total' => intval($pendidikan[$key]['total']));
          }
          break;

        case 'pekerjaan':
          foreach($pekerjaans as $key => $value)
          {
            // skip if value = 0
            if(empty($pekerjaan[$key]['total'])) continue;
            $gender_arr = array('male' => intval($pekerjaan[$key]['male']), 'female' => intval($pekerjaan[$key]['female']));
            $table_data[$value] = $gender_arr + array('total' => intval($pekerjaan[$key]['total']));
          }
          arsort($table_data);
          break;

        case 'umur':
          foreach ($umur_ranges as $i)
          {
            $gender_arr = array('male' => intval($total_umur_range['male'][$i]), 'female' => intval($total_umur_range['female'][$i]));
            $table_data[$i." tahun"] = $gender_arr + array('total' => intval($total_umur_range['total'][$i]));
          }
          $gender_arr = array('male' => intval($total_umur['male'][-1]), 'female' => intval($total_umur['female'][-1]));
          $table_data[$umur_limit." tahun ke atas"] = $gender_arr + array('total' => intval($total_umur['total'][-1]));
          break;

        case 'ktp':
          $gender_arr = array('male' => intval($wajib_ktp['male']), 'female' => intval($wajib_ktp['female']));
          $table_data['Wajib KTP'] = $gender_arr + array('total' => intval($wajib_ktp['total']));

          $gender_arr = array('male' => intval($non_wajib_ktp['male']), 'female' => intval($non_wajib_ktp['female']));
          $table_data['NON Wajib KTP'] = $gender_arr + array('total' => intval($non_wajib_ktp['total']));
          break;

        case 'akte_mati':
          $gender_arr = array('male' => intval($akta_mati['male']), 'female' => intval($akta_mati['female']));
          $table_data['Akta Mati'] = $gender_arr + array('total' => intval($akta_mati['total']));
          
          // $gender_arr = array('male' => $non_akta_mati['male'], 'female' => $non_akta_mati['female']);
          // $table_data['NON Akta Mati'] = $gender_arr + array('total' => $non_akta_mati['total']);
          break;

        case 'akte_lahir':
          $gender_arr = array('male' => intval($akta_lahir['male']), 'female' => intval($akta_lahir['female']));
          $table_data['Akta Lahir'] = $gender_arr + array('total' => intval($akta_lahir['total']));
          
          $gender_arr = array('male' => intval($non_akta_lahir['male']), 'female' => intval($non_akta_lahir['female']));
          $table_data['NON Akta Lahir'] = $gender_arr + array('total' => intval($non_akta_lahir['total']));
          break;
        
        default:
          $table_data['Jenis Kelamin'] = $gender_arr + array('total' => intval($total_laki+$total_wanita));
          break;
      }
      return $table_data;
    } //endof recalc

  }*/


}