<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_keterangan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getkategori()
    {
        $sql = $this->db->get('MASTER_KATEGORI');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Kategori -';
                $result[$row['NO']] = ucwords(strtolower($row['DESCRIP']));
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getgaji()
    {
        $sql = $this->db->get('MASTER_GAJI');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Kisaran Penghasilan -';
                $result[$row['NO']] = ucwords(strtolower($row['DESCRIP']));
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function gethunian()
    {
        $sql = $this->db->get('MASTER_HUNIAN');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Kategori Hunian -';
                $result[$row['NO']] = ucwords(strtolower($row['DESCRIP']));
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }
}
