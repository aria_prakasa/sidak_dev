<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_dropdown extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->db = $this->load->database('default', true);
    }

    public function getkecamatanlist()
    {
        /*$result = array();
        $this->db->select('*');
        $this->db->from('SETUP_KEC');
        $this->db->order_by('NO_KEC','ASC');
        $array_keys_values = $this->db->get();*/
        $this->db->order_by('NO_KEC', 'ASC');
        //$query = $this->db->get('SETUP_KEC');
        $array_keys_values = $this->db->get('SETUP_KEC');

        //var_dump($this->db->last_query());die;
        /*foreach ($array_keys_values->result() as $row)
        {
        $result[0]= '-Pilih Kecamatan-';
        $result[$row->no_kec]= $row->nama_kec;
        }*/
        //var_dump($result);die;
        // return $result;
        if ($array_keys_values->num_rows() > 0) {
            return keysToLower($array_keys_values->result());
        }

        return false;

    }

    public function getkelurahanlist()
    {
        $no_kec = $this->input->post('no_kec');
        $result = array();
        $this->db->select('*');
        $this->db->from('SETUP_KEL');
        $this->db->where('NO_KEC', $no_kec);
        $this->db->order_by('NO_KEL', 'ASC');
        $array_keys_values = $this->db->get();
        foreach ($array_keys_values->result() as $row) {
            $result[0]            = '-Pilih Kota / Kabupaten-';
            $result[$row->no_kel] = $row->nama_kel;
        }

        return $result;
    }

}
