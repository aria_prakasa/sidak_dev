<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Vbiodata_plus extends Otable
{

    public $table_name = "GETBIODATA_PLUS";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null, $type = "ID")
    {
        if (!empty($ID)) {
            $arr = array("NIK" => $ID);
            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->NIK;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->NIK;
        }
        return false;
    }

    public function get_list($offset = 0, $limit = 0, $orderby = "", $where = "")
    {
        if (!empty($where) && trim($where) != "") {
            $this->db->where($where, null, false);
        }

        if (!empty($orderby) && trim($orderby) != "") {
            $this->db->order_by($orderby);
        }

        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }

        // $this->db->select('*', FALSE);
        $res = $this->db->get($this->table_name);
         // var_dump($this->db->last_query());die;
        if (emptyres($res)) {
            return false;
        }

        return $res->result();
    }

    public function getbioPlus($kd_prop,$kd_kab,$kd_kec,$kd_kel,$nik,$no_kk,$nama)
    {
        $this->db->where(array('NO_PROP' => $kd_prop, 'NO_KAB' => $kd_kab));
        if ($no_kk) {
            $this->db->where(array('NO_KK' => $no_kk));
        }
        if ($kd_kec) {
            $this->db->where(array('NO_KEC' => $kd_kec));
        }
        if ($kd_kel) {
            $this->db->where(array('NO_KEL' => $kd_kel));
        }
        if ($nik) {
            $this->db->where(array('NIK' => $nik));
        }
        if ($no_kk) {
            $this->db->where(array('NO_KK' => $no_kk));
        }
        if ($nama) {
            $this->db->like('NAMA_LGKP', $nama, 'both'); 
        }
        $res = $this->db->get($this->table_name);
        
        if (emptyres($res)) {
            return false;
        }

        return $res->result();
    }
    /*public function get_pob()
    {
        return $this->row->TMPT_LHR;
    }

    public function get_dob()
    {
        // return parse_date_ora($this->row->TGL_LHR, "d-m-Y");

        $tgl_lhr = parse_date_ora($this->row->TGL_LHR, "d-m-Y");
        // resinc Year with current Year
        if (end(explode("-", $tgl_lhr)) > date('Y')) {
            $tgl_lhr = date("d-m-Y", strtotime("{$tgl_lhr} -100 year"));
        }
        // $this->row->TGL_LHR = $tgl_lhr;
        // $tgl_lhr = get_date_lang($tgl_lhr, "EN");
        return $tgl_lhr;
    }*/

}
