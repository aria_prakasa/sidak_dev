<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_wilayah extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function ambil_provinsi()
    {
        $sql_prov = $this->db->get('SETUP_PROP');
        if ($sql_prov->num_rows() > 0) {
            foreach ($sql_prov->result_array() as $row) {
                // $result['']= '- Pilih Provinsi -';
                $result[$row['NO_PROP']] = ucwords(strtolower($row['NAMA_PROP']));
            }
            return $result;
        }
    }

    public function ambil_kabupaten($kode_prop = 35)
    {
        $this->db->where('NO_PROP', $kode_prop);
        $this->db->order_by('NO_KAB', 'ASC');
        $sql_kabupaten = $this->db->get('SETUP_KAB');
        if ($sql_kabupaten->num_rows() > 0) {

            foreach ($sql_kabupaten->result_array() as $row) {
                // $result['']= '- Pilih Kota/Kab -';
                $result[$row['NO_KAB']] = ucwords(strtolower($row['NAMA_KAB']));
            }
        } else {
            $result[''] = '- Belum Ada Kabupaten -';
        }
        return $result;
    }

    public function ambil_kecamatan($kode_kab = 75)
    {
        $this->db->where('NO_KAB', $kode_kab);
        
        $this->db->order_by('NO_KEC', 'ASC');
        $sql_kecamatan = $this->db->get('SETUP_KEC');
        if ($sql_kecamatan->num_rows() > 0) {
            foreach ($sql_kecamatan->result_array() as $row) {
                $result['']             = '- Pilih Kecamatan -';
                $result[$row['NO_KEC']] = ucwords(strtolower($row['NAMA_KEC']));
            }            
        } else {
            $result[''] = '- Belum Ada Kecamatan -';
        }
        return $result;
    }

    public function ambil_kelurahan($kode_kec)
    {
        
        $this->db->where('NO_KEC', $kode_kec);
        $this->db->order_by('NO_KEL', 'ASC');
        $sql_kelurahan = $this->db->get('SETUP_KEL');
        if ($sql_kelurahan->num_rows() > 0) {

            foreach ($sql_kelurahan->result_array() as $row) {
                $result['']                                    = '- Pilih Kelurahan -';
                $result[$row['NO_KEC'] . "-" . $row['NO_KEL']] = ucwords(strtolower($row['NAMA_KEL']));
            }
        } else {
            $result[''] = '- Pilih Kecamatan Terlebih Dahulu -';
        }
        return $result;
    }

    public function getWilayah($kd_prop, $kd_kab, $kd_kec, $kd_kel)
    {
        if ($kd_prop) {
            $this->db->select('NAMA_PROP');
            $this->db->where('NO_PROP', $kd_prop);
        }
        if ($kd_kab) {
            $this->db->select('NAMA_KAB');
            $this->db->where('NO_KAB', $kd_kab);
        }
        if ($kd_kec) {
            $this->db->select('NAMA_KEC');
            $this->db->where('NO_KEC', $kd_kec);
        }
        if ($kd_kel) {
            $this->db->select('NAMA_KEL');
            $this->db->where('NO_KEL', $kd_kel);
        }

        $query = $this->db->get('GETDATAVALID');
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getdataWilayah($no_prop = 35, $no_kab = 75, $no_kec, $no_kel)
    {
        $this->db->select('COUNT(*) AS TOTAL', false);
        $this->db->where('NO_PROP', $no_prop);
        $this->db->where('NO_KAB', $no_kab);
        if ($no_kec) {
            $this->db->where('NO_KEC', $no_kec);
        }
        if ($no_kel) {
            $this->db->where('NO_KEL', $no_kel);
        }
        $this->db->where('FLAG_STATUS', 0);
        $query = $this->db->get('DATA_KELUARGA');
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }
}
