<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Vbiodata_mohon extends Otable
{

    public $table_name = "GETBIODATAMOHON";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null, $type = "ID")
    {
        if (!empty($ID)) {
            $arr = array("NIK" => $ID);
            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->NIK;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->NIK;
        }
        return false;
    }

    public function get_pob()
    {
        return $this->row->TMPT_LHR;
    }

    public function get_dob()
    {
        // return parse_date_ora($this->row->TGL_LHR, "d-m-Y");

        $tgl_lhr = parse_date_ora($this->row->TGL_LHR, "d-m-Y");
        // resinc Year with current Year
        if (end(explode("-", $tgl_lhr)) > date('Y')) {
            $tgl_lhr = date("d-m-Y", strtotime("{$tgl_lhr} -100 year"));
        }
        // $this->row->TGL_LHR = $tgl_lhr;
        // $tgl_lhr = get_date_lang($tgl_lhr, "EN");
        return $tgl_lhr;
    }

}
