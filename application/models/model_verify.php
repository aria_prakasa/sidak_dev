<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_verify extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->db = $this->load->database('default', true);
    }

    public function getdatamohon()
    {
        $query = $this->db->get('GETDATAMOHON');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getdatatinggal($kk)
    {
        $this->db->where(array('NO_KK' => $kk));
        $query = $this->db->get('GETDATAMOHON');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getdataanggota($kk)
    {
        $this->db->where(array('NO_KK' => $kk));
        $query = $this->db->get('GETBIODATAMOHON');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function insert_datapddk($params = array())
    {
        if (count($params) <= 0) {
            return false;
        }

        $datas = null;
        foreach ($params as $values) {
            $data = null;
            foreach ($values as $key => $value) {
                $key        = strtoupper($key);
                $data[$key] = $value;
            }
            $datas[] = $data;
        }
        //var_dump($datas);die;
        $res = $this->db->insert_batch('BIODATA_PDDK_MISKIN', $datas);
        // die(var_dump($this->db->last_query()));

        return $res;
    }

    public function getbiodata_mohon($no_kk)
    {
        $this->db->where(array('NO_KK' => $no_kk));
        $query = $this->db->get('MHN_BIODATA_PDDK_MISKIN');

        //var_dump($this->db->last_query());die;
        //var_dump($query);die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    /*public function getdata_mohon($no_kk)
    {
        $this->db->where(array('NO_KK' => $no_kk));
        $query = $this->db->get('MHN_DATA_TINGGAL');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }*/

    /*public function update_status($no_kk)
    {
        $this->db->set('SIDAK_STATUS', '2');
        $this->db->where(array('NO_KK' => $no_kk));
        $query = $this->db->update('MHN_BIODATA_PDDK_MISKIN');

        $this->db->set('SIDAK_STATUS', '2');
        $this->db->where(array('NO_KK' => $no_kk));
        $query = $this->db->update('MHN_DATA_TINGGAL');
    }*/
}
