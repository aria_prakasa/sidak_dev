<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ouser extends Otable
{

    public $table_name = "USERS";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null, $type = "ID")
    {
        if (!empty($ID)) {
            $arr = array("USERNAME" => $ID);
            if ($type != "ID") {
                $arr = array("NIP" => $ID);
            }

            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->USERNAME;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->USERNAME;
        }
        return false;
    }

    public function add($arr)
    {
        return $this->db->insert($this->table_name, $arr);
        // return $this->db->insert_id();
    }

    public function edit($arr)
    {
        $res = $this->db->update($this->table_name, $arr, array("USERNAME" => $this->ID));
        // var_dump($this->db->last_query());die;
        return $res;
    }

    public function delete()
    {
        return $this->db->delete($this->table_name, array("USERNAME" => $this->ID));
    }

    public function is_kab()
    {
        if (!$this->row) {
            return false;
        }

        if (
            intval($this->row->USER_LEVEL) === 1
            ||
            (
                intval($this->row->NO_KAB) > 0 && intval($this->row->NO_KEC) <= 0 && intval($this->row->NO_KEL) <= 0)
        ) {
            return true;
        }

        return false;
    }

    public function is_kec()
    {
        if (!$this->row) {
            return false;
        }

        if (intval($this->row->USER_LEVEL) === 2
            ||
            (
                intval($this->row->NO_KAB) > 0 && intval($this->row->NO_KEC) > 0 && intval($this->row->NO_KEL) <= 0)
        ) {
            return true;
        }

        return false;
    }

    public function is_kel()
    {
        if (!$this->row) {
            return false;
        }

        if (intval($this->row->USER_LEVEL) === 3
            ||
            (
                intval($this->row->NO_KAB) > 0 && intval($this->row->NO_KEC) > 0 && intval($this->row->NO_KEL) > 0)
        ) {
            return true;
        }

        return false;
    }

    public function get_prop_code()
    {
        return $this->row->NO_PROP;
    }

    public function get_kab_code()
    {
        // if($this->is_prop()) return;
        return $this->row->NO_PROP . "-" . $this->row->NO_KAB;
    }

    public function get_kec_code()
    {
        if ($this->is_kab()) {
            return;
        }

        return $this->row->NO_PROP . "-" . $this->row->NO_KAB . "-" . $this->row->NO_KEC;
    }

    public function get_kel_code()
    {
        if ($this->is_kab() || $this->is_kec()) {
            return;
        }
        $this->is_kab();

        return $this->row->NO_PROP . "-" . $this->row->NO_KAB . "-" . $this->row->NO_KEC . "-" . $this->row->NO_KEL;
    }

    public function get_filter_location_where()
    {
        // filter current user location
        $where_arr   = [];
        $where_arr[] = "NO_PROP = " . NO_PROP;
        $where_arr[] = "NO_KAB = " . NO_KAB;
        if ($this->is_kel()) {
            $where_arr[] = "NO_KEC = " . $this->row->NO_KEC;
            $where_arr[] = "NO_KEL = " . $this->row->NO_KEL;
        } else if ($this->is_kec()) {
            $where_arr[] = "NO_KEC = " . $this->row->NO_KEC;
        }
        $where = implode(" AND ", $where_arr);
        return $where;
    }

}
