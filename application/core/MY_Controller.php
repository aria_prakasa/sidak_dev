<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MY_Controller extends CI_Controller
{

    public $cu   = false,
    $ca          = false,
    $OProp       = false,
    $OKab        = false,
    $OKec        = false,
    $OKel        = false,
    $OU          = false,
    $global_data = false;

    public function __construct()
    {
        parent::__construct();

        // var_dump(md5('Pasuruan!'));

        // DISABLE BROWSER CACHE
        /*if (!$this->load->database('pullout')) {
            // die('Silahkan koneksikan VPN terlebih dahulu.');
            echo "<script>alert('Silahkan koneksikan VPN terlebih dahulu.');</script>";
        }

        var_dump($this->load->database('default',TRUE));die;
        try {
            $this->db_pullout = $this->load->database('pullout', true);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n\n";
            die('Silahkan koneksikan VPN terlebih dahulu.');
        }*/

        $this->output->nocache();

        $this->global_data['html_title'] = "SIDAK - Pendispenduk Capil Kota Pasuruan";
        $this->global_data['page_title'] = "Dashboard";

        // autoloads
        $autoloads = array(
            'helpers'   => array('xcommon', 'xauth', 'xnotif', 'xtime', 'xform'),
            'libraries' => null,
            'models'    => array(
                'ouser',
                'setup_prop',
                'setup_kab',
                'setup_kec',
                'setup_kel',
                'master_pekerjaan',
            ),
        );
        $this->load->library($autoloads['libraries']);
        $this->load->model($autoloads['models']);
        $this->load->helper($autoloads['helpers']);

        // define as global
        $this->master_pekerjaan = new Master_pekerjaan;

        // load CI Indonesia language pack
        $this->lang->load('form_validation', 'indonesian');

        $this->cu = $cu = get_logged_in_user();
        $this->ca = $ca = get_current_admin();

        if ($cu) {
            $OU = new Ouser;
            // must change keys to UPPERCASE
            $cu_upper = array_change_key_case((array) $cu, CASE_UPPER);
            // setup row to lib
            $OU->setup($cu_upper);
            $this->OU = $OU;

            extract(get_object_vars($cu));
            // setup object
            if (!empty($NO_PROP)) {
                $this->OProp = $OProp = new Setup_prop($NO_PROP);
            }
            if (!empty($NO_PROP) && !empty($NO_KAB)) {
                $this->OKab = $OKab = new Setup_kab($NO_PROP, $NO_KAB);
            }
            if (!empty($NO_PROP) && !empty($NO_KAB) && !empty($NO_KEC)) {
                $this->OKec = $OKec = new Setup_kec($NO_PROP, $NO_KAB, $NO_KEC);
            }
            if (!empty($NO_PROP) && !empty($NO_KAB) && !empty($NO_KEC) && !empty($NO_KEL)) {
                $this->OKel = $OKel = new Setup_kel($NO_PROP, $NO_KAB, $NO_KEC, $NO_KEL);
            }
        }

    }

    public function __destruct()
    {
        unset($this->cu);
        unset($this->ca);
        unset($this->OU);
        unset($this->OProp);
        unset($this->OKab);
        unset($this->OKec);
        unset($this->OKel);
        unset($this->global_data);
    }

    protected function _init_logged_in()
    {
        $cu = $this->cu;
        if (!$cu) {
            $this->session->set_flashdata("error", "Silahkan login terlebih dahulu.");
            user_login();
            exit;
        }
    }

    protected function _init_wilayah()
    {
        // WILAYAH
        // no
        $this->global_data['no_prop'] = $this->cu->NO_PROP;
        $this->global_data['no_kab']  = $this->cu->NO_KAB;
        $this->global_data['no_kec']  = $this->cu->NO_KEC;
        $this->global_data['no_kel']  = $this->cu->NO_KEL;
        // nama
        $this->global_data['nama_prop'] = $this->OProp->get_name();
        $this->global_data['nama_kab']  = $this->OKab->get_name();
        if ($this->OKec) {
            $this->global_data['nama_kec'] = $this->OKec->get_name();
        }

        if ($this->OKel) {
            $this->global_data['nama_kel'] = $this->OKel->get_name();
        }

    }

    protected function _init_penduduk()
    {
        // PENDUDUK
        if ($this->cu) {
            if ($this->OU->is_kab()):
                $penduduk_totals = $this->OKab->get_penduduk_totals();
                $total_kk        = $this->OKab->get_kk_total();
            elseif ($this->OU->is_kec()):
                // die(var_dump("KEC"));
                $penduduk_totals = $this->OKec->get_penduduk_totals();
                $total_kk        = $this->OKec->get_kk_total();
            elseif ($this->OU->is_kel()):
                $penduduk_totals = $this->OKel->get_penduduk_totals();
                $total_kk        = $this->OKel->get_kk_total();
            endif;
        } else {
            $this->OProp     = $OProp     = new Setup_prop(NO_PROP);
            $this->OKab      = $OKab      = new Setup_kab(NO_PROP, NO_KAB);
            $penduduk_totals = $OKab->get_penduduk_totals();
            $total_kk        = $OKab->get_kk_total();
        }
        $pt_row                              = json_decode($penduduk_totals);
        $this->global_data['total_penduduk'] = $pt_row->total_penduduk;
        $this->global_data['total_male']     = $pt_row->total_male;
        $this->global_data['total_female']   = $pt_row->total_female;
        $this->global_data['total_kk']       = $total_kk;
    }

}

class ADM_Controller extends CI_Controller
{

    public $ca   = false,
    $OA          = false,
    $global_data = false;

    public function __construct()
    {
        parent::__construct();

        // DISABLE BROWSER CACHE
        $this->output->nocache();

        $this->global_data['html_title'] = "Admin Panel | SIDAK - Pendispenduk Capil Kota Pasuruan";
        $this->global_data['page_title'] = "Dashboard";

        // autoloads
        $autoloads = array(
            'helpers'   => array('xcommon', 'xauth', 'xnotif', 'xtime', 'xform'),
            'libraries' => array('template'),
            'models'    => array(
                'oadmin',
                'ouser',
                'setup_prop',
                'setup_kab',
                'setup_kec',
                'setup_kel',
                'master_user',
            ),
        );
        $this->load->library($autoloads['libraries']);
        $this->load->model($autoloads['models']);
        $this->load->helper($autoloads['helpers']);
        // load CI Indonesia language pack
        $this->lang->load('form_validation', 'indonesian');

        $this->ca = $ca = get_current_admin();
        if ($ca) {
            $OA = new Oadmin;
            $OA->setup($ca);
            $this->OA = $OA;
        }

    }

    public function __destruct()
    {
        unset($this->ca);
        unset($this->OA);
        unset($this->global_data);
    }

    protected function _init_logged_in()
    {
        $ca = $this->ca;
        if (!$ca) {
            $this->session->set_flashdata("error", "Silahkan login terlebih dahulu.");
            admin_login();
            exit;
        }
    }

}
