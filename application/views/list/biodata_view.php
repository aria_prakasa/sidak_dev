<div class="box">
  <div class="box-body" style="padding-right: 15px; padding-top: 15px; padding-left: 15px;">
    <div class="box-header with-border bg-success">
          <h3 class="box-title"><b>Biodata Penduduk Miskin</b></h3>
    </div>
    </br>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
          	<label class="col-sm-4 control-label">No KK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_kk"; ?> ">
            </div>
          </div>
          <div class="form-group">
          	<label class="col-sm-4 control-label">NIK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nik"; ?> ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Nama Lengkap</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_lgkp"; ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Jenis Kelamin</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$jenis_klmin"; ?> " style="text-transform:uppercase">
              </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Agama</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$agama"; ?> " style="text-transform:uppercase">
              </div>
          </div>

        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Tempat Lahir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$tmpt_lhr"; ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Tanggal Lahir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo format_tanggal($tgl_lhr); ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Pendidikan Akhir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$pddk_akh"; ?> " style="text-transform:uppercase">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Jenis Pekerjaan</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$jenis_pkrjn"; ?> " style="text-transform:uppercase">
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
      <div class="box-header bg-danger">
          <h3 class="box-title"><b>Data Kemiskinan</b></h3>
      </div><!-- /.box-header -->
      </br>
      <?php echo form_open('listing_biodata/simpan/'); ?>
      <?php echo form_hidden('no_kk', $no_kk); ?>
      <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Status BPJS</label>
              <div class="col-sm-8"><?php
if ($bpjs_status == 1) {
	echo '<span class="label label-success">Aktif</span>';
	//echo '<td align="left">';
} else {
	echo '<span class="label label-danger">Tidak Aktif</span>';
}?>
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">No Peserta BPJS</label>
              <div class="col-sm-8">
                <input type="text" class="form-control input-sm" name="bpjs" id="bpjs" value="<?php echo "$no_bpjs" ?>" style="text-transform:uppercase" />
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kategori bantuan</label>
            <div class="col-sm-8">
              <!-- <input type="text" name="kk" id="kk" class="form-control" value="<?php echo "$kategori"; ?> "> -->
              <?php echo form_dropdown('kategori', $opsi_kategori, $jenis_bpjs, 'class="form-control input-sm"'); ?>
            </div>
          </div>
          <!-- <div class="form-group">
            <label class="col-sm-4 control-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" name="field4" id="field4" class="form-control" value="<?php echo "$field4"; ?> ">
              </div>
          </div> -->
          <div class="form-group">
            <div class="col-sm-4">
              <a class="btn bg-navy" href="<?php echo base_url(); ?>index.php/listing_biodata/index/1" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
            </div>
            <div class="col-sm-4">
                <button type="submit" name="submit" value="<?php echo $nik; ?>" class="btn bg-olive">Simpan Data <i class="fa fa-save"></i></button>
            </div>
            <div class="col-sm-4">
                <button type="submit" name="hapus" value="<?php echo $nik; ?>" class="btn bg-red" onclick="return confirm('Yakin akan menghapus data ini ?')">Hapus Data <i class="fa fa-times"></i></button>
                <!-- <button class="btn bg-red" onclick="return confirm('Yakin akan menghapus data ini ?')" href="<?php echo base_url(); ?>index.php/raskin/hapus/<?php echo $no_kk; ?>">Hapus Data <i class="fa fa-times"></i></button> -->
            </div>
          </div>

        </div>
      </div>
      </div>
      <?php echo form_close(); ?>

      <?php
/*if ($nik == TRUE)
{
echo '<div class="row">';
echo '<div class="col-sm-8"></div>';
echo '<div class="col-sm-2">';
echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing_biodata/'?><?php echo '" ><button class="btn bg-navy margin"><i class="fa fa-arrow-left">&nbsp;&nbsp; Kembali</i></button></a></div>';
echo '<div class="col-sm-2">';
echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing_biodata/simpan/'?><?php echo $row->nik; ?><?php echo '" ><button class="btn bg-olive margin">Simpan &nbsp;&nbsp;<i class="fa fa-save"></i></button></a></div>';
}*/
?>
    </div>
	</div><!-- /.box-body -->
</div><!-- /.box -->