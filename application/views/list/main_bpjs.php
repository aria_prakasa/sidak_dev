<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><strong>Daftar Biodata Peserta BPJS</strong></h3>
        <div class="box-tools pull-right">
           <!--  <?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
             -->
        </div>
    </div>

    <div class="box-body">
    <?php $this->load->view('tpl_filter');?>

	<?php
if (!$jml_data): ?>
    &nbsp;
    <?php else: ?>

        <div class="table-responsive">
    			<table id="example1" class="table table-bordered table-striped">
    		<?php if (!$biodata_list): ?>
        		<tr>
        			<td colspan="9">
	        		&nbsp;
	        		</td>
        		</tr>
	        <?php else: ?>
	          <thead>
		          <tr>
		            <th width="5%">NO</th>
		            <th>NO KK</th>
		            <th>NIK</th>
		            <th width="20%">NAMA LENGKAP</th>
		            <th>JENIS_KLMIN</th>
		            <th>TGL LHR</th>
		            <th>PEKERJAAN SIDAK</th>
		            <th>STATUS KELUARGA</th>
		            <th>ALAMAT</th>
		            <th>RT</th>
		            <th>RW</th>
		            <th>KELURAHAN</th>
		            <th>KECAMATAN</th>
		            <th>STATUS</th>
		            <!-- <th>OPERASI</th> -->
		          </tr>
	          </thead>
	          <tbody>
	            <?php
$no = 1;
foreach ($biodata_list as $row) {
	$row = keysToLower($row);
	extract((array) $row);
	?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo "$no_kk"; ?></td>
                      <td><?php echo "$nik"; ?></td>
                      <td><?php echo "$nama_lgkp"; ?></td>
                      <td><?php echo "$jenis_klmin"; ?></td>
                      <td><?php echo format_tanggal($tgl_lhr) ?></td>
                      <td><?php echo "$jenis_pkrjn"; ?></td>
                      <td><?php echo strtoupper($stat_hbkel); ?></td>
                      <td><?php echo "$alamat"; ?></td>
                      <td><?php echo "$no_rt"; ?></td>
                      <td><?php echo "$no_rw"; ?></td>
                      <td><?php echo "$nama_kel"; ?></td>
                      <td><?php echo "$nama_kec"; ?></td>
                      <td align="center"><?php
if ($sidak_status == 0) {
		echo '<span class="label label-success">verifikasi ok</span>';
		//echo '<td align="left">';
	} else {
		echo '<span class="label label-danger">status berubah</span>';

		/*echo '<td align="left">';
			    	                    echo '<a data-original-title="Edit" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;';
		*/
	}?>
                           <!--  &nbsp;<a data-original-title="Lihat Biodata" href="<?php echo base_url(); ?><?php echo 'index.php/listing_biodata/lihat/' ?><?php echo $row->nik; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp; -->
                      </td>
                      <!-- <td align="center">
                        <a data-original-title="Lihat" href="<?php echo base_url(); ?><?php echo 'index.php/listing_biodata/lihat/' ?><?php echo $row->nik; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
                        <a data-original-title="Hapus" href="<?php echo base_url(); ?><?php echo 'index.php/listing/hapus/' ?><?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-times"></i></a>
                      </td> -->
                    </tr>
                      <?php
}
?>
	          <?php endif;?>
	          </tbody>
	        </table>
	    </div>
	<?php endif;?>
	</div>
</div><!-- /.box -->