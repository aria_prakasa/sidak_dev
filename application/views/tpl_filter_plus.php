<script type="text/javascript">
function toggleStatusKk() {
    if ($('#toggleElementKk').is(':checked')) {
        $('#toggleKk :input').removeAttr('disabled');
    } else {
        $('#toggleKk :input').attr('disabled', true);
        $('#toggleKk :input').val("");
    }
}
function toggleStatusNik() {
    if ($('#toggleElementNik').is(':checked')) {
        $('#toggleNik :input').removeAttr('disabled');
    } else {
        $('#toggleNik :input').attr('disabled', true);
        $('#toggleNik :input').val("");
    }
}
function toggleStatusNama() {
    if ($('#toggleElementNama').is(':checked')) {
        $('#toggleNama :input').removeAttr('disabled');
    } else {
        $('#toggleNama :input').attr('disabled', true);
        $('#toggleNama :input').val("");
    }
}
</script>

<form id="check" method="post" class="form-horizontal" action="">
<div class="row">
	<?php
$provinsi = $this->model_wilayah->ambil_provinsi();
$kabupaten = $this->model_wilayah->ambil_kabupaten();
$kecamatan = $this->model_wilayah->ambil_kecamatan();
?>
    <div class="col-sm-6">
      <div class="form-group">
        <label class="col-sm-4 control-label">Propinsi</label>
        <div class="col-sm-8">
        <?php
$style_provinsi = 'class="form-control input-sm" id="provinsi_id"';
echo form_dropdown('provinsi_id', $provinsi, 35, $style_provinsi);
?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kota/Kab</span></label>
        <div class="col-sm-8">
        <?php
$style_kabupaten = 'class="form-control input-sm" id="kabupaten_id"';
echo form_dropdown("kabupaten_id", $kabupaten, 75, $style_kabupaten);
?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kecamatan</label>
        <div class="col-sm-8">
        <?php
$style_kecamatan = 'class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
echo form_dropdown("kecamatan_id", $kecamatan, $kecamatan_id, $style_kecamatan);
?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kelurahan</label>
        <div class="col-sm-8">
        	<div id="kelurahan_id_wrap" data-id="<?php echo $kelurahan_id; ?>">
            <?php
$style_kelurahan = 'disabled class="form-control input-sm" id="kelurahan_id"';
echo form_dropdown("kelurahan_id", array('Pilih Kelurahan' => '- Pilih Kecamatan Terlebih Dahulu -'), $kelurahan_id, $style_kelurahan);
?>
            </div>
        </div>
      </div>
      <div class="form-group">
        	<div class="col-sm-4"></div>
            <div class="col-sm-8">
                <button type="submit" class="btn bg-maroon"><span class="fa fa-search"></span>&nbsp;Tampilkan</button>
            </div>
      	</div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
              <div align="left">
                  <label class="control-label">
                      <input <?php echo $toggleKk ?> id="toggleElementKk" type="checkbox" name="toggle" onchange="toggleStatusKk()"  />&nbsp;&nbsp;Nomor KK
                  </label>
              </div>
          </div>
          <div class="col-md-6" id="toggleKk">
              <input disabled placeholder="masukkan no.KK 16 digit angka" type="text" class="form-control input-sm" name="inputkk" id="inputkk" value="<?php echo "$inputkk" ?>" maxlength="16" />
          </div>
        </div>
      </div>
      <?php if ($this->uri->segment(1) == 'bpjs'): ?>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
              <div align="left">
                  <label class="control-label">
                      <input <?php echo $toggleNik ?> id="toggleElementNik" type="checkbox" name="toggle" onchange="toggleStatusNik()" />&nbsp;&nbsp;NIK
                  </label>
              </div>
          </div>
          <div class="col-sm-6" id="toggleNik">
              <input disabled placeholder="masukkan NIK 16 digit angka" type="text" class="form-control input-sm" name="inputnik" id="inputnik" value="<?php echo "$inputnik" ?>" maxlength="16" />
          </div>
        </div>
      </div>
      <?php endif;?>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
              <div align="left">
                  <label class="control-label">
                      <input <?php echo $toggleNama ?> id="toggleElementNama" type="checkbox" name="toggle" onchange="toggleStatusNama()" />&nbsp;&nbsp;Nama Lengkap
                  </label>
              </div>
          </div>
          <div class="col-sm-6" id="toggleNama">
              <input disabled="disabled" placeholder="masukkan Nama" type="text" class="form-control input-sm" name="inputnama" id="inputnama" value="<?php echo "$inputnama" ?>" style="text-transform:uppercase" />
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
            <div align="left">
            </div>
          </div>
          <div class="col-sm-6" >
            <div align="left"><span style="color:red"><i>* NO KK, NIK, Nama Lengkap salah satu wajib diisi</i></span>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('#check')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                inputnama: {
                    message: 'The username is not valid',
                    validators: {
                        /*notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },*/
                        stringLength: {
                            min: 3,
                            //max: 50,
                            message: 'Nama harus lebih dari 3 karakter'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[a-zA-Z \.]+$/,
                            message: 'Nama hanya dapat berisi huruf alphabet'
                        }
                    }
                },
                inputkk: {
                    message: 'The NO KK is not valid',
                    validators: {
                        /*notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },*/
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'NO KK harus berisi 16 digit'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'No KK hanya dapat berisi angka'
                        }
                    }
                },
                inputnik: {
                    message: 'The NIK is not valid',
                    validators: {
                        /*notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },*/
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'NIK harus berisi 16 digit'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'NIK hanya dapat berisi angka'
                        }
                    }
                }
            }
        })
        ;
});
</script>