<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDaK Online | Dashboard</title>
    <link rel="shortcut icon" href="<?php echo base_url('_assets/img/pemkot.png') ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/bootstrap/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/dist/css/skins/_all-skins.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/plugins/datatables/dataTables.bootstrap.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/plugins/iCheck/square/blue.css">
    <!-- FormValidation -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/plugins/formvalidation/dist/css/formValidation.css"/>

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>_assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>_assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>_assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>_assets/dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url(); ?>_assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url(); ?>_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url(); ?>_assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo base_url(); ?>_assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?php echo base_url(); ?>_assets/plugins/chartjs/Chart.min.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>_assets/plugins/datatables/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>_assets/plugins/datatables/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <!-- FormValidation -->
    <script type="text/javascript" src="<?php echo base_url(); ?>_assets/plugins/formvalidation/dist/js/formValidation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>_assets/plugins/formvalidation/dist/js/framework/bootstrap.js"></script>
    <!-- Masking -->
    <script type="text/javascript" src="<?php echo base_url(); ?>_assets/dist/js/jquery.mask.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script type="text/javascript" src="<?php echo base_url(); ?>_assets/plugins/chartjs/Chart.min.js"></script>

    <!-- page script -->
    <script>
      /*$(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "aoColumnDefs": [
         { "bSortable": false, "aTargets": [ 0 ] }
       ] } );
      });*/

      $(function() {
        $('#example1').dataTable( {
                "fnDrawCallback": function ( oSettings ) {
                    var that = this;

                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        this.$('td:first-child', {"filter":"applied"}).each( function (i) {
                            that.fnUpdate( i+1, this.parentNode, 0, false, false );
                        } );
                    }
                },
                "paging": true,
                "ordering": true,
                "responsive": true,
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            } );
        } );
    </script>

    <script>
    function formatNumber (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    $(document).ready(function() {
      $('#agregatMiskin').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over all pages
          /*total = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            } );*/

          // Total over this page
          pageTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          lakiTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          perempuaTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+lakiTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+perempuaTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregat').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over all pages
          /*total = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            } );*/

          // Total over this page
          pageTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
              //return formatNumber(a) + formatNumber(b);
              //return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          lakiTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          perempuaTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+lakiTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+perempuaTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatPkrjn').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          pageTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
              //return formatNumber(a) + formatNumber(b);
              //return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatAgama').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over this page
          aTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          bTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          cTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          dTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          eTotal  = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          fTotal = api
            .column( 7, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          gTotal = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          pageTotal = api
            .column( 9, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+aTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+bTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+cTotal +'');
          $( api.column( 5 ).footer() ).html(
            ''+dTotal +'');
          $( api.column( 6 ).footer() ).html(
            ''+eTotal +'');
          $( api.column( 7 ).footer() ).html(
            ''+fTotal +'');
          $( api.column( 8 ).footer() ).html(
            ''+gTotal +'');
          $( api.column( 9 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatGoldrh').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over this page
          aTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          bTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          cTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          dTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          eTotal  = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          fTotal = api
            .column( 7, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          gTotal = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          hTotal = api
            .column( 9, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          iTotal = api
            .column( 10, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          jTotal = api
            .column( 11, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          kTotal = api
            .column( 12, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          lTotal = api
            .column( 13, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          mTotal = api
            .column( 14, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          pageTotal = api
            .column( 15, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+aTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+bTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+cTotal +'');
          $( api.column( 5 ).footer() ).html(
            ''+dTotal +'');
          $( api.column( 6 ).footer() ).html(
            ''+eTotal +'');
          $( api.column( 7 ).footer() ).html(
            ''+fTotal +'');
          $( api.column( 8 ).footer() ).html(
            ''+gTotal +'');
          $( api.column( 9 ).footer() ).html(
            ''+hTotal +'');
          $( api.column( 10 ).footer() ).html(
            ''+iTotal +'');
          $( api.column( 11 ).footer() ).html(
            ''+jTotal +'');
          $( api.column( 12 ).footer() ).html(
            ''+kTotal +'');
          $( api.column( 13 ).footer() ).html(
            ''+lTotal +'');
          $( api.column( 14 ).footer() ).html(
            ''+mTotal +'');
          $( api.column( 15 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatPddkan').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over this page
          aTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          bTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          cTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          dTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          eTotal  = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          fTotal = api
            .column( 7, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          gTotal = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          hTotal = api
            .column( 9, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          iTotal = api
            .column( 10, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          jTotal = api
            .column( 11, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          pageTotal = api
            .column( 12, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+aTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+bTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+cTotal +'');
          $( api.column( 5 ).footer() ).html(
            ''+dTotal +'');
          $( api.column( 6 ).footer() ).html(
            ''+eTotal +'');
          $( api.column( 7 ).footer() ).html(
            ''+fTotal +'');
          $( api.column( 8 ).footer() ).html(
            ''+gTotal +'');
          $( api.column( 9 ).footer() ).html(
            ''+hTotal +'');
          $( api.column( 10 ).footer() ).html(
            ''+iTotal +'');
          $( api.column( 11 ).footer() ).html(
            ''+jTotal +'');
          $( api.column( 12 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatSHDK').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over this page
          aTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          bTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          cTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          dTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          eTotal  = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          fTotal = api
            .column( 7, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          gTotal = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          hTotal = api
            .column( 9, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          iTotal = api
            .column( 10, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          jTotal = api
            .column( 11, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          kTotal = api
            .column( 12, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          pageTotal = api
            .column( 13, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+aTotal +'');
          $( api.column( 3 ).footer() ).html(
            ''+bTotal +'');
          $( api.column( 4 ).footer() ).html(
            ''+cTotal +'');
          $( api.column( 5 ).footer() ).html(
            ''+dTotal +'');
          $( api.column( 6 ).footer() ).html(
            ''+eTotal +'');
          $( api.column( 7 ).footer() ).html(
            ''+fTotal +'');
          $( api.column( 8 ).footer() ).html(
            ''+gTotal +'');
          $( api.column( 9 ).footer() ).html(
            ''+hTotal +'');
          $( api.column( 10 ).footer() ).html(
            ''+iTotal +'');
          $( api.column( 11 ).footer() ).html(
            ''+jTotal +'');
          $( api.column( 12 ).footer() ).html(
            ''+kTotal +'');
          $( api.column( 13 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    $(document).ready(function() {
      $('#agregatKel').DataTable( {
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "aoColumnDefs": [
            { "bSortable": false } ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          // Total over all pages
          /*total = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            } );*/

          // Total over this page
          pageTotal = api
            .column( 2, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return formatNumber(intVal(a) + intVal(b));
            }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
            ''+pageTotal +'');
        }
      } );
    } );

    </script>

  </head>
  <body class="hold-transition skin-green sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>index.php/home" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?=base_url('_assets/img/pemkot.png')?>" alt="" height="40"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=base_url('_assets/img/pemkot.png')?>" alt="" height="40">&nbsp;<b>SIDaK</b><i><sub style="bottom:-0.7em;left:-24px;">online</sub></i></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs">Sistem Informasi Data Kemiskinan</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>_assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $this->cu->NAMA_LENGKAP;
                            // echo $this->cu->NO_KEC;
                            // echo $this->cu->NO_KEL;
                       ?>
                      <small>Member since <?php echo format_tanggal($this->cu->DATE_CREATED); ?></small>
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo site_url('setting'); ?>" class="btn btn-default btn-flat"><i class="fa fa-cog"></i> Pengaturan</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('home/logout'); ?>" class="btn btn-default btn-flat" onclick="return confirm('Yakin keluar?')"><i class="fa fa-power-off"></i> Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->

            </ul>
          </div>

        </nav>
      </header>