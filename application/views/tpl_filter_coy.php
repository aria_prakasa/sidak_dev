<form id="check" method="post" class="form-horizontal" action=""> 
<div class="row">
	<?php
	$provinsi 	= $this->model_wilayah->ambil_provinsi();
	$kabupaten  = $this->model_wilayah->ambil_kabupaten();
	$kecamatan  = $this->model_wilayah->ambil_kecamatan();
	?>
    <div class="col-sm-6">
      <div class="form-group">
        <label class="col-sm-4 control-label">Propinsi</label>
        <div class="col-sm-8">
        <?php
        $style_provinsi='class="form-control input-sm" id="provinsi_id"';
        echo form_dropdown('provinsi_id',$provinsi,35,$style_provinsi);
        ?>  
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kota/Kab</span></label>
        <div class="col-sm-8">
        <?php
        $style_kabupaten='class="form-control input-sm" id="kabupaten_id"';
        echo form_dropdown("kabupaten_id",$kabupaten,75,$style_kabupaten);
        ?>  
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kecamatan</label>
        <div class="col-sm-8">
        <?php
        if($this->uri->segment(1)=='listing_biodata'){
          if($this->cu->USER_LEVEL == 2 || $this->cu->USER_LEVEL == 3){
            $kecamatan_id = $this->cu->NO_KEC;
            $style_kecamatan='class="form-control input-sm" id="kecamatan_id"';
          }else{
            $style_kecamatan='class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
          }
        }else{
          $style_kecamatan='class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
        }
        //$style_kecamatan='class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
        echo form_dropdown("kecamatan_id",$kecamatan,$kecamatan_id,$style_kecamatan);
        ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Kelurahan</label>
        <div class="col-sm-8">
        	<div id="kelurahan_id_wrap" data-id="<?php echo $kelurahan_id; ?>">
            <?php
            // $kelurahan_id = $this->cu->NO_KEL;
            $style_kelurahan='disabled class="form-control input-sm" id="kelurahan_id"';
            echo form_dropdown("kelurahan_id",array('Pilih Kelurahan'=>'- Pilih Kecamatan Terlebih Dahulu -'),$kelurahan_id,$style_kelurahan);
            ?>
            </div>
        </div>
      </div>
      <div class="form-group">
        	<div class="col-sm-4"></div>
            <div class="col-sm-8">
                <button type="submit" class="btn bg-maroon"><span class="fa fa-search"></span>&nbsp;Tampilkan</button>
            </div>
      	</div>
    </div> 
<?php if($this->uri->segment(1)=='agr_penduduk' || $this->uri->segment(1)=='stat_penduduk'): ?>
    <div class="col-sm-6">
    	<div class="form-group">
            <label class="col-sm-4 control-label">Jenis Statistik <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <?php
              $options = array('' => ' - Pilih Statistik - ',
              			'1' => 'Agregat Penduduk Berdasarkan Jenis Kelamin',
              			'2' => 'Agregat Penduduk Berdasarkan Agama',
              			'3' => 'Agregat Penduduk Berdasarkan Golongan Darah',
              			'4' => 'Agregat Penduduk Berdasarkan Pendidikan',
              			'5' => 'Agregat Penduduk Berdasarkan Pekerjaan',
                    '6' => 'Agregat Penduduk Berdasarkan Umur 5 Tahunan',
              			'7' => 'Agregat Penduduk Berdasarkan Umur Khusus',
                    '8' => 'Agregat Penduduk Wajib KTP',
                    '9' => 'Agregat Penduduk Ber-KTP',
                    '10' => 'Agregat Penduduk >17 Tahun Ber-KTP',
                    '11' => 'Agregat Penduduk >17 Tahun Telah Menikah',
                    '12' => 'Agregat Kepala Keluarga'
                    );
              echo form_dropdown('kd_agr', $options, $kd_agr, 'class="form-control input-sm"');
              ?>
              <!-- <select class="form-control input-sm" name="kd_agr">
              	<option value=""> -- Pilih Statistik -- </option>
                <option value="1">Agregat Berdasarkan Jenis Kelamin</option>
                <option value="2">Agregat Berdasarkan Agama</option>
                <option value="3">Agregat Berdasarkan Golongan Darah</option>
                <option value="4">Agregat Berdasarkan Pendidikan</option>
                <option value="5">Agregat Berdasarkan Umur</option>
                <option value="6">Agregat Penduduk Wajib KTP</option>
              </select> -->
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Periode bulan <span class="text-danger">*</span></label>
            <div class="col-sm-3">
              <?php echo form_input('bulan', $bulan, 'placeholder="MM/YYYY" type="text" class="form-control input-sm" data-mask="00/0000" id="bulan"'); ?>
            </div>
        </div>
    </div>
<?php elseif($this->uri->segment(1)=='agr_miskin'): ?>
    <div class="col-sm-6">
      <div class="form-group">
            <label class="col-sm-4 control-label">Jenis Statistik <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <?php
              $options = array('' => ' - Pilih Statistik - ',
                    '1' => 'Penduduk Miskin Berdasarkan Jenis Kelamin',
                    '2' => 'Penduduk Miskin Berdasarkan Agama',
                    '3' => 'Penduduk Miskin Berdasarkan Golongan Darah',
                    '4' => 'Penduduk Miskin Berdasarkan Pendidikan',
                    '5' => 'Penduduk Miskin Berdasarkan Status Hubungan Keluarga', 
                    '6' => 'Penduduk Miskin Berdasarkan Umur',
                    '7' => 'Penduduk Miskin Berdasarkan Umur Sekolah dan Produktif',
                    '8' => 'Penduduk Miskin Wajib KTP',      
                    '9' => 'Penduduk Miskin Berdasarkan Jenis Pekerjaan',
                    '10' => 'Penduduk Miskin Berdasarkan Jenis Pekerjaan di SIAK',  
                    );
              echo form_dropdown('kd_agr', $options, $kd_agr, 'class="form-control input-sm"');
              ?>
              <!-- <select class="form-control input-sm" name="kd_agr">
                <option value=""> -- Pilih Statistik -- </option>
                <option value="1">Agregat Berdasarkan Jenis Kelamin</option>
                <option value="2">Agregat Berdasarkan Agama</option>
                <option value="3">Agregat Berdasarkan Golongan Darah</option>
                <option value="4">Agregat Berdasarkan Pendidikan</option>
                <option value="5">Agregat Berdasarkan Umur</option>
                <option value="6">Agregat Penduduk Wajib KTP</option>
              </select> -->
            </div>
        </div>
        <!-- <div class="form-group">
            <label class="col-sm-4 control-label">Periode bulan <span class="text-danger">*</span></label>
            <div class="col-sm-3">
              <?php echo form_input('bulan', $bulan, 'placeholder="MM/YYYY" type="text" class="form-control input-sm" data-mask="00/0000" id="bulan"'); ?>
            </div>
        </div> -->
    </div>
<?php elseif($this->uri->segment(1)=='listing_biodata'): ?>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
              <div align="left">
                  <label class="control-label">
                      <input id="toggleElementNik" type="checkbox" name="toggle" onchange="toggleStatusNik()" />&nbsp;&nbsp;NIK
                  </label>
              </div>
          </div>
          <div class="col-sm-6" id="toggleNik">
              <input disabled placeholder="masuNikan NIK 16 digit angka" type="text" class="form-control input-sm" name="nik" id="nik" value="<?php echo "$no_nik"?>" maxlength="16" />
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="col-sm-4">
              <div align="left">
                  <label class="control-label">
                      <input id="toggleElementNama" type="checkbox" name="toggle" onchange="toggleStatusNama()" />&nbsp;&nbsp;Nama Lengkap
                  </label>
              </div>
          </div>
          <div class="col-sm-6" id="toggleNama">
              <input disabled placeholder="masukkan Nama Lengkap" type="text" class="form-control input-sm" name="nama" id="nama" value="<?php echo "$nama"?>" maxlength="16" />
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>
</div> 
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('#check')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                nama: {
                    message: 'The username is not valid',
                    validators: {
                        /*notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },*/
                        stringLength: {
                            min: 3,
                            //max: 50,
                            message: 'Nama harus lebih dari 3 karakter'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[a-zA-Z \.]+$/,
                            message: 'Nama hanya dapat berisi huruf alphabet'
                        }
                    }
                },
                nik: {
                    message: 'The username is not valid',
                    validators: {
                        /*notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        },*/
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'NIK harus berisi 16 digit'
                        },
                        /*remote: {
                            url: 'remote.php',
                            message: 'The username is not available'
                        },*/
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'NIK hanya dapat berisi angka'
                        }
                    }
                }
            }
        });
});
</script>