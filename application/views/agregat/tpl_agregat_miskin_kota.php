<?php 
if($agregat):
	//var_dump($agregat);die;
?>
	<div class="box-body">
		<div class="table-responsive">		
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
						</br>
						<table id="agregatMiskin" class="table table-bordered table-striped">
						<?php if(!$agregat): ?>
			        		<tr>
			        			<td colspan="6">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title" >NAMA KECAMATAN</th>
				            <th class="chart value" width="15%">LAKI-LAKI</th>
				            <th class="chart value" width="15%">PEREMPUAN</th>
				            <th class="chart total" width="15%">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregat as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
			                  <td><?php echo $no++; ?></td>
			                  <td class="chart title" ><?php echo "$nama_kec"; ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($lk); ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($pr); ?></td>
			                  <td class="chart total" style="text-align:right"><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php 
elseif($agregatAgama):
	// var_dump($agregatAgama); die;
?>
	<div class="box-body">
		<div class="table-responsive">	
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregatAgama" class="table table-bordered table-striped">
						<?php if(!$agregatAgama): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">NAMA KECAMATAN</th>
				            <th class="chart value">ISLAM</th>
							<th class="chart value">KRISTEN</th>
							<th class="chart value">KATHOLIK</th>
							<th class="chart value">HINDU</th>
							<th class="chart value">BUDHA</th>
							<th class="chart value">KONGHUCHU</th>
							<th class="chart value">LAINNYA</th>
							<th class="chart total">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatAgama as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
		                    <td><?php echo $no++; ?></td>
		                    <td class="chart title"><?php echo "$nama_kec"; ?></td>
		                    <td class="chart value" style='text-align:right'><?php echo number_format($islam); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($kristen); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($katholik); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($hindu); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($budha); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($konghuchu); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($lainnya); ?></td>
							<td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php 
elseif($agregatGoldrh):
?>
	<div class="box-body">
		<div class="table-responsive">	
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregatGoldrh" class="table table-bordered table-striped">
						<?php if(!$agregatGoldrh): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">NAMA KECAMATAN</th>
				            <th class="chart value">A</th>
							<th class="chart value">B</th>
							<th class="chart value">AB</th>
							<th class="chart value">O</th>
							<th class="chart value">A+</th>
							<th class="chart value">A-</th>
							<th class="chart value">B+</th>
							<th class="chart value">B-</th>
							<th class="chart value">AB+</th>
							<th class="chart value">AB-</th>
							<th class="chart value">O+</th>
							<th class="chart value">O-</th>
							<th class="chart value">TDK TAHU</th>
							<th class="chart total">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatGoldrh as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
		                    <td><?php echo $no++; ?></td>
		                    <td class="chart title"><?php echo "$nama_kec"; ?></td>
		                    <td class="chart value" style='text-align:right'><?php echo number_format($a); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($b); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($ab); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($o); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($a_plus); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($a_min); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($b_plus); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($b_min); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($ab_plus); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($ab_min); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($o_plus); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($o_min); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($tdk_tahu); ?></td>
							<td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>	
    </div>
<?php 
elseif($agregatPddkan):
?>
	<div class="box-body">
		<div class="table-responsive">	
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregatPddkan" class="table table-bordered table-striped">
						<?php if(!$agregatPddkan): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">NAMA KECAMATAN</th>
				            <th class="chart value">TDK SKLH</th>
							<th class="chart value">BLM TMT SD</th>
							<th class="chart value">TMT SD</th>
							<th class="chart value">SLTP</th>
							<th class="chart value">SLTA</th>
							<th class="chart value">D I</th>
							<th class="chart value">D III</th>
							<th class="chart value">S I</th>
							<th class="chart value">S II</th>
							<th class="chart value">S III</th>
							<th class="chart total">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatPddkan as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
		                    <td><?php echo $no++; ?></td>
		                    <td class="chart title"><?php echo "$nama_kec"; ?></td>
		                    <td class="chart value" style='text-align:right'><?php echo number_format($tdk_sklh); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($blm_tmt_sd); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($tmt_sd); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($sltp); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($slta); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($d_i); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($d_iii); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($s_i); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($s_ii); ?></td>
							<td class="chart value" style='text-align:right'><?php echo number_format($s_iii); ?></td>
							<td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php 
elseif($agregatSHDK):
?>
	<div class="box-body">
		<div class="table-responsive">
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregatSHDK" class="table table-bordered table-striped">
						<?php if(!$agregatSHDK): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">NAMA KECAMATAN</th>
				            <th class='chart value'>KEPALA_KELUARGA</th>
							<th class='chart value'>SUAMI</th>
							<th class='chart value'>ISTRI</th>
							<th class='chart value'>ANAK</th>
							<th class='chart value'>MENANTU</th>
							<th class='chart value'>CUCU</th>
							<th class='chart value'>ORANG_TUA</th>
							<th class='chart value'>MERTUA</th>
							<th class='chart value'>FAMILI_LAIN</th>
							<th class='chart value'>PEMBANTU</th>
							<th class='chart value'>LAINNYA</th>
							<th class="chart total">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatSHDK as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
		                    <td><?php echo $no++; ?></td>
		                    <td class="chart title"><?php echo "$nama_kec"; ?></td>
		                    <td class='chart value' style='text-align:right'><?php echo number_format($kepala_keluarga); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($suami); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($istri); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($anak); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($menantu); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($cucu); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($orang_tua); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($mertua); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($famili_lain); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($pembantu); ?></td>
							<td class='chart value' style='text-align:right'><?php echo number_format($lainnya); ?></td>
							<td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php 
elseif($agregatUmur):
?>
	<div class="box-body">
		<div class="table-responsive">	
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregat" class="table table-bordered table-striped">
						<?php if(!$agregatUmur): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">STRUKTUR UMUR</th>
				            <th class="chart value" width="15%">LAKI-LAKI</th>
				            <th class="chart value" width="15%">PEREMPUAN</th>
				            <th class="chart total" width="15%">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatUmur as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
			                  <td><?php echo $no++; ?></td>
			                  <td class="chart title"><?php echo "$struktur_umur"; ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($lk); ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($pr); ?></td>
			                  <td class="chart total" style="text-align:right"><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php 
elseif($agregatPkrjn):
?>
	<div class="box-body">
		<div class="table-responsive">	
			<div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-12">
					</br>
						<table id="agregat" class="table table-bordered table-striped">
						<?php if(!$agregatPkrjn): ?>
			        		<tr>
			        			<td colspan="5">
				        		<?php echo "TIDAK ADA DATA !!"; ?>
				        		</td>
			        		</tr>
				        <?php else: ?>
				        <thead>
				          <tr class="text-center">
				            <th width="5%">NO</th> 
				            <th class="chart title">JENIS PEKERJAAN</th>
				            <th class="chart value" width="15%">LAKI-LAKI</th>
				            <th class="chart value" width="15%">PEREMPUAN</th>
				            <th class="chart total" width="15%">JUMLAH</th>
				          </tr>
				      	</thead>
				      	
				        <tbody>
			            <?php		
				            $no=1;
				            foreach ($agregatPkrjn as $row)
				            {
				              extract((array) $row);
				              ?>
				          <tr>                      
			                  <td><?php echo $no++; ?></td>
			                  <td class="chart title"><?php echo "$pekerjaan"; ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($lk); ?></td>
			                  <td class="chart value" style="text-align:right"><?php echo number_format($pr); ?></td>
			                  <td class="chart total" style="text-align:right"><?php echo number_format($jumlah); ?></td>
				          </tr>
				              <?php
				            }
				            ?>
				        <?php endif; ?>
				                                  
				        </tbody>
				        <tfoot>
							<tr>
								<th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart value" style="text-align:right"></th>
								<th class="chart total" style="text-align:right"></th>
							</tr>
						</tfoot>
				        </table>
	    			</div>
				</div>
			</div>
		</div>
    </div>
<?php endif; ?>


