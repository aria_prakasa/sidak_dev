<script type="text/javascript">

  google.load("visualization", "1.1", {packages:["bar"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {

  	// var arr = [
   //    ['Year', 'Sales', 'Expenses', 'Profit'],
   //    ['2014', 1000, 400, 200],
   //    ['2015', 1170, 460, 250],
   //    ['2016', 660, 1120, 300],
   //    ['2017', 1030, 540, 350]
   //  ];

  	var id = ".table";

    // hasilnya seperti di atas
    var tmp_arr = [];
    var tmp_value_arr = [];
    var i = 0;
    $(id+' thead tr th.chart').each(function(index, el) {
    	if( ! $(this).hasClass('total'))
    	{
	    	tmp_arr.push($(this).text());
	    }
    });
    tmp_value_arr[i] = tmp_arr;

    // console.log(tmp_value_arr);

    $(id+' tbody tr').each(function(index, el) {
    	tmp_arr = [];
    	i++;
    	$(this).find('td.chart').each(function(index, el) {
	    	if( ! $(this).hasClass('total'))
	    	{
			    if($(this).hasClass('value'))
		    	{
			    	tmp_arr.push(parseInt($(this).text().replace(",","")));
			    }
			    else {
			    	tmp_arr.push($(this).text());
			    }
		    }
	    });
   		tmp_value_arr[i] = tmp_arr;
    });

    console.log(tmp_value_arr);

    var data = google.visualization.arrayToDataTable(tmp_value_arr);

    var options = {
      chart: {
        title: '',
        subtitle: '',
      },
      bars: 'vertical' // Required for Material Bar Charts.
    };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, options);
  }
</script>

<div id="columnchart_material" style="width: 100%; height: 500px;"></div>

