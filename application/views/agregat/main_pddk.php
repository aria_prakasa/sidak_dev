<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="box">
  <div class="box-header with-border">
      <h3 class="box-title"><strong>Statistik Penduduk Kota Pasuruan</strong></h3>
  </div>
  <div class="box-body">
        
  	<?php $this->load->view('tpl_filter'); ?>

	<?php 
	if($this->input->post()){ ?>
	<div>

	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li class="<?php echo $pane_nav; ?> <?php echo $kec_active; ?>" role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
	    <li role="presentation" class="<?php echo $kel_active; ?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Bar</a></li>
	    <!-- <li class="hide" role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Pie</a></li> -->
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content" style="margin-top:10px;">
	    <div role="tabpanel" class="tab-pane <?php echo $kec_active; ?>" id="table">
	    	<?php 
			if($this->input->post('kecamatan_id'))
			{
				if($this->input->post('kelurahan_id'))
				{
					$this->load->view('agregat/tpl_agregat_kelurahan'); 
				}
				else 
				{
					$this->load->view('agregat/tpl_agregat_kecamatan');
				}
			}
			else 
			{
				$this->load->view('agregat/tpl_agregat_kota'); 
			}			
			
		    ?> 
		</div>
	    <div role="tabpanel" class="tab-pane <?php echo $kel_active; ?>" id="home">
	    	<?php
	  
			$this->load->view('agregat/tpl_stat_pddk_bar');   
		        
		    ?> 
		</div>
	    
	  </div>
	<?php 
	}
		?>
	</div>
	
</div><!-- /.box -->
<script>

$(document).ready(function() {
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  // e.target // newly activated tab
	  // e.relatedTarget // previous active tab
	  // console.log('clicked');
	  drawChart();
	  // drawChartPie();
	});
});
</script>