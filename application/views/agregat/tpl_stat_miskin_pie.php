    <script type="text/javascript">

      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        // var arr = [
        //   ['Task', 'Hours per Day'],
        //   ['Work',     11],
        //   ['Eat',      2],
        //   ['Commute',  2],
        //   ['Watch TV', 2],
        //   ['Sleep',    7]
        // ];

        var id = ".table";

        // hasilnya seperti di atas
        var tmp_arr = [];
        var tmp_value_arr = [];
        var i = 0;
        $(id+' thead tr th.chart').each(function(index, el) {
          if( ! $(this).hasClass('value'))
          {
            tmp_arr.push($(this).text());
          }
        });
        tmp_value_arr[i] = tmp_arr;

        // console.log(tmp_value_arr);

        $(id+' tbody tr').each(function(index, el) {
          tmp_arr = [];
          i++;
          $(this).find('td.chart').each(function(index, el) {
            if( ! $(this).hasClass('value'))
            {
              if($(this).hasClass('total'))
              {
                tmp_arr.push(parseInt($(this).text()));
              }
              else {
                tmp_arr.push($(this).text());
              }
            }
          });
          tmp_value_arr[i] = tmp_arr;
        });

        // console.log(tmp_value_arr);


        var data = google.visualization.arrayToDataTable(tmp_value_arr);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

    </script>

    <!--Div that will hold the pie chart-->
    <div id="piechart" style="width: 100%; height: 500px;"></div>
