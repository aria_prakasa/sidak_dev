<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Pencarian Data Penduduk :</strong></h3>
      <!-- <div class="box-tools pull-right">
      <?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
      <?php /* ?>
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
      <?php //*/?>
      </div> -->
  </div>

  <div class="box-body">
    <?php $this->load->view('tpl_filter'); ?>


    <?php 
    if( ! $jml_data): ?>
      &nbsp; 
    <?php else: ?> 
    <div class="box-body" style="padding-left: 0px; padding-right: 0px;">
      <div class="box-header with-border bg-success">
        <h3 class="box-title"><b>Result :</b></h3>
      </div>
      </br>
      <div class="dataTables_wrapper form-inline dt-bootstrap" id="example1_wrapper">
        <div class="row">
          <div class="col-sm-12">
            <div class="box-body table-responsive no-padding">
              <table id="example1" class="table table-bordered table-striped table-responsive">     
                <thead>
                  <tr>
                    <th>NO</th> 
                    <th>NO KK</th>
                    <th>NIK</th>
                    <th>NAMA LENGKAP</th>
                    <th>ALAMAT</th>
                    <th>KELURAHAN</th>
                    <th>KECAMATAN</th>
                    <th>OPERASI</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  // var_dump($data_kk);die;
                  //for ($i=1; $i <= 6; $i++) { 
                  # code...
                  $no=1;
                  foreach ($data_kk as $row)
                  {
                  extract((array) $row);
                  ?>
                  <tr>                      
                    <td><?php echo $no++; ?></td>
                    <td><?php echo "$no_kk"; ?></td>
                    <td><?php echo "$nik"; ?></td>
                    <td><?php echo "$nama_lgkp"; ?></td>
                    <td><?php echo "$alamat; NO RT. $no_rt/NO RW. $no_rw"; ?></td>
                    <td><?php echo "$nama_kel"; ?></td>
                    <td><?php echo "$nama_kec"; ?></td>
                    <td align="center">
                      <a data-original-title="Lihat" href="<?php echo base_url();?>index.php/entry/lihat/<?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
                    </td>
                  </tr>
                  <?php
                  }
                  //}
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.row -->
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box-body -->
  <?php endif; ?>
</div><!-- /.box -->
