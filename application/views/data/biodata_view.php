<div class="box">
  <div class="box-body" style="padding-right: 15px; padding-top: 15px; padding-left: 15px;">
    <div class="box-header with-border bg-success">
          <h3 class="box-title"><b>Biodata Penduduk</b></h3>
    </div>
    </br>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
          	<label class="col-sm-4 control-label">No KK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_kk"; ?> ">
            </div>
          </div>
          <div class="form-group">
          	<label class="col-sm-4 control-label">NIK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nik"; ?> ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Nama Lengkap</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_lgkp"; ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Jenis Kelamin</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$jenis_klmin"; ?> " style="text-transform:uppercase">
              </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Agama</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$agama"; ?> " style="text-transform:uppercase">
              </div>
          </div>

        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Tempat Lahir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$tmpt_lhr"; ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Tanggal Lahir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$tgl_lhr"; ?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Pendidikan Akhir</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$pddk_akh"; ?> " style="text-transform:uppercase">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Jenis Pekerjaan</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$jenis_pkrjn"; ?> " style="text-transform:uppercase">
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
      <div class="box-header bg-danger">
          <h3 class="box-title"><b>Data Kemiskinan</b></h3>
      </div><!-- /.box-header -->
      </br>
      <form id="ctt_miskin" method="post" class="form-horizontal" action="<?php echo base_url(); ?>index.php/entry/simpan_biodata/<?php echo $no_kk; ?>/<?php echo $nik; ?>">
      <div class="row">
		  <div class="col-sm-6">
		    <div class="form-horizontal">
		      <div class="form-group">
		        <label class="col-sm-4 control-label">field 1</label>
		          <div class="col-sm-8">
		            <input type="text" name="field1" id="field1" class="form-control" value="<?php echo "$field1"; ?> " style="text-transform:uppercase">
		          </div>
		      </div>
		      <div class="form-group">
		        <label class="col-sm-4 control-label">field 2</label>
		          <div class="col-sm-8">
		            <input type="text" name="field2" id="field2" class="form-control" value="<?php echo "$field2"; ?> " style="text-transform:uppercase">
		          </div>
		      </div>
		      <div class="form-group">
		        <label class="col-sm-4 control-label">field 3</label>
		          <div class="col-sm-8">
		            <input type="text" name="field3" id="field3" class="form-control" value="<?php echo "$field3"; ?> " style="text-transform:uppercase">
		          </div>
		      </div>
		      <div class="form-group">
		        <label class="col-sm-4 control-label">field 4</label>
		          <div class="col-sm-8">
		            <input type="text" name="field4" id="field4" class="form-control" value="<?php echo "$field4"; ?> " style="text-transform:uppercase">
		          </div>
		      </div>
		      <div class="form-group">
		      	<div class="col-sm-4"></div>
          	<div class="col-sm-8">
              <a href="<?php echo base_url(); ?>index.php/<?php echo $controller; ?>/lihat/<?php echo $no_kk; ?>" class="btn bg-navy margin" style="margin-left: 0px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
      			  <button type="submit" class="btn bg-olive margin" style="margin-left: 0px;">Simpan&nbsp;&nbsp;<i class="fa fa-save"></i></button>
          	</div>
		      </div>
		    </div>
		  </div>
      </div>
  	  </form>


      <?php
/*if ($nik == TRUE)
{
echo '<div class="row">';
echo '<div class="col-sm-8"></div>';
echo '<div class="col-sm-2">';
echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing_biodata/'?><?php echo '" ><button class="btn bg-navy margin"><i class="fa fa-arrow-left">&nbsp;&nbsp; Kembali</i></button></a></div>';
echo '<div class="col-sm-2">';
echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing_biodata/simpan/'?><?php echo $row->nik; ?><?php echo '" ><button class="btn bg-olive margin">Simpan &nbsp;&nbsp;<i class="fa fa-save"></i></button></a></div>';
}*/
?>
    </div>
	</div><!-- /.box-body -->
</div><!-- /.box -->