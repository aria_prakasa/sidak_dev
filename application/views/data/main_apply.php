<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Daftar Permohonan Data Keluarga Miskin (Belum Verifikasi)</strong></h3>
    <div class="box-tools pull-right">
      <?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
      <?php /* ?>
<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
<?php //*/?>
    </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table id="example1" class="table table-bordered table-striped">
      <?php if (!$data_mohon): ?>
        <tr>
          <td colspan="9">
          <?php echo "TIDAK ADA DATA !!"; ?>
          </td>
        </tr>
      <?php else: ?>
        <thead>
          <tr>
            <th>NO</th>
            <th>NO KK</th>
            <th width="20%">NAMA KEP KELUARGA</th>
            <th>ALAMAT</th>
            <th>RT</th>
            <th>RW</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th>OPERASI</th>
          </tr>
        </thead>
        <tbody>
        <?php
//for ($i=1; $i <= 6; $i++) {
# code...
$no = 1;
foreach ($data_mohon as $row) {
	$row = keysToLower($row);
	extract((array) $row);
	?>
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo "$no_kk"; ?></td>
            <td><?php echo "$nama_kep"; ?></td>
            <td><?php echo "$alamat"; ?></td>
            <td><?php echo "$no_rt"; ?></td>
            <td><?php echo "$no_rw"; ?></td>
            <td><?php echo "$nama_kel"; ?></td>
            <td><?php echo "$nama_kec"; ?></td>
            <td align="center">
              <a data-original-title="Lihat" href="<?php echo base_url(); ?><?php echo 'index.php/apply/lihat/' ?><?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
            <!-- <?php
if ($this->cu->USER_LEVEL != 4) {?>
              <a data-original-title="Hapus" onclick="return confirm('Yakin akan menghapus data ini ?')" href="<?php echo base_url(); ?><?php echo 'index.php/apply/hapus/' ?><?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-times"></i></a>
            <?php }?> -->
            </td>
          </tr>
        <?php
}
//}
?>
        </tbody>
      <?php endif;?>

      </table>
    </div>
  </div>
</div><!-- /.box -->