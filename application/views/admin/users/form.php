<?php
$field_arr = array('nip', 'nama_lengkap', 'username', 'password', 'user_level', 'no_kec', 'no_kel');
foreach ($field_arr as $field) {
    $$field = "";
}
if (isset($row) && is_object($row)):
    extract(get_object_vars($row));
    if ($no_kel != "") {
        $no_kel = NO_PROP . "-" . NO_KAB . "-" . $no_kec . "-" . $no_kel;
    }

    if ($no_kec != "") {
        $no_kec = NO_PROP . "-" . NO_KAB . "-" . $no_kec;
    }

endif;
if ($this->input->post()) {
    extract($this->input->post());
}

?>
<?php
$add_class = 'class="form-control"';
?>
<?php echo print_status(); ?>
<?php echo (validation_errors() ? print_error(validation_errors()) : ''); ?>
<?php echo form_open('', 'role="form" class="form-horizontal"'); ?>
<div class="form-group">
  <label for="nip" class="col-sm-2 control-label">N I P</label>
  <div class="col-sm-10">
    <?php echo form_input('nip', $nip, $add_class . ' placeholder="N I P" required autofocus'); ?>
  </div>
</div>
<div class="form-group">
  <label for="nama_lengkap" class="col-sm-2 control-label">Nama</label>
  <div class="col-sm-10">
    <?php echo form_input('nama_lengkap', $nama_lengkap, $add_class . ' placeholder="Nama Lengkap"'); ?>
  </div>
</div>
<div class="form-group">
  <label for="username" class="col-sm-2 control-label">Username</label>
  <div class="col-sm-10">
    <?php echo form_input('username', $username, $add_class . ' placeholder="Username to login" required'); ?>
  </div>
</div>
<div class="form-group">
  <label for="password" class="col-sm-2 control-label">Password</label>
  <div class="col-sm-10">
    <?php echo form_password('password', $password = "", $add_class . ' placeholder="Password to login" ' . ($this->uri->segment(3) == "edit" ? "" : "required")); ?>
  </div>
</div>
<div class="form-group">
  <label for="user_level" class="col-sm-2 control-label">Level</label>
  <div class="col-sm-10">
  <?php 
  // $this->db->order_by("NO ASC");
  // $res = $this->db->get("MASTER_USER");
  // var_dump($res->result_array()); die;
  echo $this->master_user->drop_down_select('user_level', $user_level, 'class="form-control select2"');
  ?>
  </div>
</div>
<?php
$target_uri = $this->uri->segment(3);
// var_dump($target_uri); die;
// if(($target_uri == "edit" && !empty($no_kec)) || $target_uri == "add"):
  ?>
<div id="kecamatan-wrap" class="form-group">
  <label for="no_kec" class="col-sm-2 control-label">Kecamatan</label>
  <div class="col-sm-4">
    <?php
  // $arr = array("NO_PROP" => $PROP_ID, "NO_KAB" => $KAB_ID, "NO_KEC" => intval($no_kec));
  // $this->db->get_where("SETUP_KEC", $arr);
echo $this->setup_kec->drop_down_select_by_kab(NO_PROP, NO_KAB, 'no_kec', $no_kec, 'class="form-control select2" required'.(($target_uri == "edit" && !empty($no_kec)) ? "" : " disabled"), "-- Pilih Kecamatan --");
?>
  </div>
</div>
<?php /*endif; ?>
<?php if(($target_uri == "edit" && !empty($no_kec) && !empty($no_kel)) || $target_uri == "add"):*/ ?>
<div id="kelurahan-wrap" class="form-group">
  <label for="no_kel" class="col-sm-2 control-label">Kelurahan</label>
  <div class="col-sm-4">
    <div id="kel_wrap" data-id="<?php echo isset($no_kel) ? $no_kel : ""; ?>" data-disabled="0">
    <?php
if (!isset($no_kec)) {
    echo '<div class="help-block">Pilih Kecamatan terlebih dahulu.</div>';
} else {
    echo $this->setup_kel->drop_down_select_by_kec(NO_PROP, NO_KAB, (isset($no_kec) ? '' : ''), 'no_kel', $no_kel, 'class="form-control" required'.(($target_uri == "edit" && !empty($no_kel)) ? "" : " disabled"), "-- Pilih Kelurahan --");
}

?>
    </div>
  </div>
</div>
<?php //endif; ?>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
    <?php echo anchor($this->curpage, 'Cancel', 'class="btn btn-link"'); ?>
  </div>
</div>

<?php echo form_close(); ?>

<script>

  $(document).ready(function() {
    $(document).on('change', 'form select[name=user_level]', function() {
      update_kec_ddl();
    });
    $(document).on('change', 'form select[name=no_kec]', function() {
      update_kel_ddl('no_kel');
    });
    // update_kec_ddl();
    update_kel_ddl('no_kel');
  });

function update_kec_ddl()
{
  var user_level = $('form select[name=user_level]').val();
  var target = $('form select[name=no_kec]');
  if(user_level == 0 || user_level == 1 || user_level == 4)
  {
    target.val('');
    update_kel_ddl('no_kel');
    target.attr({'disabled': 'disabled'});
  }
  else {
    target.val('');
    update_kel_ddl('no_kel');
    target.removeAttr('disabled');
  }
}

function update_kel_ddl(name)
{
  var user_level = $('form select[name=user_level]').val();
  var no_kec = $('form select[name=no_kec]').val();
  var no_kel = $('#kel_wrap').data("id");
  var disabled = $('#kel_wrap').data("disabled");
  if(no_kel === "") no_kel = $('form select[name=no_kel]').val();
  var params = {'name': name, 'no_kec': no_kec, 'no_kel': no_kel, 'disabled': disabled};
  var urlx = site_url+"ajax/get_kel_ddl";
  var target = $('#kel_wrap');
  if(user_level != 0 && user_level != 1 && user_level != 2 && user_level != 4)
  {
    $.ajax({
      'type': 'POST',
      'async': true,
      url: urlx,
      data: params,
      complete: function(xhr)
      {
        var data = xhr.responseText;
        if(data !== "")
        {
          target.html(data);
          SELECT2.init();
        }
      }
     });
  }
}
</script>