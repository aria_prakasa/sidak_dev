<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDaK Online | Dashboard</title>
    <link rel="shortcut icon" href="<?php echo base_url('_assets/img/pemkot.png') ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/bootstrap/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('_assets'); ?>/plugins/iCheck/square/blue.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('_assets'); ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery Validation -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url('_assets'); ?>/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="<?php echo base_url('_assets'); ?>/js/jquery.validate.init.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url('_assets'); ?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo base_url('_assets'); ?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('_assets'); ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('_assets'); ?>/dist/js/app.min.js"></script>
    <!-- MyApp -->
    <script src="<?php echo base_url('_assets'); ?>/js/function.js?<?php echo date('ymdHi'); ?>"></script>
    <script src="<?php echo base_url('_assets'); ?>/js/main.js?<?php echo date('ymdHi'); ?>"></script>

    <script>
    var site_url = '<?php echo site_url(); ?>';
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"], input[type="radio"]').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
    </script>

  </head>
  <body class="hold-transition skin-green sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>index.php/home" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?=base_url('_assets/img/pemkot.png')?>" alt="" height="40"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=base_url('_assets/img/pemkot.png')?>" alt="" height="40">&nbsp;<b>SIDaK</b><i><sub style="bottom:-0.7em;left:-24px;">online</sub></i></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs">Bappeda Kota Pasuruan</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>_assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $this->ca->USERNAME; ?>
                      <?php /* ?>
<small>Member since <?php echo format_tanggal($this->cu->DATE_CREATED); ?></small>
<?php //*/?>
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo site_url(ADM_VIEW . 'setting'); ?>" class="btn btn-default btn-flat"><i class="fa fa-cog"></i> Pengaturan</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url(ADM_VIEW . 'home/logout'); ?>" class="btn btn-default btn-flat" onclick="return confirm('Yakin keluar?')"><i class="fa fa-power-off"></i> Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->

            </ul>
          </div>

        </nav>
      </header>