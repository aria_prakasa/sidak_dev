<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>_assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->cu->NAMA_LENGKAP; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> <?php echo user_desc($this->cu->USER_LEVEL); ?></a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="<?php echo $home_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/home">
                <i class="glyphicon glyphicon-home"></i> <span> Halaman Utama</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $dash_nav; ?>"><a href="<?php echo base_url(); ?>index.php/home"><i class="fa fa-circle-o"></i> Dashboard </a></li>
                <li class="<?php echo $about_nav; ?>"><a href="<?php echo base_url(); ?>index.php/about"><i class="fa fa-circle-o"></i> Tentang Kami </a></li>
              </ul>
            </li>
            <li class="<?php echo $data_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-edit"></i> <span> Pemasukan Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php
if ($this->cu->USER_LEVEL != 4) {?>
                <li class="<?php echo $entri_nav; ?>"><a href="<?php echo base_url(); ?>index.php/entry"><i class="fa fa-circle-o"></i> Entri Data Permohonan</a></li>
                <?php }?>
                <li class="<?php echo $ltmohon_nav; ?>"><a href="<?php echo base_url(); ?>index.php/apply"><i class="fa fa-circle-o"></i> List Data Permohonan</a></li>
              </ul>
            </li>
            <li class="<?php echo $plus_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-plus"></i> <span> Entri Kemiskinan Plus</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $kis_nav; ?> hide"><a href="<?php echo base_url(); ?>index.php/kis"><i class="fa fa-circle-o"></i> KIS (Kartu Indonesia Sehat)</a></li>
                <li class="<?php echo $bpjs_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs"><i class="fa fa-circle-o"></i> BPJS</a></li>
                <li class="<?php echo $raskin_nav; ?>"><a href="<?php echo base_url(); ?>index.php/raskin"><i class="fa fa-circle-o"></i> Raskin</a></li>
                <li class="<?php echo $pkh_nav; ?>"><a href="<?php echo base_url(); ?>index.php/pkh"><i class="fa fa-circle-o"></i> PKH/Rumah Tdk Layak Huni</a></li>
              </ul>
            </li>
            <?php
if ($this->cu->USER_LEVEL == 0) {?>
            <li class="<?php echo $verify_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-download-alt"></i> <span> Verifikasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $apply_nav; ?>"><a href="<?php echo base_url(); ?>index.php/verify_apply"><i class="fa fa-circle-o"></i> Permohonan Data</a></li>
                <li class="<?php echo $change_nav; ?>"><a href="<?php echo base_url(); ?>index.php/verify_changed"><i class="fa fa-circle-o"></i> Data Anomali</a></li>
              </ul>
            </li>
            <?php }?>
            <li class="<?php echo $daftar_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span> Data Kemiskinan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $search_nav; ?> hide"><a href="<?php echo base_url(); ?>index.php/search"><i class="fa fa-circle-o"></i> Pencarian Data Kemiskinan</a></li>
                <li class="<?php echo $list_nav; ?>"><a href="<?php echo base_url(); ?>index.php/listing"><i class="fa fa-circle-o"></i> Daftar Keluarga Miskin</a></li>
                <li class="<?php echo $bio_nav; ?>"><a href="<?php echo base_url(); ?>index.php/listing_biodata"><i class="fa fa-circle-o"></i> Daftar Biodata Miskin</a></li>
                <li class="<?php echo $list_bpjs_nav; ?>"><a href="<?php echo base_url(); ?>index.php/listing_bpjs"><i class="fa fa-circle-o"></i> Daftar Peserta BPJS</a></li>
              </ul>
            </li>
            <li class="<?php echo $agr_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-stats"></i><span> Agregat dan Statistik</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $agr_miskin_nav; ?>"><a href="<?php echo base_url(); ?>index.php/agr_miskin"><i class="fa fa-circle-o"></i> Penduduk Miskin</a></li>
                <li class="<?php echo $agr_pend_nav; ?>"><a href="<?php echo base_url(); ?>index.php/agr_penduduk"><i class="fa fa-circle-o"></i> Kota Pasuruan</a></li>
              </ul>
            </li>
            <li class="hide <?php echo $stat_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-stats"></i><span> Statistik</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $stat_miskin_nav; ?>"><a href="<?php echo base_url(); ?>index.php/stat_miskin"><i class="fa fa-circle-o"></i> Grafik Data Kemiskinan</a></li>
                <li class="<?php echo $stat_pend_nav; ?>"><a href="<?php echo base_url(); ?>index.php/stat_penduduk"><i class="fa fa-circle-o"></i> Grafik Data Penduduk</a></li>
              </ul>
            </li>


          </ul>
      </section>
        <!-- /.sidebar -->
</aside>