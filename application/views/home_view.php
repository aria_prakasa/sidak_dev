<?php $this->load->view('header_view');?>
      <!-- Left side column. contains the logo and sidebar -->

      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->

        <?php $this->load->view('menu_view');?>

        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php if (isset($judul) || isset($sub_judul)): ?>
        <section class="content-header">
          <?php if (isset($judul)): ?>
          <h1>
            <?php echo $judul; ?>
            <?php if (isset($sub_judul)): ?>
            <small><?php echo $sub_judul; ?></small>
            <?php endif;?>
          </h1>
          <?php endif;?>
          <?php if (isset($judul)): ?>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $judul; ?></li>
          </ol>
          <?php endif;?>
        </section>
        <?php endif;?>

        <!-- Main content -->
        <section class="content">
        <?php $this->load->view($content);?>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

<?php $this->load->view('footer_view');?>