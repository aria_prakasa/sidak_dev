<div class="box">
  <div class="box-body" style="padding-right: 15px; padding-top: 15px; padding-left: 15px;">
    <div class="box-header with-border bg-success">
      <h3 class="box-title"><b>Data Keluarga</b></h3>
    </div>    
    </br>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Kepala Keluarga</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kep";?> ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Alamat</label>
            <div class="col-sm-8">
              <textarea class="form-control" disabled="" rows="3"><?php echo "$alamat";?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">&nbsp;</label>
            <label class="col-sm-2 control-label">No RT</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rt";?>">
            </div>
            <label class="col-sm-2 control-label">No RW</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rw";?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Propinsi</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_prop";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kota / Kab</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kab";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kecamatan</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kec";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kelurahan</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kel";?>">
            </div>
          </div>
        </div>
      </div>    
    </div>  
    <?php echo form_open('verify_apply/simpan'); ?>
    <?php echo form_hidden('no_kk', $no_kk); ?>
    <div classs="row">   
      <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="box-header bg-danger">
          <h3 class="box-title"><b>Data Anggota Keluarga</b></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NO</th> 
                <th>NIK</th>
                <th>NAMA LENGKAP</th>
                <th>KELAMIN</th>
                <th>TGL LAHIR</th>
                <th>HUB. KELUARGA</th>
                <th>AGAMA</th>
                <th>PENDIDIKAN</th>
                <th>PEKERJAAN SIAK</th>
                <th>PEKERJAAN SIDAK</th>
                <th>STATUS</th>
                <th>CHECK</th>
              </tr>
            </thead>
            <tbody>
            <?php 
              $no=1;
              foreach ($anggota_mohon as $row)
              {
              extract((array) $row);
            ?>
              <tr>                      
                <td><?php echo $no++; ?></td>
                <td><?php echo "$nik"; ?></td>
                <td><?php echo "$nama_lgkp"; ?></td>
                <td><?php echo "$jenis_klmin"; ?></td>
                <td><?php echo format_tanggal($tgl_lhr); ?></td>
                <td><?php echo "$stat_hbkel"; ?></td>
                <td><?php echo "$agama"; ?></td>
                <td><?php echo "$pddk_akh"; ?></td>
                <td><?php echo "$jenis_pkrjn_siak"; ?></td>
                <td><?php
                    if ($status == 3){      
                      echo "$jenis_pkrjn";                
                    } else {
                      $style_pekerjaan='class="form-control input-sm" id="jenis_pkrjn"';
                      // var_dump($id_jenis_pkrjn);
                      echo $this->master_pekerjaan->drop_down_select('jenis_pkrjn['.$nik.']', $id_jenis_pkrjn, $optional = $style_pekerjaan, $default = " --- ", $readonly = "");
                      //form_dropdown("jenis_pkrjn",$pekerjaan,$jenis_pkrjn,$jenis_pkrjn);
                    }
                    ?>
                </td>
                <!-- <td align="center">
                <a data-original-title="Lihat" href="<?php echo base_url();?><?php echo 'index.php/verify_apply/lihat_biodata/'?><?php echo $row->no_kk; ?><?php echo '/'?><?php echo $row->nik; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
                </td>   --> 
                <?php 
                if ($status == 2) 
                {
                  echo '<td align="left">';
                  echo '<span class="label label-success">verifikasi ok</span></td>';
                  //echo '<td align="left">';                        
                }
                else if ($status == 3)
                {
                  echo '<td align="left">';
                  echo '<span class="label label-danger">status berubah</span></td>';
                  $status_change = 1;
                  //echo '<td align="left">';                        
                }
                else if ($status == 1)
                {
                  echo '<td align="left">';
                  echo '<span class="label label-warning">dalam proses</span>'; ?>

                <td align="left">
                  <label class="control-label">
                    <input id="check" type="checkbox" name="check[<?php echo $row->nik; ?>]" />
                  </label>
                </td>
                <?php
                  /*echo '<td align="left">';
                  echo '<a data-original-title="Edit" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;';
                  echo '<a data-original-title="Batal" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ban"></i></a></td>';  */
                }
                else
                {
                  echo '<td align="left">';
                  echo '<span class="label label-primary">no status</span>';
                  /*echo '<td align="left">';
                  echo '<a data-original-title="Proses" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-share"></i></a></td>';   */
                }?>   
              </tr>
              <?php
              }
              ?>

            </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="row">
          <div class="col-md-2">
            <a class="btn bg-navy margin" href="<?php echo base_url();?>index.php/verify_apply " > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
          </div>
          <div class="col-md-8"></div>
          <!-- <div class="col-sm-10">
          <a href="<?php echo base_url();?>index.php/verify_apply" ><button class="btn bg-navy margin" style="margin-left: 0px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</button></a></div> -->
          <div class="col-sm-2">
          <?php 
          if ($status_change == 1) 
          { 
          ?> 
            <a class="btn bg-red margin" onclick="return confirm(\'Yakin akan menghapus data ini ?\')" href="<?php echo base_url();?>index.php/apply/hapus/<?php echo $no_kk; ?>">Batal Proses &nbsp;&nbsp;<i class="fa fa-times"></i></a>
          <?php 
          } else
          {
          ?>
            <button type="submit" class="btn bg-olive margin">Validasi Data &nbsp;&nbsp;<i class="fa fa-save"></i></button>
          <?php 
          }
          ?> 
            <!-- <a href="<?php echo base_url();?>index.php/verify_apply/simpan/<?php echo $row->no_kk; ?>"><button class="btn bg-olive margin">Validasi&nbsp;&nbsp;<i class="fa fa-save"></i></button></a> -->
          </div>
        </div>
      </div>
    </div><!-- /.row -->

    <?php echo form_close(); ?>  
  </div>
</div><!-- /.box -->