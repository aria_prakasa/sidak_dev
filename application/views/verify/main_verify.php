<div class="box">
	<div class="box-header with-border">
  		<h3 class="box-title"><strong>Daftar Permohonan Data Keluarga Miskin</strong></h3>
  		<div class="box-tools pull-right">
  			<?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
  			<?php /* ?>
<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
<?php //*/?>
		</div>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
	        	<?php if (!$data_mohon): ?>
		        		<tr>
		        			<td colspan="9">
			        		<?php echo "TIDAK ADA DATA !!"; ?>
			        		</td>
		        		</tr>
			        <?php else: ?>
			  <thead>
	          <tr>
	            <th>NO</th>
	            <th>NO KK</th>
	            <th width="20%">NAMA KEP KELUARGA</th>
	            <th>ALAMAT</th>
	            <th>RT</th>
	            <th>RW</th>
	            <th>KELURAHAN</th>
	            <th>KECAMATAN</th>
	            <th>OPERASI</th>
	          </tr>
	          </thead>
	          <tbody>
	            <?php
$no = 1;
foreach ($data_mohon as $row) {
    $row = keysToLower($row);
    extract((array) $row);
    ?>
	          <tr>
	                  <td><?php echo $no++; ?></td>
	                  <td><?php echo "$no_kk"; ?></td>
	                  <td><?php echo "$nama_kep"; ?></td>
	                  <td><?php echo "$alamat"; ?></td>
	                  <td><?php echo "$no_rt"; ?></td>
	                  <td><?php echo "$no_rw"; ?></td>
	                  <td><?php echo "$nama_kel"; ?></td>
	                  <td><?php echo "$nama_kec"; ?></td>
	                  <td align="center">
	                  	<a data-original-title="Verifikasi" href="<?php echo base_url(); ?>index.php/verify_apply/lihat/<?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-check-square-o"></i></a>
	                  </td>
	                  <?php /*
    if ($status == 1)
    {
    echo '<td align="center">';
    echo '<span class="label label-success">verified</span></td>';
    echo '<td align="center">';
    }
    else if ($status == 2)
    {
    echo '<td align="center">';
    echo '<span class="label label-danger">status changed</span></td>';
    echo '<td align="center">';
    }
    else if ($status == 3)
    {
    echo '<td align="center">';
    echo '<span class="label label-warning">on process</span>';
    echo '<td align="center">';
    echo '<a data-original-title="Edit" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;';
    echo '<a data-original-title="Batal" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ban"></i></a></td>';
    }
    else
    {
    echo '<td align="center">';
    echo '<span class="label label-primary">no status</span>';
    echo '<td align="center">';
    echo '<a data-original-title="Proses" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-share"></i></a></td>';
    } */?>
	          </tr>
	              <?php
}
?>
	         <?php endif;?>
	          </tbody>
    		</table>
		</div>
    </div>
</div><!-- /.box -->