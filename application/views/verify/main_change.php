<div class="box">
	<div class="box-header with-border">
  		<h3 class="box-title"><strong>Daftar Perubahan Data Penduduk Miskin</strong></h3>
  		<!-- <div class="box-tools pull-right">
  			<?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
  			<?php /* ?>
<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
<?php //*/?>
		</div> -->
	</div>

	<div class="box-body">
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
	        	<?php if (!$biodata_change): ?>
		        		<tr>
		        			<td colspan="9">
			        		<?php echo "TIDAK ADA DATA !!"; ?>
			        		</td>
		        		</tr>
			        <?php else: ?>
			  <thead>
	          <tr>
	            <th>NO</th>
	            <th>NO KK</th>
	            <th>NIK</th>
	            <th>NAMA LENGKAP</th>
	            <th>JENIS KELAMIN</th>
	            <th>JENIS PEKERJAAN</th>
	            <th>KELURAHAN</th>
	            <th>KECAMATAN</th>
	            <th>KETERANGAN</th>
	            <th>OPERASI</th>
	          </tr>
	          </thead>
	          <tbody>
	            <?php
$no = 1;
foreach ($biodata_change as $row) {
	$row = keysToLower($row);
	extract((array) $row);
	?>
	          <tr>
	                  <td><?php echo $no++; ?></td>
	                  <td><a href="<?php echo base_url(); ?><?php echo 'index.php/verify_changed/lihat/' ?><?php echo $row->no_kk; ?>"><?php echo "$no_kk"; ?></a></td>
	                  <td><?php echo "$nik"; ?></td>
	                  <td><?php echo "$nama_lgkp"; ?></td>
	                  <td><?php echo strtoupper($jenis_klmin); ?></td>
	                  <td><?php echo "$jenis_pkrjn_siak"; ?></td>
	                  <td><?php echo "$nama_kel"; ?></td>
	                  <td><?php echo "$nama_kec"; ?></td>
	                  <?php
if ($change_status == 1) {
		echo '<td align="left">';
		echo '<span class="label label-warning">Pisah / Pindah Keluarga </span></td>';
		//echo '<td align="left">';
	} else if ($change_status == 2) {
		echo '<td align="left">';
		echo '<span class="label label-danger">Status Penduduk tidak aktif</span></td>';
		//echo '<td align="left">';
	} else if ($change_status == 3) {
		echo '<td align="left">';
		echo '<span class="label label-info">Jenis Pekerjaan tidak sesuai</span></td>';
		//echo '<td align="left">';
	}
	?>
	                  <td align="center">
                            <?php
if ($change_status != 2) {?>
	                    	<a data-original-title="Lihat KK Baru" href="<?php echo base_url(); ?><?php echo 'index.php/verify_changed/lihat_baru/' ?><?php echo $row->new_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
	                    	<?php }?>
                            <a data-original-title="Hapus" onclick="return confirm(\'Yakin akan menghapus data ini ?\')" href="<?php echo base_url(); ?><?php echo 'index.php/verify_changed/hapus/' ?><?php echo $row->nik; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-times"></i></a>
                          </td>
	                  <?php /*
	if ($status == 1)
	{
	echo '<td align="center">';
	echo '<span class="label label-success">verified</span></td>';
	echo '<td align="center">';
	}
	else if ($status == 2)
	{
	echo '<td align="center">';
	echo '<span class="label label-danger">status changed</span></td>';
	echo '<td align="center">';
	}
	else if ($status == 3)
	{
	echo '<td align="center">';
	echo '<span class="label label-warning">on process</span>';
	echo '<td align="center">';
	echo '<a data-original-title="Edit" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;';
	echo '<a data-original-title="Batal" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ban"></i></a></td>';
	}
	else
	{
	echo '<td align="center">';
	echo '<span class="label label-primary">no status</span>';
	echo '<td align="center">';
	echo '<a data-original-title="Proses" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-share"></i></a></td>';
	} */?>
	          </tr>
	              <?php
}
?>
	         <?php endif;?>
	          </tbody>
    		</table>
		</div>
    </div>
</div><!-- /.box -->