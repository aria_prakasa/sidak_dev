<!-- <div class="callout callout-success">
	<h4>Pengantar</h4>
	<p>SIDaK (Sistem Informasi Data Kemiskinan) ONLINE.</p>
	<!-- <h3 class="box-title"><strong>Pengantar</strong></h3> -->
<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><b>Pengantar</b></h3>
              </div>
              <div class="box-body">
                <div class="row margin-bottom">
	                <div class="col-sm-3" style="text-align:center">
	                  <img class="img-responsive" src="<?php echo base_url(); ?>_assets/dist/img/logo.png" alt="Photo" style="margin-left: 20px;margin-bottom: 10px;">
	                  <p><b><i> Logo Pemerintah Kota Pasuruan </i></b></p>
	                </div><!-- /.col -->
	                <div class="col-sm-9">
	                	<p> <b>Sistem Informasi Data Kemiskinan Online</b> atau disingkat dengan nama <b>SIDaK ONLINE</b> adalah suatu aplikasi yang dikembangkan oleh daerah untuk me-<i>manage</i> data Penduduk kategori miskin di daerahnya. Data Penduduk saat ini dirasakan oleh hampir semua daerah belumlah valid atau benar keadaanya. Diharapkan dengan Aplikasi ini dapat memfasilitasi pengelolaan data penduduk kategori miskin sehingga diharapkan data penduduk tersebut valid.
	                	</p> 

	                	<p> Bisnis Proses Aplikasi <b>SIDaK ONLINE</b> adalah menggandeng Dinas Kependudukan dan Pencatatan Sipil dalam hal ini adalah sebagai Pemegang Basis Data Kependudukan Pemerintah Daerah. Proses input aplikasi ini adalah memakai acuan data NO.KK (Kartu Keluarga) dan NIK (Nomor Induk Kependudukan) dari database Dinas Kependudukan dan Pencatatan Sipil untuk selanjutnya didapatkan data pokok penduduk untuk dilakukan proses pemasukan data. Jadi hanya penduduk yang terdaftar di Database Dinas Kependudukan dan Pencatatan Sipil saja yang dapat dilakukan proses. Jika penduduk yang bersangkutan tidak/bukan penduduk Daerah, maka penduduk yang bersangkutan haruslah mendaftarkan terlebih dahulu di Dinas Kependudukan dan Pencatatan Sipil Daerah untuk selanjutnya diproses di Aplikasi <b>SIDaK ONLINE</b>.</p>

	                	<p> Dengan adanya kebenaran data dalam database Kemiskinan, maka dapat dijadikan landasan oleh Pemerintah daerah dalam hal mengambil kebijakan di berbagai sektor, a.l: Pembangunan, Perekonomian, Bantuan Sosial, Kesehatan, Pendidikan, Kesejahteraan Masyarakat dan lain sebagainya. Dan juga dengan adanya kebenaran data maka mengurangi terjadinya salah sasaran dalam pemberian bantuan oleh Pemerintah.</p> 
	                	
	                	 


	                  <!-- <div class="row">
	                    <div class="col-sm-6">
	                      <img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
	                      <br>
	                      <img class="img-responsive" src="../../dist/img/photo3.jpg" alt="Photo">
	                    </div><!-- /.col --> 
	                    <!-- <div class="col-sm-6">
	                      <img class="img-responsive" src="../../dist/img/photo4.jpg" alt="Photo">
	                      <br>
	                      <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
	                    </div><!-- /.col -->
	                  <!-- </div>/.row  -->
	                </div><!-- /.col -->
	              </div>
              </div><!-- /.box-body -->
            </div>