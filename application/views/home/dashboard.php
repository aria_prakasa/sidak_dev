

<section class="content-header">
	<h1 style="margin-bottom: 20px;"><span class="ion ion-ios-pulse-strong"></span> Keterangan Penduduk Wilayah
		Kota Pasuruan <small> Data Hari Ini : <?php echo format_hari_tanggal(date('d-m-Y')); ?></small></h1>
</section>
<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="ion ion-person"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Jumlah Kep Kel</span>
        <span class="info-box-number"><?php echo number_format($jml_kk); ?>&nbsp;<small>KK</small></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="ion ion-person-stalker"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Total Penduduk</span>
        <span class="info-box-number"><?php echo number_format($jumlah); ?>&nbsp;<small>JIWA</small></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->

  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="ion ion-male"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Laki-laki</span>
        <span class="info-box-number"><?php echo number_format($laki); ?>&nbsp;<small>JIWA</small></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-yellow"><i class="ion ion-female"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Perempuan</span>
        <span class="info-box-number"><?php echo number_format($perempuan); ?>&nbsp;<small>JIWA</small></span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div><!-- /.col -->
</div>

<section class="content-header">
  <h1 style="margin-bottom: 20px;"><span class="ion ion-social-markdown"></span> Keterangan Penduduk Miskin
    Kota Pasuruan</h1>
</section>
<div class="row">
  <div class="col-sm-4">
    <!-- Info Boxes Style 2 -->
    <div class="info-box bg-green">
      <span class="info-box-icon"><i class="ion ion-man"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">LAKI - LAKI</span>
        <span class="info-box-number"><?php echo number_format($laki_mskn); ?></span>
        <div class="progress">
          <?php 
            $l = ($laki_mskn / $jumlah_mskn) * 100;
          ?>
          <div class="progress-bar" style="width: <?php echo "$l"; ?>%"></div>
        </div>
        <span class="progress-description">
          <?php echo number_format($l,2); ?>% dari <?php echo number_format($jumlah_mskn); ?> penduduk miskin
        </span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
    <div class="info-box bg-yellow">
      <span class="info-box-icon"><i class="ion ion-woman"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">PEREMPUAN</span>
        <span class="info-box-number"><?php echo number_format($perempuan_mskn); ?></span>
        <div class="progress">
          <?php 
            $p = ($perempuan_mskn / $jumlah_mskn) * 100;
          ?>
          <div class="progress-bar" style="width: <?php echo "$p"; ?>%"></div>
        </div>
        <span class="progress-description">
          <?php echo number_format($p,2); ?>% dari <?php echo number_format($jumlah_mskn); ?> penduduk miskin
        </span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
    <div class="info-box bg-red">
      <span class="info-box-icon"><i class="ion ion-android-warning"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">PERMOHONAN BELUM VERIFIKASI</span>
        <span class="info-box-number"><?php echo number_format($mohon); ?></span>
        <div class="progress">
          <?php 
            $m = ($mohon / $jml_mohon) * 100;
          ?>
          <div class="progress-bar" style="width: <?php echo "$m"; ?>%"></div>
        </div>
        <span class="progress-description">
          <?php echo number_format($m,2); ?>% dari total <?php echo number_format($jml_mohon); ?> Permohonan
        </span>
      </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
  </div>


  <div class="col-md-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Kemiskinan Chart</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
          <canvas id="pieChart" style="height:250px" auto-legend></canvas>
          
      </div><!-- /.box-body -->
    </div><!-- /.box -->  
  </div>
</div>

<script type="text/javascript">
$(function () {
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: <?php echo $laki_mskn; ?>,
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Laki-laki"
    },
    {
      value: <?php echo $perempuan_mskn; ?>,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Perempuan"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 2,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);

  document.getElementById('js-legend').innerHTML = pieChart.generateLegend();

});
</script>
