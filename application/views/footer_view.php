<script>
/*function tampilKabupaten()
 {
    $('#kabupaten_id').removeAttr('disabled');
    $('#kecamatan_id').attr({'disabled': 'disabled'});
    $('#kelurahan_id').attr({'disabled': 'disabled'});
     // $('#kecamatan_id :select').val("");
     kdprop = $("#provinsi_id").val();
     $.ajax({
         url:"<?php echo base_url(); ?>index.php/search/pilih_kabupaten/"+kdprop+"",
         success: function(response){
            $("#kabupaten_id").html(response);
         },
         dataType:"html"
     });
     return false;
 }*/

 $(function() {
 	tampilKelurahan();
 });

 /*function tampilKecamatan()
 {
    // $('#kecamatan_id').removeAttr('disabled');
    // $('#kecamatan_id').attr({'disabled': 'disabled'});
    $('#kelurahan_id').attr({'disabled': 'disabled'});
     kdkab = $("#kabupaten_id").val();
     $.ajax({
         url:"<?php echo base_url(); ?>index.php/search/pilih_kecamatan/"+kdkab+"",
         success: function(response){
            $("#kecamatan_id").html(response);
         },
         dataType:"html"
     });
     return false;
 }*/

 function tampilKelurahan()
 {
    if($('#kelurahan_id').length <= 0) return;
    $('#kelurahan_id').removeAttr('disabled');
    kdkec = $("#kecamatan_id").val();
    kdkel = $("#kelurahan_id_wrap").data('id');
    $.ajax({
        method: "POST",
        data: {'kelurahan_id': kdkel},
        url:"<?php echo base_url(); ?>index.php/search/pilih_kelurahan/"+kdkec+"",
        success: function(response){
            $("#kelurahan_id").html(response);
        },
        dataType:"html"
    });
    return false;
 }

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>_assets/dist/js/jquery.mask.min.js"></script>

<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <a href="#">Badan Perencanaan Dan Pembangunan Daerah</a></br>
        Pemerintah Kota Pasuruan</br>
        <strong>Copyright &copy; 2015</strong> | All rights reserved.
      </footer>


    </div><!-- ./wrapper -->

  </body>
</html>
