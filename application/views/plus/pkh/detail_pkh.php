<div class="box">
  <div class="box-body" style="padding-right: 15px; padding-top: 15px; padding-left: 15px;">
    <div class="box-header with-border bg-success">
          <h3 class="box-title"><b>Data Keluarga</b></h3>
    </div>    
    </br>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">No KK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_kk";?> ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kepala Keluarga</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kep";?> ">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Alamat</label>
            <div class="col-sm-8">
              <textarea class="form-control" disabled="" rows="3"><?php echo "$alamat";?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <label class="col-sm-2 control-label">No RT</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rt";?>">
            </div>
            <label class="col-sm-2 control-label">No RW</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rw";?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-4 control-label">Propinsi</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_prop";?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kota / Kab</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kab";?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kecamatan</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kec";?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kelurahan</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kel";?>">
              </div>
            </div>
        </div>
      </div>    
    </div> 
    
    <div classs="row">    
      <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="box-header bg-danger">
            <h3 class="box-title"><b>Data Anggota Keluarga</b></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding" style="margin-bottom: 20px;">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>NO</th> 
              <th>NIK</th>
              <th>NAMA LENGKAP</th>
              <th>KELAMIN</th>
              <th>TGL LAHIR</th>
              <th>HUB. KELUARGA</th>
              <th>AGAMA</th>
              <th>PENDIDIKAN</th>
              <th>PEKERJAAN</th>
              <th>STATUS KEMISKINAN</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no=1;
              foreach ($anggota_list as $row)
              {
                extract((array) $row);
                ?>
            <tr>                      
              <td><?php echo $no++; ?></td>
              <td><?php echo "$nik"; ?></td>
              <td><?php echo "$nama_lgkp"; ?></td>
              <td><?php echo "$jenis_klmin"; ?></td>
              <td><?php echo format_tanggal($tgl_lhr); ?></td>
              <td><?php echo "$stat_hbkel"; ?></td>
              <td><?php echo "$agama"; ?></td>
              <td><?php echo "$pddk_akh"; ?></td>
              <td><?php echo "$jenis_pkrjn"; ?></td>
              <!-- <td><?php
                  if ($sidak_status != 0){      
                        echo "$jenis_pkrjn";                
                      } else {
                        $style_pekerjaan='class="form-control input-sm" id="jenis_pkrjn"';
                        // var_dump($id_jenis_pkrjn);
                        echo $this->master_pekerjaan->drop_down_select('jenis_pkrjn['.$nik.']', $id_jenis_pkrjn, $optional = $style_pekerjaan, $default = " --- ", $readonly = "");
                        //form_dropdown("jenis_pkrjn",$pekerjaan,$jenis_pkrjn,$jenis_pkrjn);
                      }
                  ?></td> -->
              <?php 
              if ($sidak_status == 0)
              {
                echo '<td align="left">';
                echo '<span class="label label-success">verifikasi ok</span></td>';
                //echo '<td align="left">';                        
              }
              else
              {
                echo '<td align="left">';
                echo '<span class="label label-danger">status berubah</span></td>';
                
                /*echo '<td align="left">';
                echo '<a data-original-title="Edit" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;';
                echo '<a data-original-title="Batal" href="'?><?php echo base_url();?><?php echo 'index.php/entry/proses/'?><?php echo $row->nik; ?><?php echo'" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ban"></i></a></td>';  */
              }?>  
              <!-- <td align="center">
                           <?php 
                           if ($sidak_status == 0 && $this->cu->USER_LEVEL == 0)
                           { ?>
                             <button type="submit" name="submit" value="<?php echo $nik; ?>" class="btn btn-default btn-xs" data-original-title="Simpan" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-save"></i></button>
                              <a type="submit" data-original-title="Simpan" href="<?php echo base_url(); ?>index.php/listing/simpan/<?php echo $row->no_kk; ?>/<?php echo $row->nik; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-save"></i></a>                     
                           <?php } ?> 
                       </td>     -->         
            </tr>
                <?php
              }
              ?>
                                    
            </tbody>

          </table>
        </div><!-- /.box-body -->
        
        
        <?php 
        
          /*echo '<div class="row">';
          echo '<div class="col-sm-2">';
          echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing/'?><?php echo '" ><button class="btn bg-navy margin"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp; Kembali</button></a></div>';
          echo '<div class="col-sm-8"></div>';
          echo '<div class="col-sm-2">';
          echo '<a href="'?><?php echo base_url();?><?php echo 'index.php/listing/hapus/'?><?php echo $row->no_kk; ?><?php echo '" onclick="return confirm("Yakin akan menghapus data ini ?"); "><button class="btn bg-red margin">Hapus Validasi &nbsp;&nbsp;<i class="fa fa-times"></i></button></a></div>';  
        */
        ?>                
     </div><!-- /.box-body -->
    </div><!-- /.row -->
      
    <div classs="row">    
      <div class="col-sm-12" style="padding-bottom: 10px; padding-left: 0px; padding-right: 0px;">
        <div class="box-header bg-info">
            <h3 class="box-title"><b>Data Kemiskinan </b></h3>
        </div><!-- /.box-header -->
      </div>
    </div>

    <?php echo form_open('pkh/simpan/'); ?>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Kategori hunian</label>
            <div class="col-sm-8">
              <!-- <input type="text" name="kk" id="kk" class="form-control" value="<?php echo "$tipe_hunian";?> "> -->
              <?php echo form_dropdown('tipe_hunian', $opsi_hunian, $tipe_hunian, 'class="form-control input-sm"'); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Keterangan bantuan </label>
            <div class="col-sm-8">
              <textarea class="form-control" name="bantuan" rows="3" style="text-transform:uppercase"><?php echo "$bantuan_pkh";?></textarea>
            </div>
          </div>
          
          <div class="form-group">
            <div class="col-sm-4">
              <a class="btn bg-navy" href="<?php echo base_url();?>index.php/pkh/index/1" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
            </div>
            <div class="col-sm-4">
                <button type="submit" name="submit" value="<?php echo $no_kk; ?>" class="btn bg-olive">Simpan Data <i class="fa fa-save"></i></button>
            </div>
            <div class="col-sm-4">
                <button type="submit" name="hapus" value="<?php echo $no_kk; ?>" class="btn bg-red" onclick="return confirm('Yakin akan menghapus data ini ?')">Hapus Data <i class="fa fa-times"></i></button>
                <!-- <button class="btn bg-red" onclick="return confirm('Yakin akan menghapus data ini ?')" href="<?php echo base_url(); ?>index.php/raskin/hapus/<?php echo $no_kk; ?>">Hapus Data <i class="fa fa-times"></i></button> -->
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Tgl Entri</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$tgl_insert_pkh";?> ">
            </div>
          </div>
        </div>  
      </div>
    </div>
    <?php echo form_close(); ?>  
  </div>
</div><!-- /.box -->
