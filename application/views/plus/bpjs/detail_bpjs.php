<div class="box">
  <div class="box-body" style="padding-right: 15px; padding-top: 15px; padding-left: 15px;">
    <div class="box-header with-border bg-success">
          <h3 class="box-title"><b>Data Keluarga</b></h3>
    </div>
    </br>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">No KK</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_kk"; ?> ">
            </div>
          </div>
          <!-- <div class="form-group">
            <label class="col-sm-4 control-label">Kepala Keluarga</label>
              <div class="col-sm-8">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kep"; ?> ">
              </div>
          </div> -->
          <div class="form-group">
            <label class="col-sm-4 control-label">Alamat</label>
            <div class="col-sm-8">
              <textarea class="form-control" disabled="" rows="3"><?php echo "$alamat"; ?></textarea>
            </div>
            </div>
          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <label class="col-sm-2 control-label">No RT</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rt"; ?>">
            </div>
            <label class="col-sm-2 control-label">No RW</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$no_rw"; ?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-4 control-label">Propinsi</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="JAWA TIMUR">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kota / Kab</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="KOTA PASURUAN">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kecamatan</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kec"; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Kelurahan</label>
              <div class="col-sm-6">
                <input type="text" name="kk" id="kk" disabled="" class="form-control" value="<?php echo "$nama_kel"; ?>">
              </div>
            </div>
        </div>
      </div>
    </div>

    <!--  <?php echo form_hidden('no_kk', $no_kk); ?> -->
<!--     <?php echo form_hidden('nik', $row->nik); ?> -->
    <div classs="row">
      <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="box-header bg-danger">
            <h3 class="box-title"><b>Data Anggota Keluarga</b></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-bordered table-striped">

            <thead>
            <tr>
              <th>NO</th>
              <th>NIK</th>
              <th>NAMA LENGKAP</th>
              <th>KELAMIN / TGL LAHIR</th>
              <th>HUB. KELUARGA</th>
              <th>PEKERJAAN</th>
              <th>STATUS BPJS</th>
              <th width="20%">BPJS</th>
              <th>OPERASI</th>
            </tr>
            </thead>
            <tbody>
              <?php
// var_dump($data_list); die;
$no = 1;
foreach ($data_list as $row) {
	extract((array) $row);
	?>
            <tr>
            <?php echo form_open('bpjs/simpan/'); ?>
            <?php echo form_hidden('no_kk', $no_kk); ?>
              <td><?php echo $no++; ?></td>
              <td><?php echo "$nik"; ?></td>
              <td><?php echo "$nama_lgkp"; ?></td>
              <td><?php echo $jenis_klmin . " /<br/> " . format_tanggal($tgl_lhr); ?></td>
              <!-- <td><?php echo format_tanggal($tgl_lhr); ?></td> -->
              <td><?php echo "$stat_hbkel"; ?></td>
              <td><?php echo "$jenis_pkrjn"; ?></td>
              <?php
if ($status_bpjs == 2) {
		echo '<td align="center">';
		echo '<span class="label label-success" data-original-title="Ada" data-toggle="tooltip" data-placement="bottom">ada</span></td>';
	} else {
		echo '<td align="center">';
		echo '<span class="label label-danger" data-original-title="Tidak Ada" data-toggle="tooltip" data-placement="bottom">tidak ada</span></td>';
	}?>
              <td align="left">
                  BPJS no :<input type="text" class="form-control input-sm" name="bpjs" id="bpjs" value="<?php echo "$no_bpjs" ?>" style="text-transform:uppercase" />
                  Kategori :
                  <?php
// var_dump($kategori);die;
	/*$style_kategori='class="form-control input-sm" id="kategori"';
                  echo form_dropdown('kategori',$kategori,$kategori,$style_kategori);*/
	/*$style_kategori='class="form-control input-sm" id="kategori"';
		                        // var_dump($id_jenis_pkrjn);
	*/
	/*$options = array('' => ' - Pilih Kategori - ',
		                        '1' => 'Pusat',
		                        '2' => 'Daerah'
	*/
	echo form_dropdown('kategori', $opsi_kategori, $jenis_bpjs, 'class="form-control input-sm"');
	?>
                  <!-- berlaku s/d :
                  <?php echo form_input('period', $berlaku_bpjs, 'placeholder="DD/MM/YYYY" type="text" class="form-control input-sm" data-mask="00/00/0000" id="period"'); ?> -->
              </td>
              <td align="center">
                <button type="submit" name="submit" value="<?php echo $nik; ?>" class="btn btn-info btn-xs" data-original-title="Simpan" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-save"></i></button>&nbsp;
                <button type="submit" name="hapus" value="<?php echo $nik; ?>" class="btn btn-danger btn-xs" data-original-title="Hapus data BPJS" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Yakin akan menghapus data ini ?')"> <i class="fa fa-times"></i></button>
              </td>
            <?php echo form_close(); ?>
            </tr>
            <?php
}
?>

            </tbody>

          </table>
        </div><!-- /.box-body -->

        <div class="row">
          <div class="col-md-2">
            <a class="btn bg-navy margin" href="<?php echo base_url(); ?>index.php/bpjs/index/1" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a></div>
          <div class="col-md-8"></div>
          <!-- <div class="col-sm-10">
            <a href="<?php echo base_url(); ?>index.php/verify_apply" ><button class="btn bg-navy margin" style="margin-left: 0px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</button></a></div> -->
          <!-- <div class="col-sm-2">
            <a class="btn bg-red margin" onclick="return confirm('Yakin akan menghapus data ini ?')" href="<?php echo base_url(); ?>index.php/listing/hapus/<?php echo $no_kk; ?>">Hapus Data &nbsp;&nbsp;<i class="fa fa-times"></i></a></div> -->
          <div class="col-sm-2">

       </div><!-- /.box-body -->
    </div><!-- /.row -->

</div><!-- /.box -->