<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><strong>Pencarian Data Penduduk Miskin :</strong></h3>
        <!-- <div class="box-tools pull-right">
            <?php echo anchor($this->curpage . "/index/export", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary"') ?>
            <?php /* ?>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        <?php //*/?>
        </div> -->
    </div>

    <div class="box-body">
    <?php $this->load->view('tpl_filter_plus'); ?>

    <?php 
    if( ! $jml_data): ?>
    &nbsp;
    <?php else: ?> 
    
    <?php echo form_open('raskin/simpan/'); ?>
    <?php echo form_hidden('no_kk', $no_kk); ?>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
            <?php if (!$data_list): ?>
                <tr>
                    <td colspan="11">
                    &nbsp;
                    </td>
                </tr>
            <?php else: ?>
            <thead>
            <tr>
                <th width="5%">NO</th>
                <th>NO KK</th>
                <th>NAMA KEPALA KEL</th>
                <th>PEKERJAAN</th>
                <th>ALAMAT</th>
                <th>KELURAHAN</th>
                <th>KECAMATAN</th>
                <th>STATUS RASKIN</th>
                <th>OPERASI</th>
                <!-- <th>OPERASI</th> -->
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($data_list as $row) {
            $row = keysToLower($row);
            extract((array) $row);
            ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo "$no_kk"; ?></td>
                <td><?php echo "$nama"; ?></td>
                <td><?php echo "$jenis_pkrjn"; ?></td>
                <td><?php echo "$alamat; NO RT. $no_rt/NO RW. $no_rw"; ?></td>
                <td><?php echo "$nama_kel"; ?></td>
                <td><?php echo "$nama_kec"; ?></td>
                <?php 
                if ($status_raskin == 2)
                {
                    echo '<td align="left">';
                    echo '<span class="label label-success" data-original-title="Ada" data-toggle="tooltip" data-placement="bottom">ada</span></td>';
                }
                else
                {
                    echo '<td align="left">';
                    echo '<span class="label label-danger" data-original-title="Tidak Ada" data-toggle="tooltip" data-placement="bottom">tidak ada</span></td>';
                }?>  
                <td align="center">
                    <a data-original-title="Lihat KK" href="<?php echo base_url();?>index.php/raskin/lihat/<?php echo $row->no_kk; ?>" class="ajaxify fa-item tooltips" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-search"></i></a>&nbsp;
                </td> 
                <!-- <td align="right">
                    <input type="text" class="form-control input-sm" name="raskin" id="raskin" value="<?php echo "$no_raskin"?>" style="text-transform:uppercase" /><br/>
                    <input type="hidden" class="form-control input-sm" name="no_kk" id="no_kk" value="<?php echo "$no_kk"?>" /><br/>
                    <button type="submit" name="nik" value="<?php echo $nik; ?>" class="btn btn-success btn-xs" data-original-title="Simpan" data-toggle="tooltip" data-placement="bottom">Simpan <i class="fa fa-save"></i></button>
                </td>  -->
                <!-- <td align="center">
                    <button type="submit" name="submit" value="<?php echo $nik; ?>" class="btn btn-default btn-xs" data-original-title="Simpan" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-save"></i></button>
                </td>   -->    
            </tr>
            <?php
            }
            ?>
            <?php endif;?>
            </tbody>
            </table>
        </div>
    <?php endif; ?>
    </div>
</div><!-- /.box -->


