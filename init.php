<?php
date_default_timezone_set("Asia/Jakarta");
/** define for global */
define('DOMAIN_NAME', str_replace("www.", "", $_SERVER['HTTP_HOST']));
define('SITE_NAME', '');
/*
|--------------------------------------------------------------------------
| Flag for LOCAL /not?
|--------------------------------------------------------------------------
|
|
 */
if (stristr(DOMAIN_NAME, 'dev') || stristr(DOMAIN_NAME, 'local')) {
    define('IS_LOCAL', true);
} else {
    define('IS_LOCAL', false);
}

define('CACHE', true);
define('CACHE_TIME', (60 * 60 * 24)); // 1 day

// init default location
define('NO_PROP', 35);
define('NO_KAB', 75);

define('FR_VIEW', '');
define('ADM_VIEW', 'admin/');

define('FR_BASE', '');
define('ADM_BASE', 'admin/');

define('AGREGAT_BASE', 'agregat');
define('AGREGAT_VIEW', 'agregat/');

define('INDUK_BASE', 'induk');
define('INDUK_VIEW', 'induk/');

define('MOBILITAS_BASE', 'mobilitas');
define('MOBILITAS_VIEW', 'mobilitas/');

define('PARTIAL_VIEW', 'includes/');
define('CETAK_VIEW', 'cetak/');

/*
|--------------------------------------------------------------------------
| Domain Name
|-------------------------------------------------------------------------
 */
